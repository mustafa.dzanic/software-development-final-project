package views;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;

import static ch.lambdaj.Lambda.*;

import javax.imageio.ImageIO;
import javax.persistence.*;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

import java.awt.Color;

import javax.swing.JLabel;

import java.awt.Font;
import java.awt.Image;
import java.io.InputStream;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.JTextField;

import models.RadnoMjesto;
import models.Korisnik;

import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;

import org.hamcrest.Matchers;
import javax.swing.JPanel;

public class RadnaMjesta {

	public JFrame frmRadnaMjesta;
	private JTextField txtRadnoMjesto;
	private EntityManagerFactory emf;
	private EntityManager em;
	private JTable table;
	private  JScrollPane scrollPane;
	private int selected;
	public Korisnik currentUser;
	private List<RadnoMjesto> radnaMjesta;
	private List<RadnoMjesto> radnaMjestaSource;
	private void OsvjeziTabelu(boolean reload)
	{
		
		Query q = em.createQuery("select t from RadnoMjesto t");
	    if(reload)
	    {
	    radnaMjesta = q.getResultList();
	    radnaMjestaSource=radnaMjesta;
	    }
        Object columnNames[] = { "ID", "Radno mjesto"};
        String[][] rowData = new String[radnaMjesta.size()][2];
        
        int i=0;
	    for(RadnoMjesto rm : radnaMjesta)
	    {
	    	rowData[i][0]=new Integer(rm.getId()).toString();
	    	rowData[i][1]=rm.getNaziv();
	    	i++;
	    }
	    			
	    table.setModel(new DefaultTableModel(rowData,columnNames));
        table.getColumnModel().getColumn(0).setMinWidth(20);
        table.getColumnModel().getColumn(0).setMaxWidth(50);
        table.getColumnModel().getColumn(0).setPreferredWidth(35);	
        
	}
	
	public RadnaMjesta() 
	{
		emf = Persistence.createEntityManagerFactory("StudentskaSluzba");
	    em = emf.createEntityManager();
	    em.getEntityManagerFactory().getCache().evictAll();
	    initialize();

	}

	private void initialize() {
		frmRadnaMjesta = new JFrame();
		frmRadnaMjesta.setResizable(false);
		frmRadnaMjesta.getContentPane().setBackground(new Color(255, 255, 255));
		frmRadnaMjesta.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Naziv radnog mjesta:");
		lblNewLabel.setBounds(15, 199, 163, 16);
		frmRadnaMjesta.getContentPane().add(lblNewLabel);
		
		txtRadnoMjesto = new JTextField();
		txtRadnoMjesto.setBounds(15, 219, 237, 35);
		frmRadnaMjesta.getContentPane().add(txtRadnoMjesto);
		txtRadnoMjesto.setColumns(10);
		
		Object columnNames[] = { "ID", "Radno mjesto"};
        String[][] rowData = new String[0][2];
        table = new JTable(rowData, columnNames) {
		    @Override
		    public boolean isCellEditable(int row, int column) {
		       return false;
		    }
		};
        scrollPane = new JScrollPane(table);
        frmRadnaMjesta.getContentPane().add(scrollPane, BorderLayout.CENTER);
        scrollPane.setBounds(282, 201, 446 , 241);
        
        JButton btnPovratak = new JButton("Povratak");
        btnPovratak.setForeground(new Color(255, 255, 255));
        btnPovratak.setBackground(new Color(128, 0, 0));
        btnPovratak.setBounds(638, 454, 90, 29);
        frmRadnaMjesta.getContentPane().add(btnPovratak);
        btnPovratak.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				AdminPanel window = new AdminPanel(currentUser);
	    		frmRadnaMjesta.dispose();
	    		window.frmAdministrator.setVisible(true);
			}
		});
        
        JButton btnDodati = new JButton("Dodati");
        btnDodati.setForeground(new Color(255, 255, 255));
        btnDodati.setBackground(new Color(128, 0, 0));
        btnDodati.addActionListener(new ActionListener()
        {
        @Override
          public void actionPerformed(ActionEvent e)
          {
        	  if(txtRadnoMjesto.getText().length()<1)
        	  {
        		  JOptionPane.showMessageDialog(null, "Niste unijeli naziv radnog mjesta!", "Greška", JOptionPane.ERROR_MESSAGE);
        		  txtRadnoMjesto.requestFocus();
        	  }
        	  else
        	  {
        	  	em.getTransaction().begin();
        	    RadnoMjesto rm = new RadnoMjesto();
        	    rm.setNaziv(txtRadnoMjesto.getText());
        	    em.persist(rm);
        	    em.getTransaction().commit();        	    
        	   
        	    OsvjeziTabelu(true);
        	    txtRadnoMjesto.setText("");
        	  }
          }
        });
        btnDodati.setBounds(0, 454, 90, 42);
        frmRadnaMjesta.getContentPane().add(btnDodati);
        
        JButton btnIzmijeniti = new JButton("Izmijeniti");
        btnIzmijeniti.setBackground(new Color(128, 0, 0));
        btnIzmijeniti.setForeground(new Color(255, 255, 255));
        btnIzmijeniti.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
        	 if(selected>-1)
        	 {
        	 String a=JOptionPane.showInputDialog(null,"Izmjena radnog mjesta '" + table.getValueAt(selected, 1) + "':", table.getValueAt(selected, 1));
        	 if(a!=null && a.length()>0)
        	 {
        	    em.getTransaction().begin();        	      
        	    RadnoMjesto temp = radnaMjesta.get(selected); 
        	    temp.setNaziv(a);
        	    em.getTransaction().commit();
        	    OsvjeziTabelu(true);
        	    selected=-1;
        	 }
        	 }
          }
        
        });
        btnIzmijeniti.setBounds(88, 454, 90, 42);
        frmRadnaMjesta.getContentPane().add(btnIzmijeniti);
        
        JButton btnObrisati = new JButton("Obrisati");
        btnObrisati.setBackground(new Color(128, 0, 0));
        btnObrisati.setForeground(new Color(255, 255, 255));
        btnObrisati.addActionListener(new ActionListener()
        {
        	@Override
          public void actionPerformed(ActionEvent e)
          {
          if(selected>-1)
	         {
	        	try
	        	{
        	    em.getTransaction().begin();
        	       
        	    RadnoMjesto temp = radnaMjesta.get(selected); 
        	    em.remove(temp);
        	    em.getTransaction().commit();        	    
        	 	  DefaultTableModel model = new DefaultTableModel();
            	  model=(DefaultTableModel) table.getModel();
                  model.removeRow(selected);
                  selected=-1;
	        	}
	        	catch(Exception err)
	        	{
	        		JOptionPane.showMessageDialog(null, "Nije moguće obrisati radno mjesto koje ima pridružene nastavnike!", "Greška", JOptionPane.ERROR_MESSAGE);
	        	}
              }
             }
        
        });
        btnObrisati.setBounds(176, 454, 90, 42);
        frmRadnaMjesta.getContentPane().add(btnObrisati);
        
        JLabel label = new JLabel("");
        label.setBounds(0, 0, 740, 185);
        Image naslovnica= new ImageIcon (this.getClass().getResource("/cover.jpg")).getImage();
		label.setIcon(new ImageIcon(naslovnica));
        frmRadnaMjesta.getContentPane().add(label);
        
        JPanel panel = new JPanel();
        panel.setBounds(0, 183, 265, 313);
        frmRadnaMjesta.getContentPane().add(panel);

        OsvjeziTabelu(true);
        
       
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            			@Override
			public void valueChanged(ListSelectionEvent e) 
            {
            	selected = table.getSelectedRow();
				
			}
        });
		
		
		
		frmRadnaMjesta.setBackground(new Color(255, 255, 255));
		frmRadnaMjesta.setTitle("Radna mjesta");
		frmRadnaMjesta.setBounds(100, 100, 450, 300);
		frmRadnaMjesta.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmRadnaMjesta.setSize(new Dimension(746, 531));
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frmRadnaMjesta.setLocation(dim.width/2-frmRadnaMjesta.getSize().width/2, dim.height/2-frmRadnaMjesta.getSize().height/2);
	}
}
