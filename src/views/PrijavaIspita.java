package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseListener;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.imageio.ImageIO;
import javax.persistence.*;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;

import models.Ispit;
import models.RezultatIspita;
import models.Sekcija;
import models.Student;
import models.Usmjerenje;

import java.util.Date;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.table.DefaultTableModel;

public class PrijavaIspita {

	public JFrame frmPrijavaIspita;
	private EntityManagerFactory emf;
	private EntityManager em;
	private ArrayList<Ispit> ispiti;
	private Student currentUser;
	private JTable tableIspiti;
	private JScrollPane scrollPaneIspiti;	
	
	public PrijavaIspita(Student user) 	{
		currentUser=user;
		emf = Persistence.createEntityManagerFactory("StudentskaSluzba");		
	    em = emf.createEntityManager();			   
	    em.getEntityManagerFactory().getCache().evictAll();
	    ArrayList<Ispit> temp=new ArrayList<Ispit>();
	    for(Sekcija s : currentUser.getSekcije())
	    {
	    	temp.addAll(s.getIspiti());
	    }
	    	   
	    ispiti=new ArrayList<Ispit>();
	    SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
	    for(Ispit isp : temp)
	    {
			try {
				Date date = formatter.parse(isp.getDatum());
				Date today=new Date();
				if(today.before(date) && isp.getTip()==0)
				{
					RezultatIspita trenutniIspit=null;
					ArrayList<RezultatIspita> rezultati=new ArrayList<RezultatIspita>();
					rezultati.addAll(currentUser.getRezultati());
					Collections.sort(rezultati, new Comparator<RezultatIspita>() {
				        public int compare(RezultatIspita r1, RezultatIspita r2) {
				           return Integer.valueOf(new Float(r1.getBodovi()).compareTo(new Float(r2.getBodovi())));
				        }
				});
					Collections.reverse(rezultati);
					for(RezultatIspita rez : rezultati)
					{					
						if(rez.getIspit()==isp)
						{
							trenutniIspit=rez;
							break;
						}
					}
					if(trenutniIspit==null || trenutniIspit.getBodovi()<0.54*trenutniIspit.getIspit().getBodovi())
						ispiti.add(isp);
				}
			} catch (ParseException e) 
			{
			
			}
	    }	    
	    
		initialize();
	}
	
	private void OsvjeziTabelu()
	{
		Object columnNamesPredmeti[] = { "ID", "Sekcija", "Datum", "Vrijeme", "Sala", "Prijava"};
        Object[][] rowDataPredmeti = new Object[ispiti.size()][6];
        int j=0;
	    for(Ispit isp : ispiti)
	    {
	    	Ispit isp1=null;
	    	if(currentUser.getIspiti().contains(isp))
	    		isp1=isp;
	    	rowDataPredmeti[j][0]=new Integer(isp.getId()).toString();
	    	rowDataPredmeti[j][1]=isp.getSekcija().getNaziv() + " (" + isp.getSekcija().getPredmet().getNaziv() + ")";
	    	rowDataPredmeti[j][2]=isp.getDatum();
	    	rowDataPredmeti[j][3]=isp.getVrijeme();
	    	rowDataPredmeti[j][4]=isp.getSala();
	    	rowDataPredmeti[j][5]= isp1!=null ? true : false;
	    	j++;
	    }
	    
	    tableIspiti.setModel(new DefaultTableModel(rowDataPredmeti,columnNamesPredmeti));
	    
	    tableIspiti.getColumnModel().getColumn(0).setMinWidth(30);
	    tableIspiti.getColumnModel().getColumn(0).setMaxWidth(30);
	    tableIspiti.getColumnModel().getColumn(0).setPreferredWidth(30);
	    
	    tableIspiti.getColumnModel().getColumn(0).setMinWidth(30);
	    tableIspiti.getColumnModel().getColumn(0).setMaxWidth(30);
	    tableIspiti.getColumnModel().getColumn(0).setPreferredWidth(30);
	    
	    tableIspiti.getColumnModel().getColumn(2).setMinWidth(80);
	    tableIspiti.getColumnModel().getColumn(2).setMaxWidth(80);
	    tableIspiti.getColumnModel().getColumn(2).setPreferredWidth(80);
	    
	    tableIspiti.getColumnModel().getColumn(3).setMinWidth(50);
	    tableIspiti.getColumnModel().getColumn(3).setMaxWidth(50);
	    tableIspiti.getColumnModel().getColumn(3).setPreferredWidth(50);
	    
	    
	    tableIspiti.getColumnModel().getColumn(4).setMinWidth(40);
	    tableIspiti.getColumnModel().getColumn(4).setMaxWidth(40);
	    tableIspiti.getColumnModel().getColumn(4).setPreferredWidth(40);
	    
	    tableIspiti.getColumnModel().getColumn(5).setMinWidth(45);
	    tableIspiti.getColumnModel().getColumn(5).setMaxWidth(45);
	    tableIspiti.getColumnModel().getColumn(5).setPreferredWidth(45);
	    
	}

		private void initialize() {
		frmPrijavaIspita = new JFrame();
		frmPrijavaIspita.setResizable(false);
		frmPrijavaIspita.setTitle("Prijava ispita");
		frmPrijavaIspita.getContentPane().setBackground(Color.WHITE);
		frmPrijavaIspita.setBounds(100, 100, 593, 497);
		frmPrijavaIspita.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmPrijavaIspita.getContentPane().setLayout(null);
		
		Object columnNamesPredmeti[] = { "ID", "Sekcija", "Datum", "Vrijeme", "Prijava"};
        Object[][] rowDataPredmeti = new Object[0][6];		       
        tableIspiti = new JTable(rowDataPredmeti, columnNamesPredmeti) {
		    @Override
		    public boolean isCellEditable(int row, int column) {
		       if(column==5)
		    	   return true;
		       else
		    	   return false;
		    }
		    public Class getColumnClass(int column) {
                switch (column) {
                    case 0:
                        return String.class;
                    case 1:
                        return String.class;
                    case 2:
                        return String.class;
                    case 3:
                        return String.class;
                    case 4:
                        return String.class;
                    default:
                        return Boolean.class;
                       
                }
		    }
		};
			
		OsvjeziTabelu();
		
        scrollPaneIspiti = new JScrollPane(tableIspiti);
        frmPrijavaIspita.getContentPane().add(scrollPaneIspiti, BorderLayout.CENTER);
        scrollPaneIspiti.setBounds(18, 13, 552, 390);
	    
        
	    JButton btnPovratak = new JButton("Povratak");
	    btnPovratak.setBackground(new Color(128, 0, 0));
	    btnPovratak.setForeground(new Color(255, 255, 255));
	    btnPovratak.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
        	  StudentHome window = new StudentHome(currentUser.getKorisnik());
				frmPrijavaIspita.dispose();
	    		window.frmStudentHome.setVisible(true);
          }
        });

        btnPovratak.setBounds(480, 420, 90, 29);
        frmPrijavaIspita.getContentPane().add(btnPovratak);
        
        JButton btnPotvrditi = new JButton("Potvrditi");
        btnPotvrditi.setForeground(new Color(255, 255, 255));
        btnPotvrditi.setBackground(new Color(128, 0, 0));
        btnPotvrditi.setBounds(18, 420, 90, 29);
        frmPrijavaIspita.getContentPane().add(btnPotvrditi);
        btnPotvrditi.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {        	  
        	  	ArrayList<Ispit> odabraniIspiti=new ArrayList<Ispit>();
        	  	for(Ispit isp : currentUser.getIspiti())
        	  	{
        	  		if(isp.getTip()==1)
        	  			odabraniIspiti.add(isp);
        	  	}
      	    	for(int i=0; i<tableIspiti.getRowCount(); i++)
      	    	{
      	    		if(tableIspiti.getValueAt(i, 5).equals(true))
      	    		{           	    	
      	    			odabraniIspiti.add(ispiti.get(i));      	    			
      	    		}        	    	
      	    	}  
      	    	em.getTransaction().begin();
      	    	currentUser.setIspiti(odabraniIspiti);      	    	
      	    	em.persist(em.merge(currentUser));
      	    	em.getTransaction().commit();
        	  	StudentHome window = new StudentHome(currentUser.getKorisnik());
				frmPrijavaIspita.dispose();
	    		window.frmStudentHome.setVisible(true);      	    	
          }
        });

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frmPrijavaIspita.setLocation(dim.width/2-frmPrijavaIspita.getSize().width/2, dim.height/2-frmPrijavaIspita.getSize().height/2);
	}
}
