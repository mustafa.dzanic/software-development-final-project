package views;
import models.Nastavnik;

import java.awt.EventQueue;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import models.Ispit;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;

public class ProfesorProfil {

	public JFrame frmInfoOProfesoru;
	private Nastavnik currentProf;
	private EntityManagerFactory emf;
	private EntityManager em;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ProfesorProfil window = new ProfesorProfil();
					window.frmInfoOProfesoru.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public ProfesorProfil(Nastavnik prof) {
		currentProf=prof;
		emf = Persistence.createEntityManagerFactory("StudentskaSluzba");		
	    em = emf.createEntityManager();			   
	    em.getEntityManagerFactory().getCache().evictAll();
	    initialize();
	}
	
	public ProfesorProfil() {
	    initialize();
	}

	private void initialize() {
		frmInfoOProfesoru = new JFrame();
		frmInfoOProfesoru.setTitle("Informacije o profesoru");
		frmInfoOProfesoru.getContentPane().setBackground(Color.WHITE);
		frmInfoOProfesoru.getContentPane().setForeground(Color.WHITE);
		frmInfoOProfesoru.setBounds(100, 100, 508, 257);
		frmInfoOProfesoru.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmInfoOProfesoru.getContentPane().setLayout(null);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frmInfoOProfesoru.setLocation(dim.width/2-frmInfoOProfesoru.getSize().width/2, dim.height/2-frmInfoOProfesoru.getSize().height/2);
		
		JLabel label = new JLabel("");
		String path = currentProf.getKorisnik().getClasspath();
		if (path==null) {
		label.setIcon(new ImageIcon("C:\\\\Users\\\\Mustafa\\\\Desktop\\\\defaultIcon.jpg"));	
		}
		else {
		label.setIcon(new ImageIcon(path));
		}
		
		label.setBounds(12, 27, 161, 163);
		frmInfoOProfesoru.getContentPane().add(label);
		
	   
		
		JLabel label_1 = new JLabel("Ime: " + currentProf.getIme());
		label_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		label_1.setBounds(185, 13, 293, 40);
		frmInfoOProfesoru.getContentPane().add(label_1);
		
		JLabel label_2 = new JLabel("Prezime: "+ currentProf.getPrezime());
		label_2.setFont(new Font("Tahoma", Font.BOLD, 15));
		label_2.setBounds(185, 38, 293, 40);
		frmInfoOProfesoru.getContentPane().add(label_2);
		
		JLabel label_3 = new JLabel("Predmeti koje predaje: "+ currentProf.getListPredmeti());
		label_3.setFont(new Font("Tahoma", Font.BOLD, 15));
		label_3.setBounds(185, 80, 293, 40);
		frmInfoOProfesoru.getContentPane().add(label_3);
		
		JLabel label_5 = new JLabel("Titula: "+ currentProf.getZvanje());
		label_5.setFont(new Font("Tahoma", Font.BOLD, 15));
		label_5.setBounds(185, 108, 293, 40);
		frmInfoOProfesoru.getContentPane().add(label_5);
		
		JLabel label_7 = new JLabel("Radno mjesto: "+ currentProf.getRadnoMjesto().getNaziv());
		label_7.setFont(new Font("Tahoma", Font.BOLD, 15));
		label_7.setBounds(185, 133, 293, 40);
		frmInfoOProfesoru.getContentPane().add(label_7);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(128, 0, 0));
		panel.setBounds(0, 0, 490, 17);
		frmInfoOProfesoru.getContentPane().add(panel);
	}
}
