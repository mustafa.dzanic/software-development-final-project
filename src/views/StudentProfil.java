package views;
import models.Student;
import java.awt.EventQueue;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import models.Ispit;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;

public class StudentProfil {

	public JFrame frmInfoOStudentu;
	private Student currentStud;
	private EntityManagerFactory emf;
	private EntityManager em;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StudentProfil window = new StudentProfil();
					window.frmInfoOStudentu.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	public StudentProfil(Student stud) {
		currentStud=stud;
		emf = Persistence.createEntityManagerFactory("StudentskaSluzba");		
	    em = emf.createEntityManager();			   
	    em.getEntityManagerFactory().getCache().evictAll();
	    initialize();
	}
	
	public StudentProfil() {
	    initialize();
	}

	
	private void initialize() {
		frmInfoOStudentu = new JFrame();
		frmInfoOStudentu.setTitle("Informacije o studentu");
		frmInfoOStudentu.getContentPane().setBackground(Color.WHITE);
		frmInfoOStudentu.getContentPane().setForeground(Color.WHITE);
		frmInfoOStudentu.setBounds(100, 100, 417, 298);
		frmInfoOStudentu.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmInfoOStudentu.getContentPane().setLayout(null);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frmInfoOStudentu.setLocation(dim.width/2-frmInfoOStudentu.getSize().width/2, dim.height/2-frmInfoOStudentu.getSize().height/2);
		
		JLabel label = new JLabel("");
		String path = currentStud.getKorisnik().getClasspath();
		if (path==null) {
		label.setIcon(new ImageIcon("C:\\\\Users\\\\Mustafa\\\\Desktop\\\\defaultIcon.jpg"));	
		}
		else {
		label.setIcon(new ImageIcon(path));
		}
		
		label.setBounds(12, 27, 161, 163);
		frmInfoOStudentu.getContentPane().add(label);
		
	   
		
		JLabel label_1 = new JLabel("Ime: " + currentStud.getIme());
		label_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		label_1.setBounds(185, 13, 235, 40);
		frmInfoOStudentu.getContentPane().add(label_1);
		
		JLabel label_2 = new JLabel("Prezime: "+ currentStud.getPrezime());
		label_2.setFont(new Font("Tahoma", Font.BOLD, 15));
		label_2.setBounds(185, 38, 235, 40);
		frmInfoOStudentu.getContentPane().add(label_2);
		
		JLabel label_3 = new JLabel("Broj indexa: "+ currentStud.getBr_indeksa());
		label_3.setFont(new Font("Tahoma", Font.BOLD, 15));
		label_3.setBounds(185, 80, 235, 40);
		frmInfoOStudentu.getContentPane().add(label_3);
		
		JLabel label_4 = new JLabel("Adresa: "+ currentStud.getAdresa());
		label_4.setFont(new Font("Tahoma", Font.BOLD, 15));
		label_4.setBounds(185, 174, 235, 40);
		frmInfoOStudentu.getContentPane().add(label_4);
		
		JLabel label_5 = new JLabel();
		if (currentStud.getSemestar()==null) 
		label_5 = new JLabel("Semestar: Nema semestra");
		else
		label_5 = new JLabel("Semestar: "+ currentStud.getSemestar());
		label_5.setFont(new Font("Tahoma", Font.BOLD, 15));
		label_5.setBounds(185, 105, 235, 40);
		frmInfoOStudentu.getContentPane().add(label_5);
		
		JLabel label_6 = new JLabel("Broj telefona: "+ currentStud.getTelefon());
		label_6.setFont(new Font("Tahoma", Font.BOLD, 15));
		label_6.setBounds(185, 199, 235, 40);
		frmInfoOStudentu.getContentPane().add(label_6);
		
		JLabel label_7 = new JLabel("Usmjerenje: "+ currentStud.getUsmjerenje().getSkracenica());
		label_7.setFont(new Font("Tahoma", Font.BOLD, 15));
		label_7.setBounds(185, 133, 235, 40);
		frmInfoOStudentu.getContentPane().add(label_7);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(128, 0, 0));
		panel.setBounds(0, 0, 399, 17);
		frmInfoOStudentu.getContentPane().add(panel);
	}
}
