package views;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;

import javax.imageio.ImageIO;
import javax.persistence.*;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

import models.Korisnik;
import models.Nastavnik;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.InputStream;
import java.util.List;
import javax.swing.JPanel;

public class NastavnikHome {

	public JFrame frmNastavnikHome;
	private Nastavnik currentUser;
	private EntityManagerFactory emf;
	private EntityManager em;
	private String path;
	
		
	public NastavnikHome(Korisnik user) 
	{
		emf = Persistence.createEntityManagerFactory("StudentskaSluzba");		
	    em = emf.createEntityManager();			   
	    em.getEntityManagerFactory().getCache().evictAll();
	    Query q = em.createQuery("select t from Nastavnik t");	    
	    List<Nastavnik> lista = q.getResultList();	    
	    for(Nastavnik nast : lista)
	    {
	    	if(nast.getKorisnik().getId()==user.getId())
	    	{
	    		currentUser=nast;
	    		break;
	    	}
	    }
		initialize();
	}

	
	private void initialize() {
		frmNastavnikHome = new JFrame();
		frmNastavnikHome.getContentPane().setForeground(new Color(255, 255, 255));
		frmNastavnikHome.setResizable(false);
		frmNastavnikHome.getContentPane().setBackground(Color.WHITE);
		frmNastavnikHome.setTitle("Nastavnik");
		frmNastavnikHome.setBackground(Color.WHITE);
		frmNastavnikHome.setBounds(100, 100, 450, 300);
		frmNastavnikHome.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmNastavnikHome.setSize(new Dimension(750, 500));
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frmNastavnikHome.setLocation(dim.width/2-frmNastavnikHome.getSize().width/2, dim.height/2-frmNastavnikHome.getSize().height/2);
		JButton btnSekcije = new JButton("Sekcije");
		btnSekcije.setBounds(547, 423, 88, 29);
		btnSekcije.setBackground(new Color(128, 0, 0));
		btnSekcije.setForeground(new Color(255, 255, 255));
		btnSekcije.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				Sekcije window = new Sekcije(currentUser);
				window.currentUser=currentUser;
	    		frmNastavnikHome.dispose();
	    		window.frmSekcije.setVisible(true);
			}			
		});
		frmNastavnikHome.getContentPane().setLayout(null);
		frmNastavnikHome.getContentPane().add(btnSekcije);
		try {
			InputStream stream = this.getClass().getResourceAsStream("/logo.jpg");
			ImageIcon image;
			image = new ImageIcon(ImageIO.read(stream));
		} catch (Exception e1) 
		{
			
		}
		
		JButton btnOdjava = new JButton("Odjava");
		btnOdjava.setBounds(647, 423, 85, 29);
		btnOdjava.setForeground(new Color(255, 255, 255));
		btnOdjava.setBackground(new Color(128, 0, 0));
		frmNastavnikHome.getContentPane().add(btnOdjava);
		
		
		
		JButton btnIspiti = new JButton("");
		btnIspiti.setBounds(216, 220, 164, 159);
		btnIspiti.setBackground(new Color(255, 255, 255));
		btnIspiti.setForeground(new Color(255, 255, 255));
		Image ispiti= new ImageIcon (this.getClass().getResource("/ispiti.png")).getImage();
		btnIspiti.setIcon(new ImageIcon(ispiti));
		Border emptyBorder4 = BorderFactory.createEmptyBorder();
		btnIspiti.setBorder(emptyBorder4);
		btnIspiti.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				Ispiti window = new Ispiti(currentUser);
				window.currentUser=currentUser;
	    		frmNastavnikHome.dispose();
	    		window.frmIspiti.setVisible(true);
			}			
		});
		frmNastavnikHome.getContentPane().add(btnIspiti);
		
		JButton btnPredispitneAktivnosti = new JButton("");
		btnPredispitneAktivnosti.setBounds(392, 220, 164, 159);
		Image predispiti= new ImageIcon (this.getClass().getResource("/predispit.png")).getImage();
		btnPredispitneAktivnosti.setIcon(new ImageIcon(predispiti));
		Border emptyBorder5 = BorderFactory.createEmptyBorder();
		btnPredispitneAktivnosti.setBorder(emptyBorder5);
		btnPredispitneAktivnosti.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				Predispiti window = new Predispiti(currentUser);
				window.currentUser=currentUser;
	    		frmNastavnikHome.dispose();
	    		window.frmIspiti.setVisible(true);
			}			
		});
		frmNastavnikHome.getContentPane().add(btnPredispitneAktivnosti);
		
		JButton btnRezultati = new JButton("");
		btnRezultati.setBounds(568, 220, 164, 159);
		Image rezultati= new ImageIcon (this.getClass().getResource("/rezultati.png")).getImage();
		btnRezultati.setIcon(new ImageIcon(rezultati));
		Border emptyBorder6 = BorderFactory.createEmptyBorder();
		btnRezultati.setBorder(emptyBorder6);
		btnRezultati.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				NastavnikRezultati window = new NastavnikRezultati(currentUser);
				window.currentUser=currentUser;
	    		frmNastavnikHome.dispose();
	    		window.frmNastavnikRezultati.setVisible(true);
			}			
		});
		frmNastavnikHome.getContentPane().add(btnRezultati);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(0, 0, 744, 180);
		Image naslovnica= new ImageIcon (this.getClass().getResource("/cover.jpg")).getImage();
		lblNewLabel.setIcon(new ImageIcon(naslovnica));
		frmNastavnikHome.getContentPane().add(lblNewLabel);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 179, 205, 286);
		frmNastavnikHome.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblImeIPrezime = new JLabel(currentUser.getIme() + " " + currentUser.getPrezime());
		lblImeIPrezime.setBounds(22, 13, 144, 18);
		panel.add(lblImeIPrezime);
		lblImeIPrezime.setFont(new Font("Lucida Grande", Font.BOLD, 13));
		lblImeIPrezime.setForeground(Color.DARK_GRAY);
		
		JLabel lblNewLabel_1 = new JLabel("");
		em.getTransaction().begin();
		if ((currentUser.getKorisnik()).getClasspath()==null){
			path = "C:\\\\Users\\\\Mustafa\\\\Desktop\\\\defaultIcon.jpg";
			
			lblNewLabel_1.setIcon(new ImageIcon(path));
		}
		else
		{
			path = currentUser.getKorisnik().getClasspath();
			lblNewLabel_1.setIcon(new ImageIcon(path));
		}
		em.persist(currentUser);
		em.getTransaction().commit();
		lblNewLabel_1.setBounds(22, 44, 158, 155);
		panel.add(lblNewLabel_1);
		
		JButton btnSlika = new JButton("slika");
		btnSlika.setForeground(new Color(255, 255, 255));
		btnSlika.setBackground(new Color(128, 0, 0));
		btnSlika.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				em.getTransaction().begin();
				JFileChooser chooser = new JFileChooser();
				chooser.showOpenDialog(null);
				File f = chooser.getSelectedFile();
				String filename = f.getAbsolutePath();
				currentUser.getKorisnik().setClasspath(filename);
				ImageIcon icon=new ImageIcon(filename);
				lblNewLabel.setIcon(icon);
				em.persist(currentUser);
				em.getTransaction().commit();
			}
		});
		btnSlika.setBounds(56, 244, 85, 29);
		panel.add(btnSlika);
		
		JLabel lblOdaberiteOpciju = new JLabel("Odaberite opciju");
		lblOdaberiteOpciju.setBounds(392, 183, 192, 35);
		lblOdaberiteOpciju.setFont(new Font("Tahoma", Font.BOLD, 18));
		frmNastavnikHome.getContentPane().add(lblOdaberiteOpciju);
		
		JLabel label = new JLabel("Ispiti");
		label.setFont(new Font("Tahoma", Font.BOLD, 16));
		label.setBounds(270, 392, 56, 16);
		frmNastavnikHome.getContentPane().add(label);
		
		JLabel label_1 = new JLabel("");
		label_1.setFont(new Font("Tahoma", Font.BOLD, 16));
		label_1.setBounds(437, 392, 85, 16);
		frmNastavnikHome.getContentPane().add(label_1);
		
		JLabel label_2 = new JLabel("Predispiti");
		label_2.setFont(new Font("Tahoma", Font.BOLD, 16));
		label_2.setBounds(437, 392, 85, 16);
		frmNastavnikHome.getContentPane().add(label_2);
		
		JLabel label_3 = new JLabel("Rezultati");
		label_3.setFont(new Font("Tahoma", Font.BOLD, 16));
		label_3.setBounds(614, 392, 85, 16);
		frmNastavnikHome.getContentPane().add(label_3);
		
		btnOdjava.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				Login window = new Login();
				frmNastavnikHome.dispose();
	    		window.frmPrijavaKorisnika.setVisible(true);
			}
		});
	}
}
