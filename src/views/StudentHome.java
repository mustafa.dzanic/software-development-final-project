package views;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.InputStream;
import java.util.List;

import javax.imageio.ImageIO;
import javax.persistence.*;
import javax.swing.*;
import javax.swing.border.Border;

import com.sun.java.util.*;

import models.Korisnik;
import models.Student;

public class StudentHome {

	String path;
	public JFrame frmStudentHome;
	private Student currentUser;
	private EntityManagerFactory emf;
	private EntityManager em;
		
	public StudentHome(Korisnik user) 
	{
		emf = Persistence.createEntityManagerFactory("StudentskaSluzba");		
	    em = emf.createEntityManager();			   
	    em.getEntityManagerFactory().getCache().evictAll();
	    Query q = em.createQuery("select t from Student t");	    
	    List<Student> lista = q.getResultList();	    
	    for(Student stud : lista)
	    {
	    	if(stud.getKorisnik().getId()==user.getId())
	    	{
	    		currentUser=stud;
	    		break;
	    	}
	    }	    
		initialize();
	}

	
	private void initialize() {
		frmStudentHome = new JFrame();
		frmStudentHome.getContentPane().setForeground(new Color(255, 255, 255));
		frmStudentHome.setResizable(false);
		frmStudentHome.getContentPane().setBackground(Color.WHITE);
		frmStudentHome.setTitle("Student");
		frmStudentHome.setBackground(Color.WHITE);
		frmStudentHome.setBounds(100, 100, 450, 300);
		frmStudentHome.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmStudentHome.getContentPane().setLayout(null);
		frmStudentHome.setSize(new Dimension(742, 500));
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frmStudentHome.setLocation(dim.width/2-frmStudentHome.getSize().width/2, dim.height/2-frmStudentHome.getSize().height/2);
		JButton btnUpisSemestra = new JButton("Upis semestra");
		btnUpisSemestra.setForeground(new Color(255, 255, 255));
		btnUpisSemestra.setBackground(new Color(128, 0, 0));
		btnUpisSemestra.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if(currentUser.getSemestar()!=null && currentUser.getSemestar().equals("VIII"))
				{
					JOptionPane.showMessageDialog(null, "Već ste upisani na " + currentUser.getSemestar() + " (zadnji) semestar!", "Greška", JOptionPane.INFORMATION_MESSAGE);					    	
				}
				else
				{
				UpisSemestra window = new UpisSemestra(currentUser);
				frmStudentHome.dispose();
	    		window.frmUpisSemestra.setVisible(true);
				}
			}			
		});
		btnUpisSemestra.setBounds(492, 423, 127, 29);
		frmStudentHome.getContentPane().add(btnUpisSemestra);
		try {
			InputStream stream = this.getClass().getResourceAsStream("/logo.jpg");
			ImageIcon image;
			image = new ImageIcon(ImageIO.read(stream));
		} catch (Exception e1) 
		{
			
		}
		
		JButton btnOdjava = new JButton("Odjava");
		btnOdjava.setForeground(new Color(255, 255, 255));
		btnOdjava.setBackground(new Color(128, 0, 0));
		btnOdjava.setBounds(631, 423, 85, 29);
		frmStudentHome.getContentPane().add(btnOdjava);
		
		JLabel lblNastavnikiProfil = new JLabel("Odaberite opciju");
		lblNastavnikiProfil.setForeground(new Color(0, 0, 0));
		lblNastavnikiProfil.setFont(new Font("Calibri", Font.PLAIN, 24));
		lblNastavnikiProfil.setBounds(372, 183, 228, 28);
		frmStudentHome.getContentPane().add(lblNastavnikiProfil);
		
		
		
		JButton btnPrijavaIspita = new JButton("");
		Image ispiti= new ImageIcon (this.getClass().getResource("/ispiti.png")).getImage();
		btnPrijavaIspita.setIcon(new ImageIcon(ispiti));
		Border emptyBorder = BorderFactory.createEmptyBorder();
		btnPrijavaIspita.setBorder(emptyBorder);
		btnPrijavaIspita.setBackground(new Color(255, 255, 255));
		btnPrijavaIspita.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				PrijavaIspita window = new PrijavaIspita(currentUser);
				frmStudentHome.dispose();
	    		window.frmPrijavaIspita.setVisible(true);
			}			
		});
		btnPrijavaIspita.setBounds(205, 220, 165, 159);
		frmStudentHome.getContentPane().add(btnPrijavaIspita);
		
		JButton btnPrijavaPredispita = new JButton("");
		Image predispiti= new ImageIcon (this.getClass().getResource("/predispit.png")).getImage();
		btnPrijavaPredispita.setIcon(new ImageIcon(predispiti));
		Border emptyBorder3 = BorderFactory.createEmptyBorder();
		btnPrijavaPredispita.setBorder(emptyBorder3);
		btnPrijavaPredispita.setBackground(new Color(255, 255, 255));
		btnPrijavaPredispita.setForeground(new Color(255, 255, 255));
		btnPrijavaPredispita.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				PrijavaPredispita window = new PrijavaPredispita(currentUser);
				frmStudentHome.dispose();
	    		window.frmPrijavaPredispita.setVisible(true);
			}			
		});
		
		btnPrijavaPredispita.setBounds(382, 220, 165, 159);
		frmStudentHome.getContentPane().add(btnPrijavaPredispita);
		
		JButton btnRezultati = new JButton("");
		Image rezultati= new ImageIcon (this.getClass().getResource("/rezultati.png")).getImage();
		btnRezultati.setIcon(new ImageIcon(rezultati));
		Border emptyBorder2 = BorderFactory.createEmptyBorder();
		btnRezultati.setBorder(emptyBorder2);
		btnRezultati.setBackground(new Color(255, 255, 255));
		btnRezultati.setForeground(new Color(255, 255, 255));
		btnRezultati.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				StudentRezultati window = new StudentRezultati(currentUser);
				frmStudentHome.dispose();
	    		window.frmRezultatiIspita.setVisible(true);
			}			
		});
		
		btnRezultati.setBounds(559, 220, 165, 159);
		frmStudentHome.getContentPane().add(btnRezultati);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(230, 230, 250));
		panel.setBorder(null);
		panel.setBounds(0, 169, 193, 296);
		frmStudentHome.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblImeIPrezime = new JLabel(currentUser.getIme() + " " + currentUser.getPrezime());
		lblImeIPrezime.setBounds(12, 23, 159, 18);
		panel.add(lblImeIPrezime);
		lblImeIPrezime.setFont(new Font("Arial", Font.BOLD, 15));
		lblImeIPrezime.setForeground(Color.DARK_GRAY);
		
		JLabel lblNewLabel = new JLabel("");
		
		
		em.getTransaction().begin();
		if ((currentUser.getKorisnik()).getClasspath()==null){
			path = "C:\\\\Users\\\\Mustafa\\\\Desktop\\\\defaultIcon.jpg";
			
			lblNewLabel.setIcon(new ImageIcon(path));
		}
		else
		{
			path = currentUser.getKorisnik().getClasspath();
			lblNewLabel.setIcon(new ImageIcon(path));
		}
		em.persist(currentUser);
		em.getTransaction().commit();
		
		lblNewLabel.setBounds(16, 54, 159, 157);
		panel.add(lblNewLabel);
		
		JButton btnSlika = new JButton("Slika");
		btnSlika.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				em.getTransaction().begin();
				JFileChooser chooser = new JFileChooser();
				chooser.showOpenDialog(null);
				File f = chooser.getSelectedFile();
				String filename = f.getAbsolutePath();
				currentUser.getKorisnik().setClasspath(filename);
				ImageIcon icon=new ImageIcon(filename);
				lblNewLabel.setIcon(icon);
				em.persist(currentUser);
				em.getTransaction().commit();
			}
		});
		btnSlika.setForeground(new Color(255, 250, 240));
		btnSlika.setBackground(new Color(128, 0, 0));
		btnSlika.setBounds(48, 254, 85, 29);
		panel.add(btnSlika);
		
		JLabel label = new JLabel("");
		label.setBounds(0, 0, 736, 169);
		Image naslovnica= new ImageIcon (this.getClass().getResource("/cover.jpg")).getImage();
		label.setIcon(new ImageIcon(naslovnica));
		frmStudentHome.getContentPane().add(label);
		
		JLabel lblIspiti = new JLabel("Ispiti");
		lblIspiti.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblIspiti.setBounds(264, 388, 56, 16);
		frmStudentHome.getContentPane().add(lblIspiti);
		
		JLabel lblPredispiti = new JLabel("Predispiti");
		lblPredispiti.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblPredispiti.setBounds(424, 388, 85, 16);
		frmStudentHome.getContentPane().add(lblPredispiti);
		
		JLabel lblRezultati = new JLabel("Rezultati");
		lblRezultati.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblRezultati.setBounds(606, 388, 85, 16);
		frmStudentHome.getContentPane().add(lblRezultati);
		
		btnOdjava.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				Login window = new Login();
				frmStudentHome.dispose();
	    		window.frmPrijavaKorisnika.setVisible(true);
			}
		});
	}
}
