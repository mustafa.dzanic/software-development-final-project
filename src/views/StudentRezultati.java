package views;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;

import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;

import models.RezultatIspita;
import models.Student;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.DefaultComboBoxModel;

import static ch.lambdaj.Lambda.*;

import org.hamcrest.Matchers;
import static ch.lambdaj.Lambda.*;

public class StudentRezultati {

	public JFrame frmRezultatiIspita;
	private EntityManagerFactory emf;
	private EntityManager em;
	private List<RezultatIspita> rezultati;
	private List<RezultatIspita> rezultatiSource;
	private Student currentUser;
	private JTable tableRezultati;
	private JScrollPane scrollPaneRezultati;
	private JTextField txtFilter;
	private JComboBox cmbFilter;
	public StudentRezultati(Student user) {
		currentUser=user;
		emf = Persistence.createEntityManagerFactory("StudentskaSluzba");		
	    em = emf.createEntityManager();			   
	    em.getEntityManagerFactory().getCache().evictAll();
	    initialize();
	}

	private void OsvjeziTabelu(boolean reload)
	{
		if(reload)
		{
		rezultati=new ArrayList<RezultatIspita>();
		rezultati.addAll(currentUser.getRezultati());
	    rezultatiSource=rezultati;
		}
		Object columnNamesRezultati[] = { "Sekcija", "Datum", "Vrijeme", "Sala", "Bodovi"};
        Object[][] rowDataRezultati = new Object[rezultati.size()][5];
        int j=0;
	    for(RezultatIspita r : rezultati)
	    {
	    	rowDataRezultati[j][0]=r.getIspit().getSekcija().toString();
	    	rowDataRezultati[j][1]=r.getIspit().getDatum();
	    	rowDataRezultati[j][2]=r.getIspit().getVrijeme();
	    	rowDataRezultati[j][3]=r.getIspit().getSala();
	    	rowDataRezultati[j][4]=r.getBodovi() + " / " + r.getIspit().getBodovi();	    	
	    	j++;
	    }
	    
	    tableRezultati.setModel(new DefaultTableModel(rowDataRezultati,columnNamesRezultati));
	    
	    tableRezultati.getColumnModel().getColumn(1).setMinWidth(90);
	    tableRezultati.getColumnModel().getColumn(1).setMaxWidth(90);
	    tableRezultati.getColumnModel().getColumn(1).setPreferredWidth(90);
	    
	    tableRezultati.getColumnModel().getColumn(2).setMinWidth(70);
	    tableRezultati.getColumnModel().getColumn(2).setMaxWidth(70);
	    tableRezultati.getColumnModel().getColumn(2).setPreferredWidth(70);
	    
	    tableRezultati.getColumnModel().getColumn(3).setMinWidth(70);
	    tableRezultati.getColumnModel().getColumn(3).setMaxWidth(70);
	    tableRezultati.getColumnModel().getColumn(3).setPreferredWidth(70);
	    
	    tableRezultati.getColumnModel().getColumn(4).setMinWidth(80);
	    tableRezultati.getColumnModel().getColumn(4).setMaxWidth(80);
	    tableRezultati.getColumnModel().getColumn(4).setPreferredWidth(80);
	    
	}

	
	private void initialize() {
		frmRezultatiIspita = new JFrame();
		frmRezultatiIspita.setResizable(false);
		frmRezultatiIspita.getContentPane().setBackground(Color.WHITE);
		frmRezultatiIspita.setTitle("Rezultati ispita");
		frmRezultatiIspita.setBounds(100, 100, 450, 300);
		frmRezultatiIspita.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmRezultatiIspita.getContentPane().setLayout(null);
		frmRezultatiIspita.setSize(new Dimension(583, 481));
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frmRezultatiIspita.setLocation(dim.width/2-frmRezultatiIspita.getSize().width/2, dim.height/2-frmRezultatiIspita.getSize().height/2);
		
		Object columnNamesRezultati[] = { "Sekcija", "Datum", "Vrijeme", "Vrsta ispita", "Bodovi"};
        Object[][] rowDataRezultati = new Object[0][5];		       
        tableRezultati = new JTable(rowDataRezultati, columnNamesRezultati);
		OsvjeziTabelu(true);
		
        scrollPaneRezultati = new JScrollPane(tableRezultati);
        frmRezultatiIspita.getContentPane().add(scrollPaneRezultati, BorderLayout.CENTER);
        scrollPaneRezultati.setBounds(12, 13, 553, 350);
        
      
	
	
	    
        
	    JButton btnPovratak = new JButton("Povratak");
	    btnPovratak.setForeground(new Color(255, 255, 255));
	    btnPovratak.setBackground(new Color(128, 0, 0));
	    btnPovratak.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
        	  StudentHome window = new StudentHome(currentUser.getKorisnik());
				frmRezultatiIspita.dispose();
	    		window.frmStudentHome.setVisible(true);
          }
        });

        btnPovratak.setBounds(475, 404, 90, 29);
        frmRezultatiIspita.getContentPane().add(btnPovratak);
        
        JLabel label = new JLabel("Filter po:");
        label.setBounds(18, 372, 58, 16);
        frmRezultatiIspita.getContentPane().add(label);
        
        cmbFilter = new JComboBox();
        cmbFilter.setBackground(new Color(128, 0, 0));
        cmbFilter.setForeground(new Color(255, 255, 255));
        cmbFilter.addActionListener (new ActionListener () {
	        public void actionPerformed(ActionEvent e) {
	        	txtFilter.setText("");
	            OsvjeziTabelu(true);
	        }
	    });
        
        cmbFilter.setModel(new DefaultComboBoxModel(new String[] {"predmet", "sekcija", "datum", "sala"}));
        cmbFilter.setBounds(83, 367, 158, 27);
        frmRezultatiIspita.getContentPane().add(cmbFilter);
        
        txtFilter = new JTextField();
        txtFilter.getDocument().addDocumentListener(new DocumentListener()
        {
            public void changedUpdate(DocumentEvent arg0) 
            {
           
            }
            public void insertUpdate(DocumentEvent arg0) 
            {
            	if(cmbFilter.getSelectedIndex()==0)
            		rezultati = select(rezultatiSource, having(on(RezultatIspita.class).getIspit().getSekcija().getPredmet().getNaziv(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==1)
            		rezultati = select(rezultatiSource, having(on(RezultatIspita.class).getIspit().getSekcija().getNaziv(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==2)
            		rezultati = select(rezultatiSource, having(on(RezultatIspita.class).getIspit().getDatum(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==3)
            		rezultati = select(rezultatiSource, having(on(RezultatIspita.class).getIspit().getSala(), Matchers.containsString(txtFilter.getText())));
            	OsvjeziTabelu(false);
            }

            public void removeUpdate(DocumentEvent arg0) 
            {
             	if(cmbFilter.getSelectedIndex()==0)
            		rezultati = select(rezultatiSource, having(on(RezultatIspita.class).getIspit().getSekcija().getPredmet().getNaziv(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==1)
            		rezultati = select(rezultatiSource, having(on(RezultatIspita.class).getIspit().getSekcija().getNaziv(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==2)
            		rezultati = select(rezultatiSource, having(on(RezultatIspita.class).getIspit().getDatum(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==3)
            		rezultati = select(rezultatiSource, having(on(RezultatIspita.class).getIspit().getSala(), Matchers.containsString(txtFilter.getText())));
            	OsvjeziTabelu(false);
           }           
        });

        txtFilter.setColumns(10);
        txtFilter.setBounds(253, 366, 312, 28);
        frmRezultatiIspita.getContentPane().add(txtFilter);

	}

}
