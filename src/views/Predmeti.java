package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import static ch.lambdaj.Lambda.*;
import javax.imageio.ImageIO;
import javax.persistence.*;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import org.hamcrest.Matchers;

import models.Korisnik;
import models.Predmet;
import models.Usmjerenje;

public class Predmeti {

	public JFrame frmPredmeti;
	private EntityManagerFactory emf;
	private EntityManager em;
	private JTable table;
	private JTable tableUsmjerenja;
	private JTable tablePreduslovi;
	private JScrollPane scrollPane;
	private JScrollPane scrollPaneUsmjerenja;
	private JScrollPane scrollPanePreduslovi;
	private int selected;
	public Korisnik currentUser;
	private List<Usmjerenje> usmjerenja;
	private List<Predmet> predmeti;
	private List<Predmet> predmetiSource;
	private JTextField txtNazivPredmeta;
	private JTextField txtKratakNaziv;
	private boolean izmjena=false;
	private JButton btnObrisati;
	private JButton btnIzmijeniti;
	private JButton btnDodati;
	private JLabel lblFilterPoNazivu;
	private JTextField txtFilter;
	private JLabel label;
	private void OsvjeziTabelu(boolean reload)
	{
		Query q = em.createQuery("select t from Predmet t");
	    
		if(reload)
		{
			predmeti = q.getResultList();
	    	predmetiSource=predmeti;
		}
        Object columnNames[] = { "ID", "Naziv predmeta", "Kratak naziv", "Usmjerenja"};
        String[][] rowData = new String[predmeti.size()][4];
        
        int i=0;
	    for(Predmet pr : predmeti)
	    {
	    	rowData[i][0]=new Integer(pr.getId()).toString();
	    	rowData[i][1]=pr.getNaziv();
	    	rowData[i][2]=pr.getSkracenica();
	    	rowData[i][3]=pr.getListUsmjerenja();
	    	i++;
	    }
	    			
	    table.setModel(new DefaultTableModel(rowData,columnNames));
	    table.getColumnModel().getColumn(0).setMinWidth(20);
        table.getColumnModel().getColumn(0).setMaxWidth(50);
        table.getColumnModel().getColumn(0).setPreferredWidth(35);
        
        table.getColumnModel().getColumn(2).setMinWidth(80);
        table.getColumnModel().getColumn(2).setMaxWidth(80);
        table.getColumnModel().getColumn(2).setPreferredWidth(80);
        table.getColumnModel().getColumn(3).setMinWidth(100);
        table.getColumnModel().getColumn(3).setMaxWidth(100);
        table.getColumnModel().getColumn(3).setPreferredWidth(100);
                
        OsvjeziPreduslove();                        
	}
	
	public Predmeti() {
		emf = Persistence.createEntityManagerFactory("StudentskaSluzba");
	    em = emf.createEntityManager();
	    em.getEntityManagerFactory().getCache().evictAll();
	    Query q = em.createQuery("select t from Usmjerenje t");	    
	    usmjerenja = q.getResultList();
	    q = em.createQuery("select t from Predmet t");	    
	    predmeti = q.getResultList();
		initialize();
	}
	
	private void OcistiPolja()
	{
		btnIzmijeniti.setText("Izmijeniti");
  		 btnDodati.setText("Dodati");
  		 btnObrisati.setEnabled(true);
  		 txtNazivPredmeta.setText("");
  		 txtKratakNaziv.setText("");
  		 SetSelectedUsmjerenja(null);
  		 SetSelectedPreduslovi(null);
	}
	
	private void SetSelectedUsmjerenja(Collection<Usmjerenje> collection)
	{
			for(int i=0; i<tableUsmjerenja.getRowCount(); i++)
    	    {
    	    	tableUsmjerenja.setValueAt(false, i, 2);
    	    }
		if(collection!=null)
		{
			ArrayList<Usmjerenje> list=new ArrayList<Usmjerenje>();
			list.addAll(collection);
			
			for(int i=0; i<list.size(); i++)
			{
				if(usmjerenja.contains(list.get(i)))
					tableUsmjerenja.setValueAt(true, usmjerenja.indexOf(list.get(i)), 2);
			}
		}
	}
	
	private void SetSelectedPreduslovi(Collection<Predmet> collection)
	{
			for(int i=0; i<tablePreduslovi.getRowCount(); i++)
    	    {
    	    	tablePreduslovi.setValueAt(false, i, 2);
    	    }
		if(collection!=null)
		{
			ArrayList<Predmet> list=new ArrayList<Predmet>();
			list.addAll(collection);
			
			for(int i=0; i<list.size(); i++)
			{
				if(predmeti.contains(list.get(i)))
					tablePreduslovi.setValueAt(true, predmeti.indexOf(list.get(i)), 2);
			}
		}
	}
	
	private void OsvjeziPreduslove()
	{
		Object columnNamesPreduslovi[] = { "ID", "Predmet", "Uključen"};        
		int j=0;
		Object[][] rowDataPreduslovi = new Object[predmetiSource.size()][3];
	    for(Predmet p : predmetiSource)
	    {
	    	rowDataPreduslovi[j][0]=new Integer(p.getId()).toString();
	    	rowDataPreduslovi[j][1]=p.getNaziv();
	    	rowDataPreduslovi[j][2]=false;	    	
	    	j++;
	    }
	    tablePreduslovi.setModel(new DefaultTableModel(rowDataPreduslovi,columnNamesPreduslovi));
	    tablePreduslovi.getColumnModel().getColumn(0).setMinWidth(20);
	    tablePreduslovi.getColumnModel().getColumn(0).setMaxWidth(50);
	    tablePreduslovi.getColumnModel().getColumn(0).setPreferredWidth(35);
        
	    tablePreduslovi.getColumnModel().getColumn(2).setMinWidth(60);
	    tablePreduslovi.getColumnModel().getColumn(2).setMaxWidth(60);
	    tablePreduslovi.getColumnModel().getColumn(2).setPreferredWidth(60);
        
	}
	
	boolean tryParseInt(String value)  
	{  
	     try  
	     {  
	         Integer.parseInt(value);  
	         return true;  
	      } catch(NumberFormatException nfe)  
	      {  
	          return false;  
	      }  
	}
	
	private void initialize() {
		frmPredmeti = new JFrame();
		frmPredmeti.setResizable(false);
		frmPredmeti.getContentPane().setBackground(Color.WHITE);
		frmPredmeti.setTitle("Dodavanje novog predmeta\r\n");
		frmPredmeti.setBounds(100, 100, 450, 300);
		frmPredmeti.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frmPredmeti.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Naziv predmeta:");
		lblNewLabel.setBounds(18, 209, 163, 16);
		frmPredmeti.getContentPane().add(lblNewLabel);
		
		txtNazivPredmeta = new JTextField();
		txtNazivPredmeta.setBounds(132, 203, 163, 28);
		frmPredmeti.getContentPane().add(txtNazivPredmeta);
		txtNazivPredmeta.setColumns(50);
		
		Object columnNames[] = { "ID", "Naziv predmeta", "Kratak naziv"};
        String[][] rowData = new String[0][2];
        table = new JTable(rowData, columnNames) {
		    @Override
		    public boolean isCellEditable(int row, int column) {
		       return false;
		    }
		};
        scrollPane = new JScrollPane(table);
        scrollPane.setBounds(326, 203, 405, 289);
        frmPredmeti.getContentPane().add(scrollPane);
        
        JButton btnPovratak = new JButton("Povratak");
        btnPovratak.setBackground(new Color(128, 0, 0));
        btnPovratak.setForeground(new Color(255, 255, 255));
        btnPovratak.setBounds(641, 564, 90, 29);
        frmPredmeti.getContentPane().add(btnPovratak);
        btnPovratak.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				AdminPanel window = new AdminPanel(currentUser);
				frmPredmeti.dispose();
	    		window.frmAdministrator.setVisible(true);
			}
		});
        
        btnDodati = new JButton("Dodati");
        btnDodati.setForeground(new Color(255, 255, 255));
        btnDodati.setBackground(new Color(128, 0, 0));
        btnDodati.setBounds(0, 564, 102, 38);
        btnDodati.addActionListener(new ActionListener()
        {
        @Override
          public void actionPerformed(ActionEvent e)
          {
        	Predmet pr;
				if (izmjena == false)

					pr = new Predmet();
				else
					pr = predmeti.get(selected);
			if(txtNazivPredmeta.getText().length()<1)
        	  {
        		  JOptionPane.showMessageDialog(null, "Niste unijeli naziv predmeta!", "Greška", JOptionPane.ERROR_MESSAGE);
        		  txtNazivPredmeta.requestFocus();
        	  }
        	  else if(txtKratakNaziv.getText().length()<1)
        	  {
        		  JOptionPane.showMessageDialog(null, "Niste unijeli kratak naziv predmeta!", "Greška", JOptionPane.ERROR_MESSAGE);
        		  txtKratakNaziv.requestFocus();
        	  }
        	
        	  else
        	  {
        	  	em.getTransaction().begin();        	    
        	    
        	    pr.setNaziv(txtNazivPredmeta.getText());
        	    pr.setSkracenica(txtKratakNaziv.getText().substring(0, txtKratakNaziv.getText().length()>5 ? 5 : txtKratakNaziv.getText().length()));
        	    ArrayList<Usmjerenje> odabranaUsmjerenja=new ArrayList<Usmjerenje>();
        	    for(int i=0; i<tableUsmjerenja.getRowCount(); i++)
        	    {
        	    	if(tableUsmjerenja.getValueAt(i, 2).equals(true))
        	    	{        	    		
        	    		odabranaUsmjerenja.add(usmjerenja.get(i));
        	    	}        	    	
        	    }        	   
        	    pr.setUsmjerenja(odabranaUsmjerenja);
        	    
        	    ArrayList<Predmet> odabraniPreduslovi=new ArrayList<Predmet>();
        	    for(int i=0; i<tablePreduslovi.getRowCount(); i++)
        	    {
        	    	if(tablePreduslovi.getValueAt(i, 2).equals(true))
        	    	{     
        	    		if(izmjena==false || (izmjena==true && pr.getId()!=predmeti.get(i).getId()))
        	    			odabraniPreduslovi.add(predmeti.get(i));
        	    	}        	    	
        	    }        	   
        	    pr.setPreduslovi(odabraniPreduslovi);
        	    for(Usmjerenje us : pr.getUsmjerenja())
        	    {
        	    	if(!us.getPredmeti().contains(pr))
        	    		us.getPredmeti().add(pr);
        	    }
        	    em.persist(pr);
        	    em.getTransaction().commit();
        	    
        	    OsvjeziTabelu(true);        
        	    OcistiPolja();
        	  }        	
          }
        });
        frmPredmeti.getContentPane().add(btnDodati);

        Object columnNamesUsmjerenja[] = { "ID", "Usmjerenje", "Uključen"};        
		int j=0;
		Object[][] rowDataUsmjerenja = new Object[usmjerenja.size()][3];
	    for(Usmjerenje us : usmjerenja)
	    {
	    	rowDataUsmjerenja[j][0]=new Integer(us.getId()).toString();
	    	rowDataUsmjerenja[j][1]=us.getNaziv();
	    	rowDataUsmjerenja[j][2]=false;
	    	j++;
	    }
	    
	    tableUsmjerenja = new JTable(rowDataUsmjerenja, columnNamesUsmjerenja) {
		    @Override
		    public boolean isCellEditable(int row, int column) {
		       if(column==2)
		    	   return true;
		       else
		    	   return false;
		    }
		    public Class getColumnClass(int column) {
                switch (column) {
                    case 0:
                        return String.class;
                    case 1:
                        return String.class;
                    default:
                        return Boolean.class;
                       
                }
		    }
		};
	        
        scrollPaneUsmjerenja = new JScrollPane(tableUsmjerenja);
        scrollPaneUsmjerenja.setBounds(18, 316, 277, 97);
        frmPredmeti.getContentPane().add(scrollPaneUsmjerenja);
        tableUsmjerenja.getColumnModel().getColumn(0).setMinWidth(20);
        tableUsmjerenja.getColumnModel().getColumn(0).setMaxWidth(50);
        tableUsmjerenja.getColumnModel().getColumn(0).setPreferredWidth(35);
        tableUsmjerenja.getColumnModel().getColumn(2).setMinWidth(60);
        tableUsmjerenja.getColumnModel().getColumn(2).setMaxWidth(60);
        tableUsmjerenja.getColumnModel().getColumn(2).setPreferredWidth(60);
        
        JLabel lblListaPredmeta = new JLabel("Usmjerenja kojim pripada:");
        lblListaPredmeta.setBounds(18, 296, 187, 16);
        frmPredmeti.getContentPane().add(lblListaPredmeta);
        
        JLabel lblPreduslovi = new JLabel("Preduslovi:");
        lblPreduslovi.setBounds(18, 316, 187, 16);
        frmPredmeti.getContentPane().add(lblPreduslovi);
                     
    	Object columnNamesPreduslovi[] = { "ID", "Predmet", "Uključen"};        
		j=0;
		Object[][] rowDataPreduslovi = new Object[predmeti.size()][3];
	    for(Predmet p : predmeti)
	    {
	    	rowDataPreduslovi[j][0]=new Integer(p.getId()).toString();
	    	rowDataPreduslovi[j][1]=p.getNaziv();
	    	rowDataPreduslovi[j][2]=false;	    	
	    	j++;
	    }
	
        tablePreduslovi = new JTable(rowDataPreduslovi, columnNamesPreduslovi) {
		    @Override
		    public boolean isCellEditable(int row, int column) {
		       if(column==2)
		    	   return true;
		       else
		    	   return false;
		    }
		    public Class getColumnClass(int column) {
                switch (column) {
                    case 0:
                        return String.class;
                    case 1:
                        return String.class;
                    default:
                        return Boolean.class;
                       
                }
		    }
		};		
				
        scrollPanePreduslovi = new JScrollPane(tablePreduslovi);
        scrollPanePreduslovi.setBounds(18, 437, 277, 97);
        frmPredmeti.getContentPane().add(scrollPanePreduslovi);
        
        lblFilterPoNazivu = new JLabel("Filter po nazivu predmeta:");
        lblFilterPoNazivu.setBounds(333, 518, 187, 16);
        frmPredmeti.getContentPane().add(lblFilterPoNazivu);
        
        txtFilter = new JTextField();
        txtFilter.setBounds(488, 506, 243, 28);
        txtFilter.getDocument().addDocumentListener(new DocumentListener()
        {

            public void changedUpdate(DocumentEvent arg0) 
            {
           
            }
            public void insertUpdate(DocumentEvent arg0) 
            {
            	predmeti = select(predmetiSource, having(on(Predmet.class).getNaziv(), Matchers.containsString(txtFilter.getText())));
            	OsvjeziTabelu(false);
            }

            public void removeUpdate(DocumentEvent arg0) 
            {
            	predmeti = select(predmetiSource, having(on(Predmet.class).getNaziv(), Matchers.containsString(txtFilter.getText())));
            	OsvjeziTabelu(false);
            }           
        });

        txtFilter.setColumns(10);
        frmPredmeti.getContentPane().add(txtFilter);
        
        JLabel lblPreduslovi_1 = new JLabel("Preduslovi");
        lblPreduslovi_1.setBounds(18, 419, 86, 16);
        frmPredmeti.getContentPane().add(lblPreduslovi_1);
        
        label = new JLabel("");
        label.setBounds(0, 0, 743, 180);
        Image naslovnica= new ImageIcon (this.getClass().getResource("/cover.jpg")).getImage();
		label.setIcon(new ImageIcon(naslovnica));
        
        frmPredmeti.getContentPane().add(label);
        
        JPanel panel = new JPanel();
        panel.setBounds(0, 181, 313, 421);
        frmPredmeti.getContentPane().add(panel);
        panel.setLayout(null);
        
        btnObrisati = new JButton("Obrisati");
        btnObrisati.setBounds(205, 383, 108, 38);
        panel.add(btnObrisati);
        btnObrisati.setForeground(new Color(255, 255, 255));
        btnObrisati.setBackground(new Color(128, 0, 0));
        
        btnIzmijeniti = new JButton("Izmijeniti");
        btnIzmijeniti.setBounds(101, 383, 108, 38);
        panel.add(btnIzmijeniti);
        btnIzmijeniti.setBackground(new Color(128, 0, 0));
        btnIzmijeniti.setForeground(new Color(255, 255, 255));
        
        JLabel lblKratakNazivdo = new JLabel("Kratak naziv (do 5 karaktera):");
        lblKratakNazivdo.setBounds(18, 64, 187, 16);
        panel.add(lblKratakNazivdo);
        
        txtKratakNaziv = new JTextField();
        txtKratakNaziv.setBounds(214, 58, 81, 28);
        panel.add(txtKratakNaziv);
        txtKratakNaziv.setColumns(5);
        btnIzmijeniti.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
        	 if(izmjena==false)
        	 {        		 
        	 if(selected>-1)
        	 {
        		 izmjena=true;
        		 btnIzmijeniti.setText("Odustati");
        		 btnDodati.setText("Potvrditi");
        		 btnObrisati.setEnabled(false);
        		 Predmet pr=predmeti.get(selected);
        		 txtNazivPredmeta.setText(pr.getNaziv());
        		 txtKratakNaziv.setText(pr.getSkracenica());
        		 SetSelectedUsmjerenja(pr.getUsmjerenja());
        		 SetSelectedPreduslovi(pr.getPreduslovi());
        		 txtNazivPredmeta.requestFocus();
        	 }
        	 }
        	 else
        	 {
        		 
        		 izmjena=false;
        		 OcistiPolja();
        	 }
          }
        
        });
        btnObrisati.addActionListener(new ActionListener()
        {
        	@Override
          public void actionPerformed(ActionEvent e)
          {
          if(selected>-1)
	         {
        	  try
        	  {
        	    em.getTransaction().begin();
        	       
        	    Predmet temp = predmeti.get(selected); 
        	    em.remove(temp);
        	    em.getTransaction().commit();        	    
        	 	  DefaultTableModel model = new DefaultTableModel();
            	  model=(DefaultTableModel) table.getModel();
                  model.removeRow(selected);
                  selected=-1;
                  for(Predmet p : predmeti)
                  {
                	  if(p.getPreduslovi().contains(temp))
                		  p.getPreduslovi().remove(temp);
                  }
                  
        	  }
        	  catch (Exception err)
        	  {
        		  JOptionPane.showMessageDialog(null, "Nije moguće obrisati predmet koji je već dodijeljen nekom usmjerenju ili koji je preduslov za polaganje nekog drugog predmeta!", "Greška", JOptionPane.ERROR_MESSAGE);
        	  }
              }
            }
        
        });
        tablePreduslovi.getColumnModel().getColumn(0).setMinWidth(20);
        tablePreduslovi.getColumnModel().getColumn(0).setMaxWidth(50);
        tablePreduslovi.getColumnModel().getColumn(0).setPreferredWidth(35);
        tablePreduslovi.getColumnModel().getColumn(2).setMinWidth(60);
        tablePreduslovi.getColumnModel().getColumn(2).setMaxWidth(60);
        tablePreduslovi.getColumnModel().getColumn(2).setPreferredWidth(60);
        OsvjeziTabelu(true);
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            			@Override
			public void valueChanged(ListSelectionEvent e) 
            {
            	selected = table.getSelectedRow();
            	izmjena=false;
       		 	OcistiPolja();
			}
        });

        
        
		
        frmPredmeti.setSize(new Dimension(749, 637));
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frmPredmeti.setLocation(dim.width/2-frmPredmeti.getSize().width/2, dim.height/2-frmPredmeti.getSize().height/2);

	}
}
