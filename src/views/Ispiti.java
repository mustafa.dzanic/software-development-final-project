package views;



import javax.swing.JFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.BorderLayout;
import java.awt.Dimension;

import java.awt.Toolkit;

import javax.imageio.ImageIO;
import javax.persistence.*;
import javax.swing.ImageIcon;


import java.awt.Color;

import javax.swing.JLabel;

import java.awt.Font;
import java.awt.Image;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

import javax.swing.JTextField;

import models.Nastavnik;
import models.Predmet;
import models.Sekcija;

import models.Ispit;

import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import javax.swing.JComboBox;

import java.io.InputStream;

import javax.swing.DefaultComboBoxModel;

import static ch.lambdaj.Lambda.*;

import org.hamcrest.Matchers;
import javax.swing.JPanel;

public class Ispiti {

	public JFrame frmIspiti;
	private JTextField txtDatum;
	private EntityManagerFactory emf;
	private EntityManager em;
	private JTable table;
	private  JScrollPane scrollPane;
	private int selected;
	public Nastavnik currentUser;
	private ArrayList<Sekcija> sekcije;
	private List<Ispit> ispiti;
	private List<Ispit> ispitiSource;
	private JTextField txtVrijeme;
	private JTextField txtUsername;
	private JTextField txtPassword;
	private JComboBox cmbSekcija;
	private JComboBox cmbFilter;
	private boolean izmjena=false;
	private JButton btnObrisati;
	private JButton btnIzmijeniti;
	private JButton btnDodati;
	private JTextField txtBodovi;
	private JTextField txtFilter;
	private JPanel panel;
	
	private void OsvjeziTabelu(boolean reload)
	{
		if(reload)
		{
		ispiti = new ArrayList<Ispit>();
		for(Predmet p : currentUser.getPredmeti())
		{	
			for(Sekcija s : p.getSekcije())
			{
				for(Ispit isp : s.getIspiti())
				{
					if(isp.getTip()==0)
						ispiti.add(isp);
				}
			}
		}	
		ispitiSource=ispiti;
		}
		Object columnNames[] = { "ID", "Sekcija", "Datum ispita", "Vrijeme", "Bodovi","Sala", "Broj studenata"};
        String[][] rowData = new String[ispiti.size()][7];
        
        int i=0;
	    for(Ispit ispit : ispiti)
	    {
	    	rowData[i][0]=new Integer(ispit.getId()).toString();
	    	rowData[i][1]=ispit.getSekcija().getNaziv();
	    	rowData[i][2]=ispit.getDatum();
	    	rowData[i][3]=ispit.getVrijeme();
	    	rowData[i][4]=ispit.getSala();
	    	rowData[i][5]=new Integer(ispit.getBodovi()).toString();
	    	rowData[i][6]=new Integer(ispit.getStudenti().size()).toString();
	    	i++;
	    }
	    			
	    table.setModel(new DefaultTableModel(rowData,columnNames));
        table.getColumnModel().getColumn(0).setMinWidth(20);
        table.getColumnModel().getColumn(0).setMaxWidth(50);
        table.getColumnModel().getColumn(0).setPreferredWidth(35);
        
        table.getColumnModel().getColumn(3).setMinWidth(60);
        table.getColumnModel().getColumn(3).setMaxWidth(60);
        table.getColumnModel().getColumn(3).setPreferredWidth(60);
        
        table.getColumnModel().getColumn(5).setMinWidth(50);
        table.getColumnModel().getColumn(5).setMaxWidth(50);
        table.getColumnModel().getColumn(5).setPreferredWidth(50);
        
	}
	boolean tryParseInt(String value)  
	{  
	     try  
	     {  
	         Integer.parseInt(value);  
	         return true;  
	      } catch(NumberFormatException nfe)  
	      {  
	          return false;  
	      }  
	}
	
	
	
	public Ispiti(Nastavnik nast) {
		currentUser=nast;
		emf = Persistence.createEntityManagerFactory("StudentskaSluzba");		
	    em = emf.createEntityManager();			   
	    em.getEntityManagerFactory().getCache().evictAll();
	    sekcije=new ArrayList<Sekcija>();
	    for(Predmet p : currentUser.getPredmeti())
	    {
	    	sekcije.addAll(p.getSekcije());
	    }
		initialize();
	}
	
	private void OcistiPolja()
	{
		btnIzmijeniti.setText("Izmijeniti");
		btnDodati.setText("Dodati");
 		btnObrisati.setEnabled(true);
		txtDatum.setText("");
		txtVrijeme.setText("");
		txtBodovi.setText("");
		txtSala.setText("");
		if(sekcije.size()>0)
			cmbSekcija.setSelectedIndex(0);		
	}
	
	private boolean tryParseDate(String text)
	{
		SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
		try {
			 
			Date date = formatter.parse(text);			
			return true;
	 
		} catch (ParseException e) {
			return false;
		}
	}

	
	private void initialize() {
		frmIspiti = new JFrame();
		frmIspiti.setResizable(false);
		frmIspiti.setTitle("Dodavanje novog ispita\r\n");
		frmIspiti.getContentPane().setBackground(Color.WHITE);
		frmIspiti.setBounds(100, 100, 450, 300);
		frmIspiti.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmIspiti.getContentPane().setLayout(null);
		try {
			InputStream stream = this.getClass().getResourceAsStream("/logo.jpg");
			ImageIcon image;
			image = new ImageIcon(ImageIO.read(stream));
		} catch (Exception e1) 
		{
			
		}
		
		JLabel lblNewLabel = new JLabel("Datum odr\u017Eavanja ispita\r\n");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel.setBounds(20, 298, 177, 16);
		frmIspiti.getContentPane().add(lblNewLabel);
		
		Object columnNames[] = { "ID", "Radno mjesto"};
        String[][] rowData = new String[0][2];
        table = new JTable(rowData, columnNames) {
		    @Override
		    public boolean isCellEditable(int row, int column) {
		       return false;
		    }
		};
        scrollPane = new JScrollPane(table);
        frmIspiti.getContentPane().add(scrollPane, BorderLayout.CENTER);
        scrollPane.setBounds(300, 190, 431 , 223);
        
        JButton btnPovratak = new JButton("Povratak");
        btnPovratak.setBackground(new Color(128, 0, 0));
        btnPovratak.setForeground(new Color(255, 255, 255));
        btnPovratak.setBounds(641, 472, 90, 29);
        frmIspiti.getContentPane().add(btnPovratak);
        btnPovratak.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				NastavnikHome window = new NastavnikHome(currentUser.getKorisnik());
				frmIspiti.dispose();
	    		window.frmNastavnikHome.setVisible(true);
			}
		});
        
        btnDodati = new JButton("Dodati");
        btnDodati.setBackground(new Color(128, 0, 0));
        btnDodati.setForeground(new Color(255, 255, 255));
        btnDodati.addActionListener(new ActionListener()
        {
        @Override
          public void actionPerformed(ActionEvent e)
          {
        	
        	{
        	Ispit ispit;
        	if(izmjena==false)
        		ispit=new Ispit();
        	else
        		ispit=ispiti.get(selected);
        	        	
          	  if(!tryParseDate(txtDatum.getText()))
        	  {
        		  JOptionPane.showMessageDialog(null, "Unesite datum održavanja ispita u formatu dd.MM.yyyy!", "Greška", JOptionPane.ERROR_MESSAGE);
        		  txtDatum.requestFocus();
        	  }
        	  else if(txtVrijeme.getText().length()<1)
        	  {
        		  JOptionPane.showMessageDialog(null, "Niste unijeli vrijeme održavanja ispita!", "Greška", JOptionPane.ERROR_MESSAGE);
        		  txtVrijeme.requestFocus();
        	  }
        	  else if(txtSala.getText().length()<1)
        	  {
        		  JOptionPane.showMessageDialog(null, "Niste unijeli salu za održavanja ispita!", "Greška", JOptionPane.ERROR_MESSAGE);
        		  txtSala.requestFocus();
        	  }
        	  else if(tryParseInt(txtBodovi.getText())==false)
        	  {
        		  JOptionPane.showMessageDialog(null, "Niste unijeli maksimalan broj bodova!", "Greška", JOptionPane.ERROR_MESSAGE);
        		  txtBodovi.requestFocus();
        	  }
        	  else
        	  {
        	  	em.getTransaction().begin();
        	  	ispit.setDatum(txtDatum.getText());
        	  	ispit.setBodovi(Integer.parseInt(txtBodovi.getText()));
        	  	ispit.setVrijeme(txtVrijeme.getText());
        	  	ispit.setSala(txtSala.getText());
        	  	ispit.setSekcija((Sekcija)cmbSekcija.getSelectedItem());
        	  	ispit.setTip(0);
        	  	if(!((Sekcija)cmbSekcija.getSelectedItem()).getIspiti().contains(ispit))
        	  		((Sekcija)cmbSekcija.getSelectedItem()).getIspiti().add(ispit);
        	    em.persist(em.merge(ispit));
        	    em.merge((Sekcija)cmbSekcija.getSelectedItem());
        	    em.getTransaction().commit();        	    
        	    OsvjeziTabelu(true);       
        	    OcistiPolja();
        	  }
          }
          }
        });
        btnDodati.setBounds(0, 468, 96, 38);
        frmIspiti.getContentPane().add(btnDodati);
        
        JLabel lblPrezime = new JLabel("Vrijeme odr\u017Eavanja ispita");
        lblPrezime.setFont(new Font("Tahoma", Font.BOLD, 14));
        lblPrezime.setBounds(20, 363, 194, 16);
        frmIspiti.getContentPane().add(lblPrezime);
        
        
        JLabel lblMaksBrojBodova = new JLabel("Broj bodova:");
        lblMaksBrojBodova.setFont(new Font("Tahoma", Font.BOLD, 14));
        lblMaksBrojBodova.setBounds(20, 231, 111, 16);
        frmIspiti.getContentPane().add(lblMaksBrojBodova);
        
        JLabel label = new JLabel("Filter po:");
        label.setBounds(300, 431, 58, 16);
        frmIspiti.getContentPane().add(label);
        
        cmbFilter = new JComboBox();
        cmbFilter.setBackground(new Color(128, 0, 0));
        cmbFilter.setForeground(new Color(255, 255, 255));
        cmbFilter.addActionListener (new ActionListener () {
	        public void actionPerformed(ActionEvent e) {
	        	txtFilter.setText("");
	            OsvjeziTabelu(true);
	        }
	    });
        cmbFilter.setModel(new DefaultComboBoxModel(new String[] {"predmet", "sekcija", "datum", "sala"}));
        cmbFilter.setBounds(358, 426, 157, 27);
        frmIspiti.getContentPane().add(cmbFilter);
        
        txtFilter = new JTextField();
        txtFilter.getDocument().addDocumentListener(new DocumentListener()
        {
            public void changedUpdate(DocumentEvent arg0) 
            {
           
            }
            public void insertUpdate(DocumentEvent arg0) 
            {
            	if(cmbFilter.getSelectedIndex()==0)
            		ispiti = select(ispitiSource, having(on(Ispit.class).getSekcija().getPredmet().getNaziv(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==1)
            		ispiti = select(ispitiSource, having(on(Ispit.class).getSekcija().getNaziv(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==2)
            		ispiti = select(ispitiSource, having(on(Ispit.class).getDatum(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==3)
            		ispiti = select(ispitiSource, having(on(Ispit.class).getSala(), Matchers.containsString(txtFilter.getText())));
            	OsvjeziTabelu(false);
            }

            public void removeUpdate(DocumentEvent arg0) 
            {
            	if(cmbFilter.getSelectedIndex()==0)
            		ispiti = select(ispitiSource, having(on(Ispit.class).getSekcija().getPredmet().getNaziv(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==1)
            		ispiti = select(ispitiSource, having(on(Ispit.class).getSekcija().getNaziv(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==2)
            		ispiti = select(ispitiSource, having(on(Ispit.class).getDatum(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==3)
            		ispiti = select(ispitiSource, having(on(Ispit.class).getSala(), Matchers.containsString(txtFilter.getText())));
            	OsvjeziTabelu(false);
            }           
        });
        txtFilter.setColumns(10);
        txtFilter.setBounds(515, 426, 216, 28);
        frmIspiti.getContentPane().add(txtFilter);
        
        JLabel lblNewLabel_2 = new JLabel("");
        lblNewLabel_2.setBounds(0, 0, 743, 177);
        Image naslovnica= new ImageIcon (this.getClass().getResource("/cover.jpg")).getImage();
        lblNewLabel_2.setIcon(new ImageIcon(naslovnica));
        frmIspiti.getContentPane().add(lblNewLabel_2);
        
        panel = new JPanel();
        panel.setBounds(0, 177, 288, 329);
        frmIspiti.getContentPane().add(panel);
        panel.setLayout(null);
        
        btnIzmijeniti = new JButton("Izmijeniti");
        btnIzmijeniti.setBounds(94, 291, 96, 38);
        panel.add(btnIzmijeniti);
        btnIzmijeniti.setBackground(new Color(128, 0, 0));
        btnIzmijeniti.setForeground(new Color(255, 255, 255));
        
        btnObrisati = new JButton("Obrisati");
        btnObrisati.setBounds(183, 291, 105, 38);
        panel.add(btnObrisati);
        btnObrisati.setForeground(new Color(255, 255, 255));
        btnObrisati.setBackground(new Color(128, 0, 0));
        
        txtVrijeme = new JTextField();
        txtVrijeme.setBounds(12, 209, 263, 38);
        panel.add(txtVrijeme);
        txtVrijeme.setColumns(10);
        
        txtDatum = new JTextField();
        txtDatum.setBounds(12, 141, 263, 38);
        panel.add(txtDatum);
        txtDatum.setColumns(10);
        
        txtBodovi = new JTextField();
        txtBodovi.setBounds(12, 74, 263, 38);
        panel.add(txtBodovi);
        txtBodovi.setColumns(10);
        
        
        
        cmbSekcija = new JComboBox();
        cmbSekcija.setBackground(new Color(255, 255, 255));
        cmbSekcija.setBounds(12, 13, 263, 38);
        panel.add(cmbSekcija);
        for(Sekcija s : sekcije)
        	cmbSekcija.addItem(s);
        btnObrisati.addActionListener(new ActionListener()
        {
        	@Override
          public void actionPerformed(ActionEvent e)
          {
          if(selected>-1)
	         {
	        	
        	  try
        	  {
        	    em.getTransaction().begin();
        	       
        	    Ispit ispit = ispiti.get(selected);
        	    em.remove(em.merge(ispit));
        	    em.getTransaction().commit();        	    
        	 	  DefaultTableModel model = new DefaultTableModel();
            	  model=(DefaultTableModel) table.getModel();
                  model.removeRow(selected);
                  selected=-1;
        	  }
        	  catch (Exception err)
        	  {
        		  JOptionPane.showMessageDialog(null, "Nije moguće obrisati ispit koji ima pridružene podatke!", "Greška", JOptionPane.ERROR_MESSAGE);
        	  }
              }
             }
        
        });
        btnIzmijeniti.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
        	 if(izmjena==false)
          	 {        		 
          	 if(selected>-1)
          	 {
          		 izmjena=true;
          		 btnIzmijeniti.setText("Odustati");
          		 btnDodati.setText("Potvrditi");
          		 btnObrisati.setEnabled(false);
          		 Ispit ispit = ispiti.get(selected);
          		 txtDatum.setText(ispit.getDatum());
          		 txtVrijeme.setText(ispit.getVrijeme());
          		 txtSala.setText(ispit.getSala());
          		 txtBodovi.setText(new Integer(ispit.getBodovi()).toString());
          		 if(sekcije.size()>0)
          			 cmbSekcija.setSelectedItem(ispit.getSekcija());
          		 txtDatum.requestFocus();
          	 }
          	 }
          	 else
          	 {         		 
          		 izmjena=false;
          		 OcistiPolja();
          	 }
          }
        
        });
        
        
        OsvjeziTabelu(true);
        
        table.addMouseListener(new MouseAdapter() {
	        public void mouseClicked(MouseEvent e) {
	            
	        		if (e.getClickCount()==2)
	        		{
	        			JTable target = (JTable) e.getSource();
	        			int row = target.getSelectedRow();	        			
	        			Ispit isp=ispiti.get(row);
	        			IspitStudenti window=new IspitStudenti(isp);
	        			window.frmIspitStudenti.setVisible(true);
	        		}
	        }
	        

	    });
       
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            			@Override
			public void valueChanged(ListSelectionEvent e) 
            {
            	selected = table.getSelectedRow();
            	OcistiPolja();
				
			}
        });
		
		
		
        frmIspiti.setBackground(new Color(255, 255, 255));        
        frmIspiti.setBounds(100, 100, 450, 300);
        frmIspiti.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmIspiti.setSize(new Dimension(749, 541));
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frmIspiti.setLocation(dim.width/2-frmIspiti.getSize().width/2, dim.height/2-frmIspiti.getSize().height/2);

	}

}
