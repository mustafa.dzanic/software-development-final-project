package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseListener;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.persistence.*;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;

import models.Predmet;
import models.Sekcija;
import models.Student;
import models.Usmjerenje;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.table.DefaultTableModel;

public class UpisSemestra {

	public JFrame frmUpisSemestra;
	private EntityManagerFactory emf;
	private EntityManager em;
	private List<Sekcija> sekcije;
	private Student currentUser;
	private JTable tableSekcije;
	private JScrollPane scrollPaneSekcije;
	private JLabel lblECTS;
	private JComboBox cmbSemestar;
	private int ects;
	
	public UpisSemestra(Student user) 
	{
		currentUser=user;
		emf = Persistence.createEntityManagerFactory("StudentskaSluzba");		
	    em = emf.createEntityManager();			   
	    em.getEntityManagerFactory().getCache().evictAll();
	    Query q = em.createQuery("select t from Sekcija t where t.semestar='I'");
	    sekcije=q.getResultList();
	    
		initialize();
	}

	private void OsvjeziTabelu()
	{
		Query q = em.createQuery("select t from Sekcija t where t.semestar='" + cmbSemestar.getSelectedItem().toString() +"'");
	    
	    sekcije = q.getResultList();
       
	    Object columnNamesPredmeti[] = { "ID", "Sekcija", "Usmjerenje", ""};
        Object[][] rowDataPredmeti = new Object[sekcije.size()][5];
        ects=0;
        int j=0;
	    for(Sekcija sek : sekcije)
	    {
	    	rowDataPredmeti[j][0]=new Integer(sek.getId()).toString();
	    	rowDataPredmeti[j][1]=sek.getNaziv() + " (" + sek.getPredmet().getNaziv() + ")";
	    	rowDataPredmeti[j][2]=sek.getPredmet().getListUsmjerenja();
	    	rowDataPredmeti[j][3]=false;
	    	j++;
	    }
	    
	    tableSekcije.setModel(new DefaultTableModel(rowDataPredmeti,columnNamesPredmeti));
	    tableSekcije.getColumnModel().getColumn(0).setMinWidth(30);
	    tableSekcije.getColumnModel().getColumn(0).setMaxWidth(30);
	    tableSekcije.getColumnModel().getColumn(0).setPreferredWidth(30);	
        
	    tableSekcije.getColumnModel().getColumn(2).setMinWidth(80);
	    tableSekcije.getColumnModel().getColumn(2).setMaxWidth(80);
	    tableSekcije.getColumnModel().getColumn(2).setPreferredWidth(80);
	    
	    tableSekcije.getColumnModel().getColumn(3).setMinWidth(20);
	    tableSekcije.getColumnModel().getColumn(3).setMaxWidth(20);
	    tableSekcije.getColumnModel().getColumn(3).setPreferredWidth(20);	

	
	}

	
	private void initialize() {
		frmUpisSemestra = new JFrame();
		frmUpisSemestra.setResizable(false);
		frmUpisSemestra.setTitle("Upis semestra");
		frmUpisSemestra.getContentPane().setBackground(Color.WHITE);
		frmUpisSemestra.setBounds(100, 100, 751, 452);
		frmUpisSemestra.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmUpisSemestra.getContentPane().setLayout(null);
	
		
		JLabel lblNewLabel = new JLabel("Usmjerenje:");
		lblNewLabel.setBounds(18, 222, 102, 16);
		frmUpisSemestra.getContentPane().add(lblNewLabel);
		
		JLabel lblSemestar = new JLabel("Semestar:");
		lblSemestar.setBounds(18, 261, 102, 16);
		frmUpisSemestra.getContentPane().add(lblSemestar);
		
		JLabel lblPredmeti = new JLabel("Predmeti:");
		lblPredmeti.setBounds(351, 191, 102, 16);
		frmUpisSemestra.getContentPane().add(lblPredmeti);
		
		JComboBox cmbUsmjerenje = new JComboBox();
		cmbUsmjerenje.setModel(new DefaultComboBoxModel(new String[]{currentUser.getUsmjerenje().getNaziv()}));
		cmbUsmjerenje.setEnabled(false);
		cmbUsmjerenje.setBounds(100, 213, 239, 35);
		frmUpisSemestra.getContentPane().add(cmbUsmjerenje);
		
		cmbSemestar = new JComboBox();		
		int idx=0;
		String semestri[]={"I", "II", "III", "IV", "V", "VI", "VII", "VIII"};
		if(currentUser.getSemestar()!=null)
		{
			if(currentUser.getSemestar().equals("I"))
			{
				idx=1;
			}
			if(currentUser.getSemestar().equals("II"))
			{
				idx=2;
			}
			if(currentUser.getSemestar().equals("III"))
			{
				idx=3;
			}
			if(currentUser.getSemestar().equals("IV"))
			{
				idx=4;
			}
			if(currentUser.getSemestar().equals("V"))
			{
				idx=5;
			}
			if(currentUser.getSemestar().equals("VI"))
			{
				idx=6;
			}
			if(currentUser.getSemestar().equals("VII"))
			{
				idx=7;
			}
			if(currentUser.getSemestar().equals("VIII"))
			{
				idx=-1;
			}
			
		}
			for(int i=idx; i<8; i++)
			{
				cmbSemestar.addItem(semestri[i]);
			}
		cmbSemestar.setBounds(236, 252, 102, 35);
		frmUpisSemestra.getContentPane().add(cmbSemestar);
		cmbSemestar.addActionListener (new ActionListener () {
	        public void actionPerformed(ActionEvent e) {
	            OsvjeziTabelu();
	        }
	    });
		Object columnNamesPredmeti[] = { "ID", "Predmet", "Usmjerenje", ""};
        Object[][] rowDataPredmeti = new Object[0][3];		       
        tableSekcije = new JTable(rowDataPredmeti, columnNamesPredmeti) {
		    @Override
		    public boolean isCellEditable(int row, int column) {
		       if(column==3)
		    	   return true;
		       else
		    	   return false;
		    }
		    public Class getColumnClass(int column) {
                switch (column) {
                    case 0:
                        return String.class;
                    case 1:
                        return String.class;
                    case 2:
                        return String.class;
                        
                    default:
                        return Boolean.class;
                       
                }
		    }
		};
			
        scrollPaneSekcije = new JScrollPane(tableSekcije);
        frmUpisSemestra.getContentPane().add(scrollPaneSekcije, BorderLayout.CENTER);
        scrollPaneSekcije.setBounds(351, 214, 382, 151);
	    OsvjeziTabelu();
        tableSekcije.addMouseListener(new MouseAdapter() {
	        public void mouseClicked(MouseEvent e) {
	            
	                JTable target = (JTable) e.getSource();
	                int row = target.getSelectedRow();
	                int column = target.getSelectedColumn();
	                ects=0;
	                if(column==3)
	                {	                
	                for(int i=0; i<tableSekcije.getRowCount(); i++)
	                {
	                	if(tableSekcije.getValueAt(i, 3).equals(true))
	                	{	                			                	    			
	                	    ects+=sekcije.get(i).getPredmet().getECTS();
	                	}
	                	
	                }	                
	                lblECTS.setText("Ukupno označenih predmeta: " + new Integer(ects).toString());
	                }
	                
	            }
	        

	    });
	    
	    lblECTS = new JLabel();
	    lblECTS.setFont(new Font("Yu Gothic UI Semilight", Font.BOLD, 16));
	    lblECTS.setText("Ukupno označenih predmeta: 0");
	    frmUpisSemestra.getContentPane().add(lblECTS);
	    lblECTS.setForeground(Color.DARK_GRAY);	    
	    lblECTS.setBounds(351, 375, 350, 28);
	    
	    JButton btnPovratak = new JButton("Povratak");
	    btnPovratak.setForeground(new Color(255, 255, 255));
	    btnPovratak.setBackground(new Color(128, 0, 0));
	    btnPovratak.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
        	  StudentHome window = new StudentHome(currentUser.getKorisnik());
				frmUpisSemestra.dispose();
	    		window.frmStudentHome.setVisible(true);
          }
        });

        btnPovratak.setBounds(643, 375, 90, 29);
        frmUpisSemestra.getContentPane().add(btnPovratak);
        
        JButton btnPotvrditi = new JButton("Potvrditi");
        btnPotvrditi.setForeground(new Color(255, 255, 255));
        btnPotvrditi.setBackground(new Color(128, 0, 0));
        btnPotvrditi.setBounds(18, 375, 90, 29);
        frmUpisSemestra.getContentPane().add(btnPotvrditi);
        
        JLabel label = new JLabel("");
        label.setBounds(0, 0, 745, 186);
        Image naslovnica= new ImageIcon (this.getClass().getResource("/cover.jpg")).getImage();
		label.setIcon(new ImageIcon(naslovnica));
        frmUpisSemestra.getContentPane().add(label);
        btnPotvrditi.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {        	  
        	  	ArrayList<Sekcija> odabraneSekcije=new ArrayList<Sekcija>();
        	  	ArrayList<Sekcija> stareSekcije=new ArrayList<Sekcija>();
        	  	stareSekcije.addAll(currentUser.getSekcije());
      	    	for(int i=0; i<tableSekcije.getRowCount(); i++)
      	    	{
      	    		if(tableSekcije.getValueAt(i, 3).equals(true))
      	    		{           	    	
      	    			odabraneSekcije.add(sekcije.get(i));      	    			
      	    		}        	    	
      	    	}  
      	    	boolean ispunjeniPreduslovi=true;
      	    	Predmet odabranP=null;
      	    	Predmet preduslovP=null;
      	    	int brojac1=0;
      	    	for(Sekcija s : odabraneSekcije)
      	    	{      	    		
      	    		odabranP=s.getPredmet();
      	    		for(Predmet p : s.getPredmet().getPreduslovi())
      	    		{
      	    			ispunjeniPreduslovi=false;
      	    			preduslovP=p;
      	    			int brojac=0;
      	    			for(Sekcija staraS : stareSekcije)
      	    			{      	    				
      	    				if(staraS.getPredmet().getId()==p.getId())
      	    				{
      	    					ispunjeniPreduslovi=true;      	    					
      	    					break;
      	    				}      	    				
      	    				if(brojac==stareSekcije.size()-1)
      	    				{
      	    					ispunjeniPreduslovi=false;      	    					
      	    					break;
      	    				}      	    				
      	    				brojac++;
      	    			}
      	    			if(ispunjeniPreduslovi==true)
      	    				break;
      	    		}
      	    		if(ispunjeniPreduslovi==true && brojac1==odabraneSekcije.size()-1)
  	    				break;
      	    		brojac1++;
      	    	}
      	    	int drugaUsmjerenja=0;
      	    	for(Sekcija s : odabraneSekcije)
      	    	{
      	    		int c=0;
      	    		for(Usmjerenje usm : s.getPredmet().getUsmjerenja())
      	    		{
      	    			if(usm.getId()==currentUser.getUsmjerenje().getId())
      	    				break;
      	    			if(c==s.getPredmet().getUsmjerenja().size()-1)
      	    			{
      	    				drugaUsmjerenja++;      	    				
      	    			}
      	    			c++;
      	    		}
      	    	}      	    	
      	    	if(cmbSemestar.getSelectedItem().toString().equals(currentUser.getSemestar()))
      	    	{
      	    		JOptionPane.showMessageDialog(null, "Već ste upisani na " + currentUser.getSemestar() + " semestar!", "Greška", JOptionPane.ERROR_MESSAGE);
      	    	}
      	    	else if(drugaUsmjerenja>2)
      	    	{
      	    		JOptionPane.showMessageDialog(null, "Možete odabrati najviše 2 predmeta koja ne pripadaju vašem usmjerenju!", "Greška", JOptionPane.ERROR_MESSAGE);
      	    	}
      	    	else if(ispunjeniPreduslovi==false)
      	    	{
      	    		JOptionPane.showMessageDialog(null, "Za odabir predmeta " + odabranP.getNaziv() + " potrebno je da imate odslušan predmet " + preduslovP.getNaziv(), "Greška", JOptionPane.ERROR_MESSAGE);
      	    	}
      	    	else if(ects>5)
      	    	{
      	    		JOptionPane.showMessageDialog(null, "Odabrali ste vise od 5 predmeta u jednom semestru " , "Greška", JOptionPane.ERROR_MESSAGE);
      	    	}
      	    	else
      	    	{
      	    	em.getTransaction().begin();
      	    	stareSekcije.addAll(odabraneSekcije);
      	    	currentUser.setSekcije(stareSekcije);
      	    	currentUser.setSemestar(cmbSemestar.getSelectedItem().toString());
      	    	em.persist(em.merge(currentUser));
      	    	em.getTransaction().commit();
        	  	StudentHome window = new StudentHome(currentUser.getKorisnik());
				frmUpisSemestra.dispose();
	    		window.frmStudentHome.setVisible(true);
      	    	}
          }
        });
        
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frmUpisSemestra.setLocation(dim.width/2-frmUpisSemestra.getSize().width/2, dim.height/2-frmUpisSemestra.getSize().height/2);
	}
}
