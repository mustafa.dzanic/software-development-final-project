package views;

import java.awt.EventQueue;

import javax.swing.JFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.imageio.ImageIO;
import javax.persistence.*;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

import java.awt.Color;

import javax.swing.JLabel;

import java.awt.Font;
import java.awt.Image;
import java.io.InputStream;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.JTextField;

import models.Student;
import models.Usmjerenje;
import models.Korisnik;
import models.Nastavnik;

import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import static ch.lambdaj.Lambda.*;
import org.hamcrest.Matchers;
import javax.swing.JPanel;

public class Studenti {

	public JFrame frmStudenti;
	private JTextField txtIme;
	private EntityManagerFactory emf;
	private EntityManager em;
	private JTable table;
	private  JScrollPane scrollPane;
	private int selected;
	public Korisnik currentUser;
	private List<Student> studenti;
	private List<Student> studentiSource;
	private List<Usmjerenje> usmjerenja;
	private List<Korisnik> korisnici;
	private JTextField txtPrezime;
	private JTextField txtBrojIndeksa;
	private JTextField txtAdresa;
	private JTextField txtBrTelefona;
	private JTextField txtUsername;
	private JTextField txtPassword;
	private JComboBox cmbUsmjerenja;
	private boolean izmjena=false;
	private JButton btnObrisati;
	private JButton btnIzmijeniti;
	private JButton btnDodati;
	private JComboBox cmbFilter;
	private JTextField txtFilter;
	private JPanel panel;
	private JPanel panel_1;
	private JLabel label;
	private JPanel panel_2;
	private void OsvjeziTabelu(boolean reload)
	{
		Query q = em.createQuery("select t from Student t");
	    
		if(reload)
		{
	    studenti = q.getResultList();
	    studentiSource=studenti;
		}
	    q = em.createQuery("select t from Korisnik t");
	    
	    korisnici = q.getResultList();
        Object columnNames[] = { "ID", "Ime", "Prezime", "Broj indeksa", "Usmjerenje"};
        String[][] rowData = new String[studenti.size()][5];
        
        int i=0;
	    for(Student s : studenti)
	    {
	    	rowData[i][0]=new Integer(s.getId()).toString();
	    	rowData[i][1]=s.getIme();
	    	rowData[i][2]=s.getPrezime();
	    	rowData[i][3]=s.getBr_indeksa();
	    	rowData[i][4]=s.getUsmjerenje().getNaziv();
	    	i++;
	    }
	    			
	    table.setModel(new DefaultTableModel(rowData,columnNames));
        table.getColumnModel().getColumn(0).setMinWidth(20);
        table.getColumnModel().getColumn(0).setMaxWidth(50);
        table.getColumnModel().getColumn(0).setPreferredWidth(35);	
        
	}

	public Studenti() 
	{
		emf = Persistence.createEntityManagerFactory("StudentskaSluzba");		
	    em = emf.createEntityManager();			   
	    em.getEntityManagerFactory().getCache().evictAll();
	    Query q = em.createQuery("select t from Usmjerenje t");	    
	    usmjerenja = q.getResultList();
		initialize();
	}

	
	
	private void OcistiPolja()
	{
		btnIzmijeniti.setText("Izmijeniti");
		btnDodati.setText("Dodati");
 		btnObrisati.setEnabled(true);
		txtIme.setText("");
		txtPrezime.setText("");
		txtUsername.setText("");
		txtPassword.setText("");
		txtAdresa.setText("");
		txtBrTelefona.setText("");
		txtBrojIndeksa.setText("");
		if(usmjerenja.size()>0)
			cmbUsmjerenja.setSelectedIndex(0);
	}
	
	private void initialize() {
		frmStudenti = new JFrame();
		frmStudenti.setResizable(false);
		frmStudenti.getContentPane().setBackground(Color.WHITE);
		frmStudenti.setTitle("Dodavanje novog studenta");
		frmStudenti.setBounds(100, 100, 450, 300);
		frmStudenti.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmStudenti.getContentPane().setLayout(null);
		
		
		JLabel lblNewLabel = new JLabel("Ime:");
		lblNewLabel.setBounds(18, 220, 111, 16);
		frmStudenti.getContentPane().add(lblNewLabel);
		
		txtIme = new JTextField();
		txtIme.setBounds(102, 210, 172, 36);
		frmStudenti.getContentPane().add(txtIme);
		txtIme.setColumns(10);
		
		Object columnNames[] = { "ID", "Radno mjesto"};
        String[][] rowData = new String[0][2];
        table = new JTable(rowData, columnNames) {
		    @Override
		    public boolean isCellEditable(int row, int column) {
		       return false;
		    }
		};
        scrollPane = new JScrollPane(table);
        frmStudenti.getContentPane().add(scrollPane, BorderLayout.CENTER);
        scrollPane.setBounds(305, 210, 428 , 354);
        
        JButton btnPovratak = new JButton("Povratak");
        btnPovratak.setForeground(new Color(255, 255, 255));
        btnPovratak.setBackground(new Color(128, 0, 0));
        btnPovratak.setBounds(643, 624, 90, 29);
        frmStudenti.getContentPane().add(btnPovratak);
        btnPovratak.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				AdminPanel window = new AdminPanel(currentUser);
				frmStudenti.dispose();
	    		window.frmAdministrator.setVisible(true);
			}
		});
        
        btnDodati = new JButton("Dodati");
        btnDodati.setForeground(new Color(255, 255, 255));
        btnDodati.setBackground(new Color(128, 0, 0));
        btnDodati.addActionListener(new ActionListener()
        {
        @Override
          public void actionPerformed(ActionEvent e)
          {
        	
        	{
        	Student stud;
        	if(izmjena==false)
        		stud=new Student();
        	else
        		stud=studenti.get(selected);
        	for(Korisnik k : korisnici)
        	{
        	if(izmjena==false && k.getUsername().equals(txtUsername.getText()))
        
        		JOptionPane.showMessageDialog(null, "Korisničko ime " + txtUsername.getText() + " već postoji u bazi podataka!", "Greška", JOptionPane.ERROR_MESSAGE);
        		txtUsername.requestFocus();
        		return;
        	}
        	if(izmjena==true && k.getUsername().equals(txtUsername.getText()) && k.getId()!=stud.getKorisnik().getId())
        	{
        		JOptionPane.showMessageDialog(null, "Korisničko ime " + txtUsername.getText() + " već postoji u bazi podataka!", "Greška", JOptionPane.ERROR_MESSAGE);
        		txtUsername.requestFocus();
        		return;
        	}
        	
        	}
        	  if(txtIme.getText().length()<1)
        	  {
        		  JOptionPane.showMessageDialog(null, "Niste unijeli ime studenta!", "Greška", JOptionPane.ERROR_MESSAGE);
        		  txtIme.requestFocus();
        	  }
        	  else if(txtPrezime.getText().length()<1)
        	  {
        		  JOptionPane.showMessageDialog(null, "Niste unijeli prezime studenta!", "Greška", JOptionPane.ERROR_MESSAGE);
        		  txtPrezime.requestFocus();
        	  }
        	  else if(txtUsername.getText().length()<1)
        	  {
        		  JOptionPane.showMessageDialog(null, "Niste unijeli korisniÄ�ko ime studenta!", "Greška", JOptionPane.ERROR_MESSAGE);
        		  txtUsername.requestFocus();
        	  }
        	  else if(txtPassword.getText().length()<1)
        	  {
        		  JOptionPane.showMessageDialog(null, "Niste unijeli lozinku!", "Greška", JOptionPane.ERROR_MESSAGE);
        		  txtPassword.requestFocus();
        	  }
        	  else if(txtBrojIndeksa.getText().length()<1)
        	  {
        		  JOptionPane.showMessageDialog(null, "Niste unijeli broj indeksa studenta!", "Greška", JOptionPane.ERROR_MESSAGE);
        		  txtBrojIndeksa.requestFocus();
        	  }        	  
        	  else
        	  {
        	  	em.getTransaction().begin();
        	  	Korisnik kor= izmjena ? stud.getKorisnik() : new Korisnik();        	    
        	    kor.setUsername(txtUsername.getText());
        	    kor.setLozinka(txtPassword.getText());
        	    kor.setRole(2);
        	    stud.setKorisnik(kor);
        	    stud.setAdresa(txtAdresa.getText());
        	    stud.setBr_indeksa(txtBrojIndeksa.getText());
        	    stud.setIme(txtIme.getText());
        	    stud.setPrezime(txtPrezime.getText());
        	    stud.setTelefon(txtBrTelefona.getText());
        	    stud.setUsmjerenje((Usmjerenje)cmbUsmjerenja.getSelectedItem());
        	    em.persist(stud.getKorisnik());
        	    em.persist(stud);
        	    em.getTransaction().commit();        	    
        	    OsvjeziTabelu(true);       
        	    OcistiPolja();
        	  }
          }
          }
        });
        
        
   	 table.addMouseListener(new MouseAdapter() {
	        public void mouseClicked(MouseEvent e) {
	            
	        		if (e.getClickCount()==2)
	        		{
	        			JTable target = (JTable) e.getSource();
	        			int row = target.getSelectedRow();	        			
	        			Student stud=studentiSource.get(row);
	        			StudentProfil window=new StudentProfil(stud);
	        			window.frmInfoOStudentu.setVisible(true);
	        		}
	        }
	        

	    });
        
        btnDodati.setBounds(0, 624, 99, 42);
        frmStudenti.getContentPane().add(btnDodati);
        
        btnIzmijeniti = new JButton("Izmijeniti");
        btnIzmijeniti.setForeground(new Color(255, 255, 255));
        btnIzmijeniti.setBackground(new Color(128, 0, 0));
        btnIzmijeniti.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
        	 if(izmjena==false)
          	 {        		 
          	 if(selected>-1)
          	 {
          		 izmjena=true;
          		 btnIzmijeniti.setText("Odustati");
          		 btnDodati.setText("Potvrditi");
          		 btnObrisati.setEnabled(false);
          		 Student stud = studenti.get(selected);
          		 txtIme.setText(stud.getIme());
          		 txtPrezime.setText(stud.getPrezime());
          		 txtUsername.setText(stud.getKorisnik().getUsername());
          		 txtPassword.setText(stud.getKorisnik().getLozinka());
          		 txtAdresa.setText(stud.getAdresa());
          		 txtBrTelefona.setText(stud.getTelefon());
          		 txtBrojIndeksa.setText(stud.getBr_indeksa());
          		 if(usmjerenja.size()>0)
          			 cmbUsmjerenja.setSelectedItem(stud.getUsmjerenje());
          		 txtIme.requestFocus();
          	 }
          	 }
          	 else
          	 {         		 
          		 izmjena=false;
          		 OcistiPolja();
          	 }
          }
        
        });
        btnIzmijeniti.setBounds(97, 624, 102, 42);
        frmStudenti.getContentPane().add(btnIzmijeniti);
        
        btnObrisati = new JButton("Obrisati");
        btnObrisati.setForeground(new Color(255, 255, 255));
        btnObrisati.setBackground(new Color(128, 0, 0));
        btnObrisati.addActionListener(new ActionListener()
        {
        	@Override
          public void actionPerformed(ActionEvent e)
          {
          if(selected>-1)
	         {
	        	try
	        	{
        	    em.getTransaction().begin();
        	       
        	    Student temp = studenti.get(selected);
        	    
        	    em.remove(temp);
        	    em.getTransaction().commit();        	    
        	 	  DefaultTableModel model = new DefaultTableModel();
            	  model=(DefaultTableModel) table.getModel();
                  model.removeRow(selected);
                  selected=-1;
	        	}
	        	catch (Exception err)
	        	  {
	        		  JOptionPane.showMessageDialog(null, "Nije moguće obrisati studenta koji ima pridružene podatke!", "Greška", JOptionPane.ERROR_MESSAGE);
	        	  }
              }
             }
        
        });
        btnObrisati.setBounds(194, 624, 99, 42);
        frmStudenti.getContentPane().add(btnObrisati);
        
        JLabel lblPrezime = new JLabel("Prezime:");
        lblPrezime.setBounds(18, 259, 111, 16);
        frmStudenti.getContentPane().add(lblPrezime);
        
        txtPrezime = new JTextField();
        txtPrezime.setColumns(10);
        txtPrezime.setBounds(102, 249, 172, 36);
        frmStudenti.getContentPane().add(txtPrezime);
        
        JLabel lblBrojIndeksa = new JLabel("Broj indeksa:");
        lblBrojIndeksa.setBounds(18, 420, 111, 16);
        frmStudenti.getContentPane().add(lblBrojIndeksa);
        
        txtBrojIndeksa = new JTextField();
        txtBrojIndeksa.setColumns(10);
        txtBrojIndeksa.setBounds(130, 410, 144, 36);
        frmStudenti.getContentPane().add(txtBrojIndeksa);
        
        txtAdresa = new JTextField();
        txtAdresa.setColumns(10);
        txtAdresa.setBounds(99, 489, 175, 36);
        frmStudenti.getContentPane().add(txtAdresa);
        
        JLabel lblAdresa = new JLabel("Adresa:");
        lblAdresa.setBounds(18, 499, 111, 16);
        frmStudenti.getContentPane().add(lblAdresa);
        
        JLabel lblBrojTelefona = new JLabel("Broj telefona:");
        lblBrojTelefona.setBounds(18, 538, 111, 16);
        frmStudenti.getContentPane().add(lblBrojTelefona);
        
        txtBrTelefona = new JTextField();
        txtBrTelefona.setColumns(10);
        txtBrTelefona.setBounds(130, 528, 144, 36);
        frmStudenti.getContentPane().add(txtBrTelefona);
        
        cmbUsmjerenja = new JComboBox();
        for(Usmjerenje us : usmjerenja)
        	cmbUsmjerenja.addItem(us);
        cmbUsmjerenja.setBounds(99, 450, 175, 36);
        frmStudenti.getContentPane().add(cmbUsmjerenja);
        
        JLabel lblUsmjerenje = new JLabel("Usmjerenje:");
        lblUsmjerenje.setBounds(18, 460, 111, 16);
        frmStudenti.getContentPane().add(lblUsmjerenje);
        
        JLabel lblKorisnikoIme = new JLabel("Korisničko ime\r\n");
        lblKorisnikoIme.setBounds(18, 322, 111, 16);
        frmStudenti.getContentPane().add(lblKorisnikoIme);
        
        txtUsername = new JTextField();
        txtUsername.setColumns(10);
        txtUsername.setBounds(144, 312, 130, 36);
        frmStudenti.getContentPane().add(txtUsername);
        
        JLabel lblPassword = new JLabel("Password:");
        lblPassword.setBounds(20, 359, 70, 16);
        frmStudenti.getContentPane().add(lblPassword);
        
        txtPassword = new JTextField();
        txtPassword.setColumns(10);
        txtPassword.setBounds(144, 349, 130, 36);
        frmStudenti.getContentPane().add(txtPassword);
        
        JLabel lblFilterPo = new JLabel("Filter po:");
        lblFilterPo.setBounds(342, 589, 70, 16);
        frmStudenti.getContentPane().add(lblFilterPo);
        
        txtFilter = new JTextField();
        txtFilter.getDocument().addDocumentListener(new DocumentListener()
        {
            public void changedUpdate(DocumentEvent arg0) 
            {
           
            }
            public void insertUpdate(DocumentEvent arg0) 
            {
            	if(cmbFilter.getSelectedIndex()==0)
            		studenti = select(studentiSource, having(on(Student.class).getBr_indeksa(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==1)
            		studenti = select(studentiSource, having(on(Student.class).getIme(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==2)
            		studenti = select(studentiSource, having(on(Student.class).getPrezime(), Matchers.containsString(txtFilter.getText())));            	
            	OsvjeziTabelu(false);
            }

            public void removeUpdate(DocumentEvent arg0) 
            {
            	if(cmbFilter.getSelectedIndex()==0)
            		studenti = select(studentiSource, having(on(Student.class).getBr_indeksa(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==1)
            		studenti = select(studentiSource, having(on(Student.class).getIme(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==2)
            		studenti = select(studentiSource, having(on(Student.class).getPrezime(), Matchers.containsString(txtFilter.getText())));
            	OsvjeziTabelu(false);
            }           
        });
        txtFilter.setColumns(10);
        txtFilter.setBounds(520, 583, 213, 28);
        frmStudenti.getContentPane().add(txtFilter);
        
        cmbFilter = new JComboBox();
        cmbFilter.setForeground(new Color(255, 255, 255));
        cmbFilter.setBackground(new Color(128, 0, 0));
        cmbFilter.addActionListener (new ActionListener () {
	        public void actionPerformed(ActionEvent e) {
	        	txtFilter.setText("");
	            OsvjeziTabelu(true);
	        }
	    });
        cmbFilter.setModel(new DefaultComboBoxModel(new String[] {"broj indeksa", "ime", "prezime"}));
        cmbFilter.setBounds(402, 584, 119, 27);
        frmStudenti.getContentPane().add(cmbFilter);
        
        panel = new JPanel();
        panel.setBackground(new Color(230, 230, 250));
        panel.setBounds(0, 398, 293, 268);
        frmStudenti.getContentPane().add(panel);
        
        panel_1 = new JPanel();
        panel_1.setBackground(new Color(250, 235, 215));
        panel_1.setBounds(0, 300, 293, 100);
        frmStudenti.getContentPane().add(panel_1);
        
        label = new JLabel("");
        label.setBounds(0, 0, 745, 180);
        Image naslovnica= new ImageIcon (this.getClass().getResource("/cover.jpg")).getImage();
		label.setIcon(new ImageIcon(naslovnica));
        frmStudenti.getContentPane().add(label);
        
        panel_2 = new JPanel();
        panel_2.setBackground(new Color(230, 230, 250));
        panel_2.setBounds(0, 180, 293, 121);
        frmStudenti.getContentPane().add(panel_2);

        OsvjeziTabelu(true);
       
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            			@Override
			public void valueChanged(ListSelectionEvent e) 
            {
            	selected = table.getSelectedRow();
            	OcistiPolja();
				
			}
        });
		
		
		
        frmStudenti.setBackground(new Color(255, 255, 255));        
        frmStudenti.setBounds(100, 100, 450, 300);
        frmStudenti.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmStudenti.setSize(new Dimension(751, 701));
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frmStudenti.setLocation(dim.width/2-frmStudenti.getSize().width/2, dim.height/2-frmStudenti.getSize().height/2);

		
	}
}
