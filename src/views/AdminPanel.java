package views;

import models.Korisnik;
import models.Nastavnici;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.Image;

import java.io.InputStream;


import javax.swing.border.Border;

public class AdminPanel {

	public JFrame frmAdministrator;
	private Korisnik currentUser;
	public AdminPanel(Korisnik user) {
		currentUser=user;
		initialize();
	}

	
	private void initialize() {
		frmAdministrator = new JFrame();
		frmAdministrator.setResizable(false);
		frmAdministrator.getContentPane().setBackground(Color.WHITE);
		frmAdministrator.setTitle("Administrator");
		frmAdministrator.setBackground(Color.WHITE);
		frmAdministrator.setBounds(100, 100, 450, 300);
		frmAdministrator.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAdministrator.getContentPane().setLayout(null);
		frmAdministrator.setSize(new Dimension(744, 500));
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frmAdministrator.setLocation(dim.width/2-frmAdministrator.getSize().width/2, dim.height/2-frmAdministrator.getSize().height/2);
		JButton btnNastavnici = new JButton("");
		btnNastavnici.setForeground(Color.WHITE);
		btnNastavnici.setBackground(Color.WHITE);
		Image nastavnici= new ImageIcon (this.getClass().getResource("/nastavnik.png")).getImage();
		btnNastavnici.setIcon(new ImageIcon(nastavnici));
		
		Border emptyBorder = BorderFactory.createEmptyBorder();
		btnNastavnici.setBorder(emptyBorder);
		btnNastavnici.addActionListener(new ActionListener()
	{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				Nastavnici window = new Nastavnici();
				window.currentUser=currentUser;
	    		frmAdministrator.dispose();
	    		window.frmNastavnici.setVisible(true);
			}			
		});
		btnNastavnici.setBounds(12, 225, 169, 168);
		frmAdministrator.getContentPane().add(btnNastavnici);
	
		
		JButton btnPredmeti = new JButton("");
		Image predmeti= new ImageIcon (this.getClass().getResource("/predmet.png")).getImage();
		btnPredmeti.setIcon(new ImageIcon(predmeti));
		btnPredmeti.setBackground(Color.WHITE);
		btnPredmeti.setForeground(Color.WHITE);
		Border emptyBorder2 = BorderFactory.createEmptyBorder();
		btnPredmeti.setBorder(emptyBorder2);
		btnPredmeti.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				Predmeti window = new Predmeti();
				window.currentUser=currentUser;
	    		frmAdministrator.dispose();
	    		window.frmPredmeti.setVisible(true);
			}			
		});
		btnPredmeti.setBounds(193, 225, 163, 168);
		frmAdministrator.getContentPane().add(btnPredmeti);
		
		JButton btnUsmjerenja = new JButton("");
		Image usmjerenja= new ImageIcon (this.getClass().getResource("/usmjerenje.png")).getImage();
		btnUsmjerenja.setIcon(new ImageIcon(usmjerenja));
		btnUsmjerenja.setBackground(new Color(255, 255, 255));
		btnUsmjerenja.setForeground(new Color(255, 255, 255));
		Border emptyBorder3 = BorderFactory.createEmptyBorder();
		btnUsmjerenja.setBorder(emptyBorder3);
		btnUsmjerenja.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				Usmjerenja window = new Usmjerenja();
				window.currentUser=currentUser;
	    		frmAdministrator.dispose();
	    		window.frmUsmjerenja.setVisible(true);
			}			
		});
		btnUsmjerenja.setBounds(549, 225, 169, 168);
		frmAdministrator.getContentPane().add(btnUsmjerenja);
		
		JLabel lblCurrentUser = new JLabel("Odaberite opciju");
		lblCurrentUser.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblCurrentUser.setBounds(283, 195, 176, 17);
		frmAdministrator.getContentPane().add(lblCurrentUser);
		
		JButton btnOdjava = new JButton("Odjava");
		btnOdjava.setBackground(new Color(128, 0, 0));
		btnOdjava.setForeground(new Color(255, 255, 255));
		btnOdjava.setBounds(631, 423, 85, 29);
		frmAdministrator.getContentPane().add(btnOdjava);
		
		JButton btnStudenti = new JButton("");
		Image studenti= new ImageIcon (this.getClass().getResource("/student.jpg")).getImage();
		btnStudenti.setIcon(new ImageIcon(studenti));
		btnStudenti.setForeground(Color.WHITE);
		btnStudenti.setBackground(Color.WHITE);
		Border emptyBorder4 = BorderFactory.createEmptyBorder();
		btnStudenti.setBorder(emptyBorder4);
		btnStudenti.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				Studenti window = new Studenti();
				window.currentUser=currentUser;
	    		frmAdministrator.dispose();
	    		window.frmStudenti.setVisible(true);
			}			
		});
		btnStudenti.setBounds(368, 225, 169, 168);
		frmAdministrator.getContentPane().add(btnStudenti);
		
		JButton btnInfo = new JButton("Radna mjesta");
		btnInfo.addActionListener(new ActionListener() {
			
				@Override
				public void actionPerformed(ActionEvent e) 
				{
					RadnaMjesta window = new RadnaMjesta();
					window.currentUser=currentUser;
		    		frmAdministrator.dispose();
		    		window.frmRadnaMjesta.setVisible(true);
				}			
			
		});
		btnInfo.setBackground(new Color(128, 0, 0));
		btnInfo.setForeground(new Color(255, 255, 255));
		btnInfo.setBounds(497, 423, 122, 29);
		frmAdministrator.getContentPane().add(btnInfo);
		
		JLabel lblNewLabel = new JLabel("");
		Image naslovnica= new ImageIcon (this.getClass().getResource("/cover.jpg")).getImage();
		lblNewLabel.setIcon(new ImageIcon(naslovnica));
		lblNewLabel.setBounds(0, 0, 738, 180);
		frmAdministrator.getContentPane().add(lblNewLabel);
		
		JLabel lblNastavnici = new JLabel("Nastavnici");
		lblNastavnici.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNastavnici.setBounds(54, 394, 102, 16);
		frmAdministrator.getContentPane().add(lblNastavnici);
		
		JLabel lblPredmeti = new JLabel("Predmeti");
		lblPredmeti.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblPredmeti.setBounds(237, 394, 102, 16);
		frmAdministrator.getContentPane().add(lblPredmeti);
		
		JLabel lblStudenti = new JLabel("Studenti");
		lblStudenti.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblStudenti.setBounds(419, 394, 102, 16);
		frmAdministrator.getContentPane().add(lblStudenti);
		
		JLabel lblUsmjerenja = new JLabel("Usmjerenja");
		lblUsmjerenja.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblUsmjerenja.setBounds(589, 394, 102, 16);
		frmAdministrator.getContentPane().add(lblUsmjerenja);
		
		btnOdjava.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				Login window = new Login();
	    		frmAdministrator.dispose();
	    		window.frmPrijavaKorisnika.setVisible(true);
			}
		});
	}
}
