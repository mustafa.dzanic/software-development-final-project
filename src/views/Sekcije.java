package views;


import java.awt.EventQueue;

import javax.swing.JFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.imageio.ImageIO;
import javax.persistence.*;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

import java.awt.Color;

import javax.swing.JLabel;

import java.awt.Font;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.JTextField;

import models.Nastavnik;
import models.Predmet;
import models.Sekcija;
import models.Student;
import models.Usmjerenje;
import models.Korisnik;

import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;

import java.awt.Color;
import java.io.InputStream;

import javax.swing.DefaultComboBoxModel;

import static ch.lambdaj.Lambda.*;

import org.hamcrest.Matchers;
import javax.swing.JPanel;

public class Sekcije {

	public JFrame frmSekcije;
	private JTextField txtNaziv;
	private EntityManagerFactory emf;
	private EntityManager em;
	private JTable table;
	private  JScrollPane scrollPane;
	private int selected;
	public Nastavnik currentUser;
	private List<Sekcija> sekcije;
	private List<Sekcija> sekcijeSource;
	private ArrayList<Predmet> predmeti;	
	private JTextField txtGodina;
	private JTextField txtUsername;
	private JTextField txtPassword;
	private JComboBox cmbPredmet;
	private JComboBox cmbSemestar;
	private JComboBox cmbFilter;
	private boolean izmjena=false;
	private JButton btnObrisati;
	private JButton btnIzmijeniti;
	private JButton btnDodati;
	private JTextField txtFilter;
	private JPanel panel;
	private void OsvjeziTabelu(boolean reload)
	{
		if(reload)
		{
		sekcije = new ArrayList<Sekcija>();
		for(Predmet p : currentUser.getPredmeti())
		{
			sekcije.addAll(p.getSekcije());
		}			
		sekcijeSource=sekcije;
		}
	    Object columnNames[] = { "ID", "Naziv", "Godina", "Predmet", "Semestar", "Broj studenata"};
        String[][] rowData = new String[sekcije.size()][6];
        
        int i=0;
	    for(Sekcija s : sekcije)
	    {
	    	rowData[i][0]=new Integer(s.getId()).toString();
	    	rowData[i][1]=s.getNaziv();
	    	rowData[i][2]=new Integer(s.getGodina()).toString();
	    	rowData[i][3]=s.getPredmet().getSkracenica();
	    	rowData[i][4]=s.getSemestar();
	    	rowData[i][5]=new Integer(s.getStudenti().size()).toString();
	    	i++;
	    }
	    
	    	
	    table.setModel(new DefaultTableModel(rowData,columnNames));
        table.getColumnModel().getColumn(0).setMinWidth(20);
        table.getColumnModel().getColumn(0).setMaxWidth(50);
        table.getColumnModel().getColumn(0).setPreferredWidth(35);	
        
	}
	boolean tryParseInt(String value)  
	{  
	     try  
	     {  
	         Integer.parseInt(value);  
	         return true;  
	      } catch(NumberFormatException nfe)  
	      {  
	          return false;  
	      }  
	}
	public Sekcije(Nastavnik nast) {
		currentUser=nast;
		emf = Persistence.createEntityManagerFactory("StudentskaSluzba");		
	    em = emf.createEntityManager();			   
	    em.getEntityManagerFactory().getCache().evictAll();
	    predmeti=new ArrayList<Predmet>();
	    predmeti.addAll(currentUser.getPredmeti());
	    initialize();
	}
	
	private void OcistiPolja()
	{
		btnIzmijeniti.setText("Izmijeniti");
		btnDodati.setText("Dodati");
 		btnObrisati.setEnabled(true);
		txtNaziv.setText("");
		txtGodina.setText("");
		if(predmeti.size()>0)
			cmbPredmet.setSelectedIndex(0);
		cmbSemestar.setSelectedIndex(0);
	}

	
	private void initialize() {
		frmSekcije = new JFrame();
		frmSekcije.setResizable(false);
		frmSekcije.getContentPane().setBackground(Color.WHITE);
		frmSekcije.setTitle("Dodavanje nove sekcije\r\n");		
		frmSekcije.setBounds(100, 100, 450, 300);
		frmSekcije.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmSekcije.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Naziv sekcije:");
		lblNewLabel.setBounds(18, 192, 111, 16);
		frmSekcije.getContentPane().add(lblNewLabel);
		
		txtNaziv = new JTextField();
		txtNaziv.setBounds(18, 213, 240, 36);
		frmSekcije.getContentPane().add(txtNaziv);
		txtNaziv.setColumns(10);
		
		Object columnNames[] = { "ID", "Radno mjesto"};
        String[][] rowData = new String[0][2];
        table = new JTable(rowData, columnNames) {
		    @Override
		    public boolean isCellEditable(int row, int column) {
		       return false;
		    }
		};
        scrollPane = new JScrollPane(table);
        frmSekcije.getContentPane().add(scrollPane, BorderLayout.CENTER);
        scrollPane.setBounds(292, 192, 441 , 218);
        
        JButton btnPovratak = new JButton("Povratak");
        btnPovratak.setBackground(new Color(128, 0, 0));
        btnPovratak.setForeground(new Color(255, 255, 255));
        btnPovratak.setBounds(643, 470, 90, 29);
        frmSekcije.getContentPane().add(btnPovratak);
        btnPovratak.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				NastavnikHome window = new NastavnikHome(currentUser.getKorisnik());
				frmSekcije.dispose();
	    		window.frmNastavnikHome.setVisible(true);
			}
		});
        
        btnDodati = new JButton("Dodati");
        btnDodati.setForeground(new Color(255, 255, 255));
        btnDodati.setBackground(new Color(128, 0, 0));
        btnDodati.addActionListener(new ActionListener()
        {
        @Override
          public void actionPerformed(ActionEvent e)
          {
        	
        	{
        	Sekcija sek;
        	if(izmjena==false)
        		sek=new Sekcija();
        	else
        		sek=sekcije.get(selected);
          	  if(txtNaziv.getText().length()<1)
        	  {
        		  JOptionPane.showMessageDialog(null, "Niste unijeli naziv sekcije!", "Greška", JOptionPane.ERROR_MESSAGE);
        		  txtNaziv.requestFocus();
        	  }
        	  else if(tryParseInt(txtGodina.getText())==false)
        	  {
        		  JOptionPane.showMessageDialog(null, "Niste unijeli godinu!", "Greška", JOptionPane.ERROR_MESSAGE);
        		  txtGodina.requestFocus();
        	  }
        	  else
        	  {
        	  	em.getTransaction().begin();
        	  	sek.setNaziv(txtNaziv.getText());
        	  	sek.setGodina(Integer.parseInt(txtGodina.getText()));
        	  	sek.setPredmet((Predmet)cmbPredmet.getSelectedItem());
        	  	sek.setSemestar((String)cmbSemestar.getSelectedItem());
        	  	if(!((Predmet)cmbPredmet.getSelectedItem()).getSekcije().contains(sek))
        	  		((Predmet)cmbPredmet.getSelectedItem()).getSekcije().add(sek);
        	    em.persist(em.merge(sek));
        	    em.merge((Predmet)cmbPredmet.getSelectedItem());
        	    em.getTransaction().commit();        	    
        	    OsvjeziTabelu(true);       
        	    OcistiPolja();
        	  }
          }
          }
        });
        btnDodati.setBounds(0, 479, 96, 36);
        frmSekcije.getContentPane().add(btnDodati);
        
        btnIzmijeniti = new JButton("Izmijeniti");
        btnIzmijeniti.setBackground(new Color(128, 0, 0));
        btnIzmijeniti.setForeground(new Color(255, 255, 255));
        btnIzmijeniti.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
        	 if(izmjena==false)
          	 {        		 
          	 if(selected>-1)
          	 {
          		 izmjena=true;
          		 btnIzmijeniti.setText("Odustati");
          		 btnDodati.setText("Potvrditi");
          		 btnObrisati.setEnabled(false);
          		 Sekcija sek = sekcije.get(selected);
          		 txtNaziv.setText(sek.getNaziv());
          		 txtGodina.setText(new Integer(sek.getGodina()).toString());
          		 cmbSemestar.setSelectedItem(sek.getSemestar());
          		 if(predmeti.size()>0)
          			 cmbPredmet.setSelectedItem(sek.getPredmet());
          		 txtNaziv.requestFocus();
          	 }
          	 }
          	 else
          	 {         		 
          		 izmjena=false;
          		 OcistiPolja();
          	 }
          }
        
        });
        btnIzmijeniti.setBounds(93, 479, 90, 36);
        frmSekcije.getContentPane().add(btnIzmijeniti);
        
        btnObrisati = new JButton("Obrisati");
        btnObrisati.setForeground(new Color(255, 255, 255));
        btnObrisati.setBackground(new Color(128, 0, 0));
        btnObrisati.addActionListener(new ActionListener()
        {
        	@Override
          public void actionPerformed(ActionEvent e)
          {
          if(selected>-1)
	         {
	        	try
	        	{
        	    em.getTransaction().begin();
        	       
        	    Sekcija temp = sekcije.get(selected);
        	    em.remove(em.merge(temp));
        	    em.getTransaction().commit();        	    
        	 	  DefaultTableModel model = new DefaultTableModel();
            	  model=(DefaultTableModel) table.getModel();
                  model.removeRow(selected);
                  selected=-1;
	        	}
	        	catch (Exception err)
	        	  {
	        		  JOptionPane.showMessageDialog(null, "Nije moguće obrisati sekciju koja ima aktivne ispite!", "Greška", JOptionPane.ERROR_MESSAGE);
	        	  }
              }
             }
        
        });
        btnObrisati.setBounds(179, 479, 96, 36);
        frmSekcije.getContentPane().add(btnObrisati);
        
        JLabel lblPrezime = new JLabel("Godina:");
        lblPrezime.setBounds(18, 342, 111, 16);
        frmSekcije.getContentPane().add(lblPrezime);
        
        txtGodina = new JTextField();
        txtGodina.setColumns(10);
        txtGodina.setBounds(93, 332, 165, 36);
        frmSekcije.getContentPane().add(txtGodina);
        
        JLabel lblAdresa = new JLabel("Semestar:");
        lblAdresa.setBounds(18, 394, 111, 16);
        frmSekcije.getContentPane().add(lblAdresa);
        
        
        
        cmbPredmet = new JComboBox();
        cmbPredmet.setBackground(new Color(255, 255, 255));
        for(Predmet p : predmeti)
        	cmbPredmet.addItem(p);
        cmbPredmet.setBounds(18, 282, 240, 36);
        frmSekcije.getContentPane().add(cmbPredmet);
        
        JLabel lblUsmjerenje = new JLabel("Predmet:");
        lblUsmjerenje.setBounds(18, 262, 111, 16);
        frmSekcije.getContentPane().add(lblUsmjerenje);
        
        cmbSemestar = new JComboBox();
        cmbSemestar.setBackground(new Color(255, 255, 255));
        cmbSemestar.addItem("I");
        cmbSemestar.addItem("II");
        cmbSemestar.addItem("III");
        cmbSemestar.addItem("IV");
        cmbSemestar.addItem("V");
        cmbSemestar.addItem("VI");
        cmbSemestar.addItem("VII");
        cmbSemestar.addItem("VIII");
        cmbSemestar.setBounds(93, 384, 165, 36);
        frmSekcije.getContentPane().add(cmbSemestar);
        
        JLabel label = new JLabel("Filter po:");
        label.setBounds(326, 441, 58, 16);
        frmSekcije.getContentPane().add(label);
        
        cmbFilter = new JComboBox();
        cmbFilter.setForeground(new Color(255, 255, 255));
        cmbFilter.setBackground(new Color(128, 0, 0));
        cmbFilter.addActionListener (new ActionListener () {
	        public void actionPerformed(ActionEvent e) {
	        	txtFilter.setText("");
	            OsvjeziTabelu(true);
	        }
	    });
        cmbFilter.setModel(new DefaultComboBoxModel(new String[] {"predmet", "godina", "semestar"}));
        cmbFilter.setBounds(389, 436, 111, 27);
        frmSekcije.getContentPane().add(cmbFilter);
        
        txtFilter = new JTextField();
        txtFilter.getDocument().addDocumentListener(new DocumentListener()
        {
            public void changedUpdate(DocumentEvent arg0) 
            {
           
            }
            public void insertUpdate(DocumentEvent arg0) 
            {
            	if(cmbFilter.getSelectedIndex()==0)
            		sekcije = select(sekcijeSource, having(on(Sekcija.class).getPredmet().getNaziv(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==1)
            		sekcije = select(sekcijeSource, having(on(Sekcija.class).getGodinaString(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==2)
            		sekcije = select(sekcijeSource, having(on(Sekcija.class).getSemestar(), Matchers.containsString(txtFilter.getText())));            	
            	OsvjeziTabelu(false);
            }

            public void removeUpdate(DocumentEvent arg0) 
            {
            	if(cmbFilter.getSelectedIndex()==0)
            		sekcije = select(sekcijeSource, having(on(Sekcija.class).getPredmet().getNaziv(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==1)
            		sekcije = select(sekcijeSource, having(on(Sekcija.class).getGodinaString(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==2)
            		sekcije = select(sekcijeSource, having(on(Sekcija.class).getSemestar(), Matchers.containsString(txtFilter.getText())));            	
            	OsvjeziTabelu(false);
            }           
        });
        txtFilter.setColumns(10);
        txtFilter.setBounds(500, 436, 233, 28);
        frmSekcije.getContentPane().add(txtFilter);
        
        JLabel label_1 = new JLabel("");
        label_1.setBounds(0, 0, 745, 181);
        Image naslovnica= new ImageIcon (this.getClass().getResource("/cover.jpg")).getImage();
		label_1.setIcon(new ImageIcon(naslovnica));
        frmSekcije.getContentPane().add(label_1);
        
        panel = new JPanel();
        panel.setBounds(0, 180, 275, 332);
        frmSekcije.getContentPane().add(panel);
        
        
        OsvjeziTabelu(true);
        
        table.addMouseListener(new MouseAdapter() {
	        public void mouseClicked(MouseEvent e) {
	            
	        		if (e.getClickCount()==2)
	        		{
	        			JTable target = (JTable) e.getSource();
	        			int row = target.getSelectedRow();	        			
	        			Sekcija sek=sekcije.get(row);
	        			SekcijaStudenti window=new SekcijaStudenti(sek);
	        			window.frmSekcijaStudenti.setVisible(true);
	        		}
	        }
	        

	    });
       
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            			@Override
			public void valueChanged(ListSelectionEvent e) 
            {
            	selected = table.getSelectedRow();
            	OcistiPolja();
				
			}
        });
		
        frmSekcije.setBackground(new Color(255, 255, 255));        
        frmSekcije.setBounds(100, 100, 450, 300);
        frmSekcije.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmSekcije.setSize(new Dimension(751, 547));
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frmSekcije.setLocation(dim.width/2-frmSekcije.getSize().width/2, dim.height/2-frmSekcije.getSize().height/2);
	}
}
