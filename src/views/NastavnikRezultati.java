package views;


import java.awt.EventQueue;

import javax.swing.JFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.imageio.ImageIO;
import javax.persistence.*;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

import java.awt.Color;

import javax.swing.JLabel;

import java.awt.Font;
import java.awt.Image;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.JTextField;

import models.Ispit;
import models.Nastavnik;
import models.Predmet;
import models.RezultatIspita;
import models.Sekcija;
import models.Student;
import models.Usmjerenje;
import models.Korisnik;

import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;

import java.awt.Color;
import java.io.InputStream;

import static ch.lambdaj.Lambda.*;
import org.hamcrest.Matchers;
import javax.swing.JPanel;

public class NastavnikRezultati {

	public JFrame frmNastavnikRezultati;
	private EntityManagerFactory emf;
	private EntityManager em;
	private JTable table;
	private  JScrollPane scrollPane;
	private int selected;
	public Nastavnik currentUser;
	private ArrayList<Sekcija> sekcije;
	private ArrayList<Ispit> ispiti;
	private ArrayList<Student> studenti;
	private List<RezultatIspita> rezultati;
	private List<RezultatIspita> rezultatiSource;
	private JTextField txtBodovi;
	private JComboBox cmbStudent;
	private JComboBox cmbSekcija;
	private JComboBox cmbIspit;
	private JComboBox cmbFilter;
	private boolean izmjena=false;
	private JButton btnObrisati;
	private JButton btnIzmijeniti;
	private JButton btnDodati;
	private JTextField txtFilter;
	private JLabel lblNewLabel;
	private JPanel panel;
	private void OsvjeziTabelu(boolean reload)
	{
		em.close();
		em = emf.createEntityManager();
		em.getEntityManagerFactory().getCache().evictAll();
		Query q = em.createQuery("select t from Nastavnik t where t.id=" + currentUser.getId());
	    
	    currentUser = (Nastavnik)q.getResultList().get(0);
	    if(reload)
	    {
		rezultati = new ArrayList<RezultatIspita>();
		for(Predmet p : currentUser.getPredmeti())
		{
			for(Sekcija s : p.getSekcije())
			{
				for(Ispit isp : s.getIspiti())
				{
					if(isp.getRezultat()!=null)
						rezultati.add(isp.getRezultat());
				}
			}
		}	
		rezultatiSource=rezultati;
	    }
	    Object columnNames[] = { "Sekcija", "Datum ispita", "Student", "Vrsta ispita", "Bodovi"};
        String[][] rowData = new String[rezultati.size()][5];
        
        int i=0;
	    for(RezultatIspita rez : rezultati)
	    {
	    	rowData[i][0]=rez.getIspit().getSekcija().getNaziv();
	    	rowData[i][1]=rez.getIspit().getDatum();
	    	rowData[i][2]=rez.getStudent().getBr_indeksa() + " - " + rez.getStudent().getIme() + " " + rez.getStudent().getPrezime();
	    	rowData[i][3]=rez.getIspit().getTip()==0 ? "Ispit" : "Predispitna aktivnost";
	    	rowData[i][4]=rez.getBodovi() + " / " + rez.getIspit().getBodovi();	    	
	    	i++;
	    }
	    			
	    table.setModel(new DefaultTableModel(rowData,columnNames));
	    table.getColumnModel().getColumn(4).setMinWidth(90);
	    table.getColumnModel().getColumn(4).setMaxWidth(90);
	    table.getColumnModel().getColumn(4).setPreferredWidth(90);
	    table.getColumnModel().getColumn(3).setMinWidth(80);
	    table.getColumnModel().getColumn(3).setMaxWidth(80);
	    table.getColumnModel().getColumn(3).setPreferredWidth(80);
	    table.getColumnModel().getColumn(0).setMinWidth(80);
	    table.getColumnModel().getColumn(0).setMaxWidth(80);
	    table.getColumnModel().getColumn(0).setPreferredWidth(80);
	    table.getColumnModel().getColumn(1).setMinWidth(90);
	    table.getColumnModel().getColumn(1).setMaxWidth(90);
	    table.getColumnModel().getColumn(1).setPreferredWidth(90);
	}
	boolean tryParseFloat(String value)  
	{  
	     try  
	     {  
	         Float.parseFloat(value);  
	         return true;  
	      } catch(NumberFormatException nfe)  
	      {  
	          return false;  
	      }  
	}
	public NastavnikRezultati(Nastavnik nast) {
		currentUser=nast;
		emf = Persistence.createEntityManagerFactory("StudentskaSluzba");		
	    em = emf.createEntityManager();			   
	    em.getEntityManagerFactory().getCache().evictAll();
	    sekcije=new ArrayList<Sekcija>();
	    studenti=new ArrayList<Student>();
	    ispiti=new ArrayList<Ispit>();
	    for(Predmet p : currentUser.getPredmeti())
	    {
	    	for(Sekcija s : p.getSekcije())
	    	{
	    		if(!sekcije.contains(s))
	    			sekcije.add(s);
	    	}
	    }
	    Collections.sort(studenti, new Comparator<Student>() {
	        public int compare(Student s1, Student s2) {
	           return Integer.valueOf(s1.getPrezime().compareTo(s2.getPrezime()));
	        }
	});
	    initialize();
	}
	
	private void OcistiPolja()
	{
		izmjena=false;
		btnIzmijeniti.setText("Izmijeniti");
		btnDodati.setText("Dodati");
 		btnObrisati.setEnabled(true);
		txtBodovi.setText("");		
	}

	
	
	private void RefreshIspiti()
	{
		ispiti=new ArrayList<Ispit>();
		cmbIspit.removeAllItems();
    	Sekcija sek=(Sekcija) cmbSekcija.getSelectedItem();
    	if(sek!=null)
    	{    	
    	ispiti.addAll(sek.getIspiti());    			
		for(Ispit isp : ispiti)
        	cmbIspit.addItem(isp);
    	}
        
	}
	
	private void RefreshStudenti()
	{
		studenti=new ArrayList<Student>();
		cmbStudent.removeAllItems();
		Ispit isp=(Ispit) cmbIspit.getSelectedItem();
		if(isp!=null)
		{    	    
    	studenti.addAll(isp.getStudenti());    	
		for(Student s : studenti)
        	cmbStudent.addItem(s);
		}
        
	}
	private void initialize() {
		frmNastavnikRezultati = new JFrame();
		frmNastavnikRezultati.setResizable(false);
		frmNastavnikRezultati.getContentPane().setBackground(Color.WHITE);
		frmNastavnikRezultati.setTitle("Rezultati ispita");		
		frmNastavnikRezultati.setBounds(100, 100, 450, 300);
		frmNastavnikRezultati.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmNastavnikRezultati.getContentPane().setLayout(null);
		
		
		Object columnNames[] = { "ID", "Radno mjesto"};
        String[][] rowData = new String[0][2];
        table = new JTable(rowData, columnNames) {
		    @Override
		    public boolean isCellEditable(int row, int column) {
		       return false;
		    }
		};
		
		
		
		
        scrollPane = new JScrollPane(table);
        frmNastavnikRezultati.getContentPane().add(scrollPane, BorderLayout.CENTER);
        scrollPane.setBounds(280, 196, 452 , 247);
        
        JButton btnPovratak = new JButton("Povratak");
        btnPovratak.setBackground(new Color(128, 0, 0));
        btnPovratak.setForeground(new Color(255, 255, 255));
        btnPovratak.setBounds(642, 492, 90, 29);
        frmNastavnikRezultati.getContentPane().add(btnPovratak);
        btnPovratak.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if (((currentUser.getKorisnik()).getRole())==1)
				{
				NastavnikHome window = new NastavnikHome(currentUser.getKorisnik());
				frmNastavnikRezultati.dispose();
	    		window.frmNastavnikHome.setVisible(true);
				}
				else
				{
					AsistentHome window = new AsistentHome(currentUser.getKorisnik());
					frmNastavnikRezultati.dispose();
		    		window.frmAsistentHome.setVisible(true);
				}
			}
		});
        
        btnDodati = new JButton("Dodati");
        btnDodati.setBackground(new Color(128, 0, 0));
        btnDodati.setForeground(new Color(255, 255, 255));
        btnDodati.addActionListener(new ActionListener()
        {
        @Override
          public void actionPerformed(ActionEvent e)
          {
        	if(selected>-1)
        	{
        	RezultatIspita rez;
        	if(izmjena==false)
        	{
        		rez=new RezultatIspita();        		
        	}
        	else
        		rez=rezultati.get(selected);
        	if(cmbSekcija.getSelectedItem()==null || cmbIspit.getSelectedItem()==null || cmbStudent.getSelectedItem()==null)
      	  {
      		  JOptionPane.showMessageDialog(null, "Niste odabrali sve potrebne podatke!", "Greška", JOptionPane.ERROR_MESSAGE);      		  
      	  }
        	else if(tryParseFloat(txtBodovi.getText())==false)
        	  {
        		  JOptionPane.showMessageDialog(null, "Niste unijeli broj osvojenih bodova!", "Greška", JOptionPane.ERROR_MESSAGE);
        		  txtBodovi.requestFocus();
        	  }
        	  else if(Float.parseFloat(txtBodovi.getText())>((Ispit)cmbIspit.getSelectedItem()).getBodovi())
        	  {
        		  JOptionPane.showMessageDialog(null, "Broj bodova ne može biti veći od ukupnog broja koliko nosi odabrani ispit!", "Greška", JOptionPane.ERROR_MESSAGE);
        		  txtBodovi.requestFocus();
        	  }
        	  else
        	  {
        	  	em.getTransaction().begin();        	  	
        	  	rez.setBodovi(Float.parseFloat(txtBodovi.getText()));
        	  	rez.setIspit((Ispit)cmbIspit.getSelectedItem());
        	  	rez.setStudent((Student)cmbStudent.getSelectedItem());
        	  	em.persist(em.merge(rez));
        	    em.merge((Student)cmbStudent.getSelectedItem());
        	    em.merge((Ispit)cmbIspit.getSelectedItem());
        	    em.getTransaction().commit();        	    
        	    OsvjeziTabelu(true);       
        	    OcistiPolja();
        	  }
          }
          }
        });
        btnDodati.setBounds(0, 485, 90, 42);
        frmNastavnikRezultati.getContentPane().add(btnDodati);
        
        btnIzmijeniti = new JButton("Izmijeniti");
        btnIzmijeniti.setForeground(new Color(255, 255, 255));
        btnIzmijeniti.setBackground(new Color(128, 0, 0));
                btnIzmijeniti.setBounds(83, 485, 90, 42);
        frmNastavnikRezultati.getContentPane().add(btnIzmijeniti);
        
        btnObrisati = new JButton("Obrisati");
        btnObrisati.setForeground(new Color(255, 255, 255));
        btnObrisati.setBackground(new Color(128, 0, 0));
        btnObrisati.addActionListener(new ActionListener()
        {
        	@Override
          public void actionPerformed(ActionEvent e)
          {
          if(selected>-1)
	         {
	        	try
	        	{
        	    em.getTransaction().begin();
        	       
        	    RezultatIspita rez = rezultati.get(selected);
        	    em.remove(em.merge(rez));
        	    em.getTransaction().commit();        	    
        	 	  DefaultTableModel model = new DefaultTableModel();
            	  model=(DefaultTableModel) table.getModel();
                  model.removeRow(selected);
                  selected=-1;
                  izmjena=false;
                  OcistiPolja();
	        	}
	        	catch (Exception err)
	        	  {
	        		  JOptionPane.showMessageDialog(null, "Nije moguće obrisati odabrani unos!", "Greška", JOptionPane.ERROR_MESSAGE);
	        	  }
              }
             }
        
        });
        btnObrisati.setBounds(172, 485, 90, 42);
        frmNastavnikRezultati.getContentPane().add(btnObrisati);
        
        JLabel lblPrezime = new JLabel("Osvojeno bodova:");
        lblPrezime.setFont(new Font("Tahoma", Font.BOLD, 14));
        lblPrezime.setBounds(9, 427, 142, 16);
        frmNastavnikRezultati.getContentPane().add(lblPrezime);
        
        txtBodovi = new JTextField();
        txtBodovi.setColumns(10);
        txtBodovi.setBounds(160, 421, 84, 28);
        frmNastavnikRezultati.getContentPane().add(txtBodovi);
        
        JLabel lblAdresa = new JLabel("Sekcija:");
        lblAdresa.setFont(new Font("Tahoma", Font.BOLD, 14));
        lblAdresa.setBounds(9, 184, 117, 16);
        frmNastavnikRezultati.getContentPane().add(lblAdresa);
        
        
        cmbStudent = new JComboBox();
        cmbStudent.setBackground(new Color(255, 255, 255));
        cmbStudent.setBounds(9, 367, 235, 35);
        frmNastavnikRezultati.getContentPane().add(cmbStudent);
        
        JLabel lblUsmjerenje = new JLabel("Student:");
        lblUsmjerenje.setFont(new Font("Tahoma", Font.BOLD, 14));
        lblUsmjerenje.setBounds(12, 338, 117, 16);
        frmNastavnikRezultati.getContentPane().add(lblUsmjerenje);
        
        DefaultComboBoxModel model = new DefaultComboBoxModel(sekcije.toArray());

        cmbSekcija = new JComboBox(model);
        cmbSekcija.setBackground(new Color(255, 255, 255));
        cmbSekcija.setForeground(new Color(0, 0, 0));
        cmbSekcija.setBounds(9, 213, 235, 35);
        frmNastavnikRezultati.getContentPane().add(cmbSekcija);
        
        cmbIspit = new JComboBox();
        cmbIspit.setBackground(new Color(255, 255, 255));
        cmbIspit.setBounds(9, 290, 235, 35);
        frmNastavnikRezultati.getContentPane().add(cmbIspit);
        
        JLabel lblIspit = new JLabel("Ispit");
        lblIspit.setFont(new Font("Tahoma", Font.BOLD, 14));
        lblIspit.setBounds(9, 261, 148, 16);
        frmNastavnikRezultati.getContentPane().add(lblIspit);
        
        JLabel label = new JLabel("Filter po:");
        label.setBounds(290, 456, 58, 16);
        frmNastavnikRezultati.getContentPane().add(label);
        
        cmbFilter = new JComboBox();
        cmbFilter.setForeground(new Color(255, 255, 255));
        cmbFilter.setBackground(new Color(128, 0, 0));
        cmbFilter.addActionListener (new ActionListener () {
	        public void actionPerformed(ActionEvent e) {
	        	txtFilter.setText("");
	            OsvjeziTabelu(true);
	        }
	    });
        cmbFilter.setModel(new DefaultComboBoxModel(new String[] {"predmet", "sekcija", "datum ispita", "broj indeksa", "ime studenta", "prezime studenta", "broj bodova"}));
        cmbFilter.setBounds(345, 451, 142, 27);
        frmNastavnikRezultati.getContentPane().add(cmbFilter);
        
        txtFilter = new JTextField();
        txtFilter.getDocument().addDocumentListener(new DocumentListener()
        {
            public void changedUpdate(DocumentEvent arg0) 
            {
           
            }
            public void insertUpdate(DocumentEvent arg0) 
            {
            	if(cmbFilter.getSelectedIndex()==0)
            		rezultati = select(rezultatiSource, having(on(RezultatIspita.class).getIspit().getSekcija().getPredmet().getNaziv(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==1)
            		rezultati = select(rezultatiSource, having(on(RezultatIspita.class).getIspit().getSekcija().getNaziv(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==2)
            		rezultati = select(rezultatiSource, having(on(RezultatIspita.class).getIspit().getDatum(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==3)
            		rezultati = select(rezultatiSource, having(on(RezultatIspita.class).getStudent().getBr_indeksa(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==4)
            		rezultati = select(rezultatiSource, having(on(RezultatIspita.class).getStudent().getIme(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==5)
            		rezultati = select(rezultatiSource, having(on(RezultatIspita.class).getStudent().getPrezime(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==6)
            		rezultati = select(rezultatiSource, having(on(RezultatIspita.class).getBodoviString(), Matchers.containsString(txtFilter.getText())));
            	OsvjeziTabelu(false);
            }

            public void removeUpdate(DocumentEvent arg0) 
            {
            	if(cmbFilter.getSelectedIndex()==0)
            		rezultati = select(rezultatiSource, having(on(RezultatIspita.class).getIspit().getSekcija().getPredmet().getNaziv(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==1)
            		rezultati = select(rezultatiSource, having(on(RezultatIspita.class).getIspit().getSekcija().getNaziv(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==2)
            		rezultati = select(rezultatiSource, having(on(RezultatIspita.class).getIspit().getDatum(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==3)
            		rezultati = select(rezultatiSource, having(on(RezultatIspita.class).getStudent().getBr_indeksa(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==4)
            		rezultati = select(rezultatiSource, having(on(RezultatIspita.class).getStudent().getIme(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==5)
            		rezultati = select(rezultatiSource, having(on(RezultatIspita.class).getStudent().getPrezime(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==6)
            		rezultati = select(rezultatiSource, having(on(RezultatIspita.class).getBodoviString(), Matchers.containsString(txtFilter.getText())));
            	OsvjeziTabelu(false);
            }           
        });

        txtFilter.setColumns(10);
        txtFilter.setBounds(488, 451, 244, 28);
        frmNastavnikRezultati.getContentPane().add(txtFilter);
        
        lblNewLabel = new JLabel("");
        lblNewLabel.setBounds(0, 0, 744, 183);
        Image naslovnica= new ImageIcon (this.getClass().getResource("/cover.jpg")).getImage();
		lblNewLabel.setIcon(new ImageIcon(naslovnica));
        frmNastavnikRezultati.getContentPane().add(lblNewLabel);
        
        panel = new JPanel();
        panel.setBounds(0, 180, 268, 347);
        frmNastavnikRezultati.getContentPane().add(panel);
        
        
        OsvjeziTabelu(true);
        
        RefreshIspiti();
        RefreshStudenti();
        cmbSekcija.addActionListener (new ActionListener () {
	        public void actionPerformed(ActionEvent e) 
	        {
	        	RefreshIspiti();
	        	RefreshStudenti();
	        }
	    });
        cmbIspit.addActionListener (new ActionListener () {
	        public void actionPerformed(ActionEvent e) 
	        {
	        	RefreshStudenti();
	        }
	    });
       
        btnIzmijeniti.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
        	 if(izmjena==false)
          	 {        		 
          	 if(selected>-1)
          	 {
          		 izmjena=true;
          		 btnIzmijeniti.setText("Odustati");
          		 btnDodati.setText("Potvrditi");
          		 btnObrisati.setEnabled(false);
          		 RezultatIspita rez = rezultati.get(selected);           		 
          		 txtBodovi.setText(new Float(rez.getBodovi()).toString());
          		 int t=0;
          		 for(Sekcija s : sekcije)
          		 {
          			 if(s.getId()==rez.getIspit().getSekcija().getId())
          			 {
          				 break;
          			 }
          			 t++;
          		 }
          		 cmbSekcija.setSelectedIndex(t);
          		 t=0;
         		 for(Ispit s : ispiti)
         		 {
         			 if(s.getId()==rez.getIspit().getId())
         			 {
         				 break;
         			 }
         			 t++;
         		 }
          		 cmbIspit.setSelectedIndex(t);
          		 
          		 t=0;
        		 for(Student s : studenti)
        		 {
        			 if(s.getId()==rez.getStudent().getId())
        			 {
        				 break;
        			 }
        			 t++;
        		 }
         		 cmbStudent.setSelectedIndex(t);
          	 }
          	 }
          	 else
          	 {         		 
          		 izmjena=false;
          		 OcistiPolja();
          	 }
          }
        
        });

        
     
	
        
        
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            			@Override
			public void valueChanged(ListSelectionEvent e) 
            {
            	selected = table.getSelectedRow();
            	OcistiPolja();
				
			}
        });
		
		
		
        frmNastavnikRezultati.setBackground(new Color(255, 255, 255));        
        frmNastavnikRezultati.setBounds(100, 100, 450, 300);
        frmNastavnikRezultati.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmNastavnikRezultati.setSize(new Dimension(750, 562));
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frmNastavnikRezultati.setLocation(dim.width/2-frmNastavnikRezultati.getSize().width/2, dim.height/2-frmNastavnikRezultati.getSize().height/2);

	}
}
