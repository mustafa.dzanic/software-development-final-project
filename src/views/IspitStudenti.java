package views;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;

import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;

import models.Ispit;
import models.RezultatIspita;
import models.Sekcija;
import models.Student;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.DefaultComboBoxModel;

import static ch.lambdaj.Lambda.*;

import org.hamcrest.Matchers;


public class IspitStudenti {

	public JFrame frmIspitStudenti;
	private EntityManagerFactory emf;
	private EntityManager em;
	private List<Student> studenti;
	private List<Student> studentiSource;
	private Ispit currentIspit;
	private JTable tableRezultati;
	private JScrollPane scrollPaneRezultati;
	private JTextField txtFilter;
	private JComboBox cmbFilter;
	
	public IspitStudenti(Ispit isp) {
		currentIspit=isp;
		emf = Persistence.createEntityManagerFactory("StudentskaSluzba");		
	    em = emf.createEntityManager();			   
	    em.getEntityManagerFactory().getCache().evictAll();
	    initialize();
	}

	private void OsvjeziTabelu(boolean reload)
	{
		if(reload)
		{
		studenti=new ArrayList<Student>();
		studenti.addAll(currentIspit.getStudenti());
	    studentiSource=studenti;
		}
		Object columnNamesRezultati[] = { "Broj indeksa", "Ime", "Prezime", "Usmjerenje", "Semestar"};
        Object[][] rowDataRezultati = new Object[studenti.size()][5];
        int j=0;
	    for(Student s : studenti)
	    {
	    	rowDataRezultati[j][0]=s.getBr_indeksa();
	    	rowDataRezultati[j][1]=s.getIme();
	    	rowDataRezultati[j][2]=s.getPrezime();
	    	rowDataRezultati[j][3]=s.getUsmjerenje().getSkracenica();
	    	rowDataRezultati[j][4]=s.getSemestar()==null ? "Nije upisan" : s.getSemestar();	    	
	    	j++;
	    }
	    
	    tableRezultati.setModel(new DefaultTableModel(rowDataRezultati,columnNamesRezultati));
	    
	    tableRezultati.getColumnModel().getColumn(0).setMinWidth(100);
	    tableRezultati.getColumnModel().getColumn(0).setMaxWidth(100);
	    tableRezultati.getColumnModel().getColumn(0).setPreferredWidth(100);
	    
	    tableRezultati.getColumnModel().getColumn(3).setMinWidth(80);
	    tableRezultati.getColumnModel().getColumn(3).setMaxWidth(80);
	    tableRezultati.getColumnModel().getColumn(3).setPreferredWidth(80);	   	   
	    
	}

	
	private void initialize() {
		frmIspitStudenti = new JFrame();
		frmIspitStudenti.setResizable(false);
		frmIspitStudenti.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmIspitStudenti.getContentPane().setBackground(Color.WHITE);
		frmIspitStudenti.setTitle("Lista studenata");
		frmIspitStudenti.setBounds(100, 100, 450, 300);
		frmIspitStudenti.getContentPane().setLayout(null);
		frmIspitStudenti.setSize(new Dimension(750, 522));
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frmIspitStudenti.setLocation(dim.width/2-frmIspitStudenti.getSize().width/2, dim.height/2-frmIspitStudenti.getSize().height/2);
		
		JLabel lblNaslov = new JLabel(currentIspit.getSekcija().toString());
		lblNaslov.setForeground(Color.DARK_GRAY);
		lblNaslov.setFont(new Font("Calibri", Font.PLAIN, 24));
		lblNaslov.setBounds(10, 177, 527, 28);
		frmIspitStudenti.getContentPane().add(lblNaslov);
		
		Object columnNamesRezultati[] = { "Broj indeksa", "Ime", "Prezime", "Usmjerenje", "Semestar"};
        Object[][] rowDataRezultati = new Object[0][5];		       
        tableRezultati = new JTable(rowDataRezultati, columnNamesRezultati){
        @Override
	    public boolean isCellEditable(int row, int column) {
	       return false;
	    }
        };
		OsvjeziTabelu(true);
		
		 tableRezultati.addMouseListener(new MouseAdapter() {
		        public void mouseClicked(MouseEvent e) {
		            
		        		if (e.getClickCount()==2)
		        		{
		        			JTable target = (JTable) e.getSource();
		        			int row = target.getSelectedRow();	        			
		        			Student stud=studentiSource.get(row);
		        			StudentProfil window=new StudentProfil(stud);
		        			window.frmInfoOStudentu.setVisible(true);
		        		}
		        }
		        

		    });
		
        scrollPaneRezultati = new JScrollPane(tableRezultati);
        frmIspitStudenti.getContentPane().add(scrollPaneRezultati, BorderLayout.CENTER);
        scrollPaneRezultati.setBounds(12, 206, 720, 228);
        
        JLabel label = new JLabel("Filter po:");
        label.setBounds(125, 452, 58, 16);
        frmIspitStudenti.getContentPane().add(label);
        
        cmbFilter = new JComboBox();
        cmbFilter.addActionListener (new ActionListener () {
	        public void actionPerformed(ActionEvent e) {
	        	txtFilter.setText("");
	            OsvjeziTabelu(true);
	        }
	    });
        
        cmbFilter.setModel(new DefaultComboBoxModel(new String[] {"broj indeksa", "ime", "prezime"}));
        cmbFilter.setBounds(181, 447, 158, 27);
        frmIspitStudenti.getContentPane().add(cmbFilter);
        
        txtFilter = new JTextField();
        txtFilter.getDocument().addDocumentListener(new DocumentListener()
        {
            public void changedUpdate(DocumentEvent arg0) 
            {
           
            }
            public void insertUpdate(DocumentEvent arg0) 
            {
            	if(cmbFilter.getSelectedIndex()==0)
            		studenti = select(studentiSource, having(on(Student.class).getBr_indeksa(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==1)
            		studenti = select(studentiSource, having(on(Student.class).getIme(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==2)
            		studenti = select(studentiSource, having(on(Student.class).getPrezime(), Matchers.containsString(txtFilter.getText())));
            	OsvjeziTabelu(false);
            }

            public void removeUpdate(DocumentEvent arg0) 
            {
            	if(cmbFilter.getSelectedIndex()==0)
            		studenti = select(studentiSource, having(on(Student.class).getBr_indeksa(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==1)
            		studenti = select(studentiSource, having(on(Student.class).getIme(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==2)
            		studenti = select(studentiSource, having(on(Student.class).getPrezime(), Matchers.containsString(txtFilter.getText())));
            	OsvjeziTabelu(false);
           }           
        });

        
        
        txtFilter.setColumns(10);
        txtFilter.setBounds(339, 446, 312, 28);
        frmIspitStudenti.getContentPane().add(txtFilter);
        
        JLabel lblDatum = new JLabel(currentIspit.getDatum() + " u " + currentIspit.getVrijeme());
        lblDatum.setForeground(Color.DARK_GRAY);
        lblDatum.setFont(new Font("Lucida Grande", Font.BOLD | Font.ITALIC, 14));
        lblDatum.setBounds(553, 177, 179, 28);
        frmIspitStudenti.getContentPane().add(lblDatum);
        
        JLabel label_1 = new JLabel("");
        label_1.setBounds(0, 0, 744, 169);
        Image naslovnica= new ImageIcon (this.getClass().getResource("/cover.jpg")).getImage();
        label_1.setIcon(new ImageIcon(naslovnica));
        frmIspitStudenti.getContentPane().add(label_1);

	}

}
