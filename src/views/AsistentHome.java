package views;

import java.awt.Dimension;

import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;

import javax.imageio.ImageIO;
import javax.persistence.*;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.border.Border;

import models.Korisnik;
import models.Nastavnik;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.InputStream;
import java.util.List;
import javax.swing.JPanel;

public class AsistentHome {

	public JFrame frmAsistentHome;
	private Nastavnik currentUser;
	private EntityManagerFactory emf;
	private EntityManager em;
	private String path;
	
	public AsistentHome(Korisnik user) 
	{
		emf = Persistence.createEntityManagerFactory("StudentskaSluzba");		
	    em = emf.createEntityManager();			   
	    em.getEntityManagerFactory().getCache().evictAll();
	    Query q = em.createQuery("select t from Nastavnik t");	    
	    List<Nastavnik> lista = q.getResultList();	    
	    for(Nastavnik nast : lista)
	    {
	    	if(nast.getKorisnik().getId()==user.getId())
	    	{
	    		currentUser=nast;
	    		break;
	    	}
	    }
		initialize();
	}

	private void initialize() {
		frmAsistentHome = new JFrame();
		frmAsistentHome.getContentPane().setForeground(new Color(255, 255, 255));
		frmAsistentHome.setResizable(false);
		frmAsistentHome.getContentPane().setBackground(Color.WHITE);
		frmAsistentHome.setTitle("Asistent");
		frmAsistentHome.setBackground(Color.WHITE);
		frmAsistentHome.setBounds(100, 100, 450, 300);
		frmAsistentHome.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAsistentHome.setSize(new Dimension(750, 500));
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frmAsistentHome.setLocation(dim.width/2-frmAsistentHome.getSize().width/2, dim.height/2-frmAsistentHome.getSize().height/2);
		frmAsistentHome.getContentPane().setLayout(null);
		
		JButton btnOdjava = new JButton("Odjava");
		btnOdjava.setBounds(647, 423, 85, 29);
		btnOdjava.setForeground(new Color(255, 255, 255));
		btnOdjava.setBackground(new Color(128, 0, 0));
		frmAsistentHome.getContentPane().add(btnOdjava);
		Image ispiti= new ImageIcon (this.getClass().getResource("/ispiti.png")).getImage();
		Border emptyBorder4 = BorderFactory.createEmptyBorder();
		
		JButton btnPredispitneAktivnosti = new JButton("");
		btnPredispitneAktivnosti.setBounds(289, 220, 164, 159);
		Image predispiti= new ImageIcon (this.getClass().getResource("/predispit.png")).getImage();
		btnPredispitneAktivnosti.setIcon(new ImageIcon(predispiti));
		Border emptyBorder5 = BorderFactory.createEmptyBorder();
		btnPredispitneAktivnosti.setBorder(emptyBorder5);
		btnPredispitneAktivnosti.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				Predispiti window = new Predispiti(currentUser);
				window.currentUser=currentUser;
	    		frmAsistentHome.dispose();
	    		window.frmIspiti.setVisible(true);
			}			
		});
		frmAsistentHome.getContentPane().add(btnPredispitneAktivnosti);
		
		JButton btnRezultati = new JButton("");
		btnRezultati.setBounds(486, 220, 164, 159);
		Image rezultati= new ImageIcon (this.getClass().getResource("/rezultati.png")).getImage();
		btnRezultati.setIcon(new ImageIcon(rezultati));
		Border emptyBorder6 = BorderFactory.createEmptyBorder();
		btnRezultati.setBorder(emptyBorder6);
		btnRezultati.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				NastavnikRezultati window = new NastavnikRezultati(currentUser);
				window.currentUser=currentUser;
	    		frmAsistentHome.dispose();
	    		window.frmNastavnikRezultati.setVisible(true);
			}			
		});
		frmAsistentHome.getContentPane().add(btnRezultati);
		
		JLabel naslovna = new JLabel("");
		naslovna.setBounds(0, 0, 744, 180);
		Image naslovnica= new ImageIcon (this.getClass().getResource("/cover.jpg")).getImage();
		naslovna.setIcon(new ImageIcon(naslovnica));
		frmAsistentHome.getContentPane().add(naslovna);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 179, 205, 286);
		frmAsistentHome.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblImeIPrezime = new JLabel(currentUser.getIme() + " " + currentUser.getPrezime());
		lblImeIPrezime.setBounds(22, 13, 144, 18);
		panel.add(lblImeIPrezime);
		lblImeIPrezime.setFont(new Font("Lucida Grande", Font.BOLD, 13));
		lblImeIPrezime.setForeground(Color.DARK_GRAY); 
		
		JLabel lblNewLabel_1 = new JLabel("");
		em.getTransaction().begin();
		if ((currentUser.getKorisnik()).getClasspath()==null){
			path = "C:\\\\Users\\\\Mustafa\\\\Desktop\\\\defaultIcon.jpg";
		}
		else
		{
			path = currentUser.getKorisnik().getClasspath();
			lblNewLabel_1.setIcon(new ImageIcon(path));
		}
		em.persist(currentUser);
		em.getTransaction().commit();
		lblNewLabel_1.setBounds(22, 44, 158, 155);
		panel.add(lblNewLabel_1);
		
		JButton btnSlika = new JButton("Slika");
		btnSlika.setBackground(new Color(128, 0, 0));
		btnSlika.setForeground(new Color(255, 255, 255));
		btnSlika.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				em.getTransaction().begin();
				JFileChooser chooser = new JFileChooser();
				chooser.showOpenDialog(null);
				File f = chooser.getSelectedFile();
				String filename = f.getAbsolutePath();
				currentUser.getKorisnik().setClasspath(filename);
				ImageIcon icon=new ImageIcon(filename);
				lblNewLabel_1.setIcon(icon);
				em.persist(currentUser);
				em.getTransaction().commit();
			}
		});
		btnSlika.setBounds(60, 244, 85, 29);
		panel.add(btnSlika);
		
		JLabel lblOdaberiteOpciju = new JLabel("Odaberite opciju");
		lblOdaberiteOpciju.setBounds(392, 183, 192, 35);
		lblOdaberiteOpciju.setFont(new Font("Tahoma", Font.BOLD, 18));
		frmAsistentHome.getContentPane().add(lblOdaberiteOpciju);
		
		JLabel label = new JLabel("Predispiti");
		label.setFont(new Font("Tahoma", Font.BOLD, 16));
		label.setBounds(328, 392, 85, 16);
		frmAsistentHome.getContentPane().add(label);
		
		JLabel label_1 = new JLabel("Rezultati");
		label_1.setFont(new Font("Tahoma", Font.BOLD, 16));
		label_1.setBounds(533, 392, 85, 16);
		frmAsistentHome.getContentPane().add(label_1);
		
		btnOdjava.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				Login window = new Login();
				frmAsistentHome.dispose();
	    		window.frmPrijavaKorisnika.setVisible(true);
			}
		});
	}

}
