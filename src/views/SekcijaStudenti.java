package views;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;

import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;

import models.RezultatIspita;
import models.Sekcija;
import models.Student;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.DefaultComboBoxModel;

import static ch.lambdaj.Lambda.*;

import org.hamcrest.Matchers;


public class SekcijaStudenti {

	public JFrame frmSekcijaStudenti;
	private EntityManagerFactory emf;
	private EntityManager em;
	private List<Student> studenti;
	private List<Student> studentiSource;
	private Sekcija currentSekcija;
	private JTable tableRezultati;
	private JScrollPane scrollPaneRezultati;
	private JTextField txtFilter;
	private JComboBox cmbFilter;
	private JLabel label_2;
	
	public SekcijaStudenti(Sekcija sekcija) {
		currentSekcija=sekcija;
		emf = Persistence.createEntityManagerFactory("StudentskaSluzba");		
	    em = emf.createEntityManager();			   
	    em.getEntityManagerFactory().getCache().evictAll();
	    initialize();
	}

	private void OsvjeziTabelu(boolean reload)
	{
		if(reload)
		{
		studenti=new ArrayList<Student>();
		studenti.addAll(currentSekcija.getStudenti());
	    studentiSource=studenti;
		}
		Object columnNamesRezultati[] = { "Broj indeksa", "Ime", "Prezime", "Usmjerenje", "Semestar"};
        Object[][] rowDataRezultati = new Object[studenti.size()][5];
        int j=0;
	    for(Student s : studenti)
	    {
	    	rowDataRezultati[j][0]=s.getBr_indeksa();
	    	rowDataRezultati[j][1]=s.getIme();
	    	rowDataRezultati[j][2]=s.getPrezime();
	    	rowDataRezultati[j][3]=s.getUsmjerenje().getSkracenica();
	    	rowDataRezultati[j][4]=s.getSemestar()==null ? "Nije upisan" : s.getSemestar();	    	
	    	j++;
	    }
	    
	    tableRezultati.setModel(new DefaultTableModel(rowDataRezultati,columnNamesRezultati));
	    
	    tableRezultati.getColumnModel().getColumn(0).setMinWidth(100);
	    tableRezultati.getColumnModel().getColumn(0).setMaxWidth(100);
	    tableRezultati.getColumnModel().getColumn(0).setPreferredWidth(100);
	    
	    tableRezultati.getColumnModel().getColumn(3).setMinWidth(80);
	    tableRezultati.getColumnModel().getColumn(3).setMaxWidth(80);
	    tableRezultati.getColumnModel().getColumn(3).setPreferredWidth(80);	   	   
	    
	}

	
	private void initialize() {
		frmSekcijaStudenti = new JFrame();
		frmSekcijaStudenti.setResizable(false);
		frmSekcijaStudenti.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmSekcijaStudenti.getContentPane().setBackground(Color.WHITE);
		frmSekcijaStudenti.setTitle("Lista studenata");
		frmSekcijaStudenti.setBounds(100, 100, 450, 300);
		frmSekcijaStudenti.getContentPane().setLayout(null);
		frmSekcijaStudenti.setSize(new Dimension(745, 536));
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frmSekcijaStudenti.setLocation(dim.width/2-frmSekcijaStudenti.getSize().width/2, dim.height/2-frmSekcijaStudenti.getSize().height/2);
		try {
			InputStream stream = this.getClass().getResourceAsStream("/logo.jpg");
			ImageIcon image;
			image = new ImageIcon(ImageIO.read(stream));
		} catch (Exception e1) 
		{
			
		}
		
		JLabel lblNaslov = new JLabel("Lista studenata na sekciji " + currentSekcija.getNaziv());
		lblNaslov.setForeground(Color.DARK_GRAY);
		lblNaslov.setFont(new Font("Calibri", Font.PLAIN, 24));
		lblNaslov.setBounds(18, 184, 527, 28);
		frmSekcijaStudenti.getContentPane().add(lblNaslov);
		
		Object columnNamesRezultati[] = { "Broj indeksa", "Ime", "Prezime", "Usmjerenje", "Semestar"};
        Object[][] rowDataRezultati = new Object[0][5];		       
        tableRezultati = new JTable(rowDataRezultati, columnNamesRezultati);
		OsvjeziTabelu(true);
		
        scrollPaneRezultati = new JScrollPane(tableRezultati);
        frmSekcijaStudenti.getContentPane().add(scrollPaneRezultati, BorderLayout.CENTER);
        scrollPaneRezultati.setBounds(12, 215, 709, 238);
        
        JLabel label = new JLabel("Filter po:");
        label.setBounds(22, 466, 58, 16);
        frmSekcijaStudenti.getContentPane().add(label);
        
        cmbFilter = new JComboBox();
        cmbFilter.addActionListener (new ActionListener () {
	        public void actionPerformed(ActionEvent e) {
	        	txtFilter.setText("");
	            OsvjeziTabelu(true);
	        }
	    });
        
        cmbFilter.setModel(new DefaultComboBoxModel(new String[] {"broj indeksa", "ime", "prezime"}));
        cmbFilter.setBounds(80, 461, 158, 27);
        frmSekcijaStudenti.getContentPane().add(cmbFilter);
        
        txtFilter = new JTextField();
        txtFilter.getDocument().addDocumentListener(new DocumentListener()
        {
            public void changedUpdate(DocumentEvent arg0) 
            {
           
            }
            public void insertUpdate(DocumentEvent arg0) 
            {
            	if(cmbFilter.getSelectedIndex()==0)
            		studenti = select(studentiSource, having(on(Student.class).getBr_indeksa(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==1)
            		studenti = select(studentiSource, having(on(Student.class).getIme(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==2)
            		studenti = select(studentiSource, having(on(Student.class).getPrezime(), Matchers.containsString(txtFilter.getText())));
            	OsvjeziTabelu(false);
            }

            public void removeUpdate(DocumentEvent arg0) 
            {
            	if(cmbFilter.getSelectedIndex()==0)
            		studenti = select(studentiSource, having(on(Student.class).getBr_indeksa(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==1)
            		studenti = select(studentiSource, having(on(Student.class).getIme(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==2)
            		studenti = select(studentiSource, having(on(Student.class).getPrezime(), Matchers.containsString(txtFilter.getText())));
            	OsvjeziTabelu(false);
           }           
        });

        txtFilter.setColumns(10);
        txtFilter.setBounds(237, 460, 484, 28);
        frmSekcijaStudenti.getContentPane().add(txtFilter);
        
        label_2 = new JLabel("");
        label_2.setBounds(0, 0, 739, 181);
        Image naslovnica= new ImageIcon (this.getClass().getResource("/cover.jpg")).getImage();
		label_2.setIcon(new ImageIcon(naslovnica));
        frmSekcijaStudenti.getContentPane().add(label_2);

	}

}
