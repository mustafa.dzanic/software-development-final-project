package views;

import models.Korisnik;
import models.Predmet;
import models.Usmjerenje;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import static ch.lambdaj.Lambda.*;

import javax.imageio.ImageIO;
import javax.persistence.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.eclipse.persistence.exceptions.DatabaseException;
import org.hamcrest.Matchers;

import java.awt.Color;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Usmjerenja {

	public JFrame frmUsmjerenja;
	private EntityManagerFactory emf;
	private EntityManager em;
	private JTable table;
	private JScrollPane scrollPane;
	private JTable tablePredmeti;
	private JScrollPane scrollPanePredmeti;
	private int selected;
	public Korisnik currentUser;
	private List<Usmjerenje> usmjerenja;
	private List<Usmjerenje> usmjerenjaSource;
	private List<Predmet> predmeti;
	private JTextField txtNazivUsmjerenja;
	private JTextField txtKratakNaziv;
	private boolean izmjena=false;
	private JButton btnObrisati;
	private JButton btnIzmijeniti;
	private JButton btnDodati;
	private JTextField txtFilter;
	private JLabel label;
	private JPanel panel;
	
	private void OsvjeziTabelu(boolean reload)
	{
		Query q = em.createQuery("select t from Usmjerenje t");
	    
		if(reload)
		{
	    usmjerenja = q.getResultList();
	    usmjerenjaSource=usmjerenja;
		}
        Object columnNames[] = { "ID", "Naziv usmjerenja", "Kratak naziv", "Predmeti"};
        String[][] rowData = new String[usmjerenja.size()][4];
        
        int i=0;
	    for(Usmjerenje us : usmjerenja)
	    {
	    	rowData[i][0]=new Integer(us.getId()).toString();
	    	rowData[i][1]=us.getNaziv();
	    	rowData[i][2]=us.getSkracenica();
	    	rowData[i][3]=us.getListPredmeti();
	    	i++;
	    }
	    			
	    table.setModel(new DefaultTableModel(rowData,columnNames));
	    table.getColumnModel().getColumn(0).setMinWidth(20);
        table.getColumnModel().getColumn(0).setMaxWidth(50);
        table.getColumnModel().getColumn(0).setPreferredWidth(35);
        table.getColumnModel().getColumn(2).setMinWidth(80);
        table.getColumnModel().getColumn(2).setMaxWidth(80);
        table.getColumnModel().getColumn(2).setPreferredWidth(80);
	}
	
	public Usmjerenja() {
		emf = Persistence.createEntityManagerFactory("StudentskaSluzba");		
	    em = emf.createEntityManager();			   
	    em.getEntityManagerFactory().getCache().evictAll();
	    Query q = em.createQuery("select t from Predmet t");	    
	    predmeti = q.getResultList();
		initialize();		
	}
	
	private void OcistiPolja()
	{
		btnIzmijeniti.setText("Izmijeniti");
  		 btnDodati.setText("Dodati");
  		 btnObrisati.setEnabled(true);
  		 txtNazivUsmjerenja.setText("");
  		 txtKratakNaziv.setText(""); 
  		 SetSelectedPredmeti(null);
	}
	
	private void SetSelectedPredmeti(Collection<Predmet> collection)
	{
			for(int i=0; i<tablePredmeti.getRowCount(); i++)
    	    {
    	    	tablePredmeti.setValueAt(false, i, 2);
    	    }
		if(collection!=null)
		{
			ArrayList<Predmet> list=new ArrayList<Predmet>();
			list.addAll(collection);
			for(int i=0; i<list.size(); i++)
			{
				if(predmeti.contains(list.get(i)))
					tablePredmeti.setValueAt(true, predmeti.indexOf(list.get(i)), 2);
			}
		}
	}

	
	private void initialize() {
		frmUsmjerenja = new JFrame();
		frmUsmjerenja.setResizable(false);
		frmUsmjerenja.getContentPane().setBackground(Color.WHITE);
		frmUsmjerenja.setTitle("Usmjerenja");
		frmUsmjerenja.setBounds(100, 100, 450, 300);
		frmUsmjerenja.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmUsmjerenja.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Naziv usmjerenja:");
		lblNewLabel.setBounds(18, 189, 163, 16);
		frmUsmjerenja.getContentPane().add(lblNewLabel);
		
		txtNazivUsmjerenja = new JTextField();
		txtNazivUsmjerenja.setBounds(18, 219, 277, 36);
		frmUsmjerenja.getContentPane().add(txtNazivUsmjerenja);
		txtNazivUsmjerenja.setColumns(50);
		
		Object columnNames[] = { "ID", "Naziv usmjerenja", "Kratak naziv", "Predmeti"};
        String[][] rowData = new String[0][2];
        table = new JTable(rowData, columnNames) {
		    @Override
		    public boolean isCellEditable(int row, int column) {
		       return false;
		    }
		};
        scrollPane = new JScrollPane(table);
        scrollPane.setBounds(333, 193, 399, 277);
        frmUsmjerenja.getContentPane().add(scrollPane);
        
        JButton btnPovratak = new JButton("Povratak");
        btnPovratak.setForeground(new Color(255, 255, 255));
        btnPovratak.setBackground(new Color(128, 0, 0));
        btnPovratak.setBounds(642, 522, 90, 29);
        frmUsmjerenja.getContentPane().add(btnPovratak);
        btnPovratak.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				AdminPanel window = new AdminPanel(currentUser);
				frmUsmjerenja.dispose();
	    		window.frmAdministrator.setVisible(true);
			}
		});
        
        btnDodati = new JButton("Dodati");
        btnDodati.setForeground(new Color(255, 255, 255));
        btnDodati.setBackground(new Color(128, 0, 0));
        btnDodati.setBounds(0, 522, 107, 42);
        btnDodati.addActionListener(new ActionListener()
        {
        @Override
          public void actionPerformed(ActionEvent e)
          {
        	Usmjerenje us;
        	if(izmjena==false)
        		us=new Usmjerenje();
        	else
        		us=usmjerenja.get(selected);
        	  if(txtNazivUsmjerenja.getText().length()<1)
        	  {
        		  JOptionPane.showMessageDialog(null, "Niste unijeli naziv usmjerenja!", "Greška", JOptionPane.ERROR_MESSAGE);
        		  txtNazivUsmjerenja.requestFocus();
        	  }
        	  else if(txtKratakNaziv.getText().length()<1)
        	  {
        		  JOptionPane.showMessageDialog(null, "Niste unijeli kratak naziv usmjerenja!", "Greška", JOptionPane.ERROR_MESSAGE);
        		  txtKratakNaziv.requestFocus();
        	  }
        	  else
        	  {
        	  	em.getTransaction().begin();        	    
        	    us.setNaziv(txtNazivUsmjerenja.getText());
        	    us.setSkracenica(txtKratakNaziv.getText().substring(0, txtKratakNaziv.getText().length()>5 ? 5 : txtKratakNaziv.getText().length()));
        	    ArrayList<Predmet> odabraniPredmeti=new ArrayList<Predmet>();
        	    for(int i=0; i<tablePredmeti.getRowCount(); i++)
        	    {
        	    	if(tablePredmeti.getValueAt(i, 2).equals(true))
        	    	{
        	    		odabraniPredmeti.add(predmeti.get(i));
        	    	}
        	    }
        	    us.setPredmeti(odabraniPredmeti);
        	    em.persist(us);
        	    em.getTransaction().commit();
        	    txtFilter.setText("");
        	    OsvjeziTabelu(true);        
        	    txtNazivUsmjerenja.setText("");
        	    txtKratakNaziv.setText("");
        	    OcistiPolja();
        	  }
          }
        });
        frmUsmjerenja.getContentPane().add(btnDodati);
        
        btnIzmijeniti = new JButton("Izmijeniti");
        btnIzmijeniti.setForeground(new Color(255, 255, 255));
        btnIzmijeniti.setBackground(new Color(128, 0, 0));
        btnIzmijeniti.setBounds(104, 522, 107, 42);
        btnIzmijeniti.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
        	 if(izmjena==false)
         	 {        		 
         	 if(selected>-1)
         	 {
         		 izmjena=true;
         		 btnIzmijeniti.setText("Odustati");
         		 btnDodati.setText("Potvrditi");
         		 btnObrisati.setEnabled(false);
         		 Usmjerenje us=usmjerenja.get(selected);
         		 txtNazivUsmjerenja.setText(us.getNaziv());
         		 txtKratakNaziv.setText(us.getSkracenica());
         		 SetSelectedPredmeti(us.getPredmeti());
         		 txtNazivUsmjerenja.requestFocus();
         	 }
         	 }
         	 else
         	 {         		 
         		 izmjena=false;
         		 OcistiPolja();
         	 }
          }
        
        });
        frmUsmjerenja.getContentPane().add(btnIzmijeniti);
        
        btnObrisati = new JButton("Obrisati");
        btnObrisati.setBackground(new Color(128, 0, 0));
        btnObrisati.setForeground(new Color(255, 255, 255));
        btnObrisati.setBounds(209, 522, 107, 42);
        btnObrisati.addActionListener(new ActionListener()
        {
        	@Override
          public void actionPerformed(ActionEvent e)
          {
          if(selected>-1)
	         {
	        	try
	        	{
	        		em.getTransaction().begin();
	        	       
	        	    Usmjerenje temp = usmjerenja.get(selected); 
	        	    em.remove(temp);
	        	    em.getTransaction().commit();        	    
	        	 	  DefaultTableModel model = new DefaultTableModel();
	            	  model=(DefaultTableModel) table.getModel();
	                  model.removeRow(selected);
	                  selected=-1;
	        	}
	        	catch (Exception err)
	        	{
	        		JOptionPane.showMessageDialog(null, "Nije moguće obrisati usmjerenje koje ima pridružene predmete!", "Greška", JOptionPane.ERROR_MESSAGE);
	        	}
	        	                  
              }
             }
        
        });
        
        frmUsmjerenja.getContentPane().add(btnObrisati);
        
        JLabel lblKratakNazivdo = new JLabel("Kratak naziv (do 5 karaktera):");
        lblKratakNazivdo.setBounds(18, 278, 187, 16);
        frmUsmjerenja.getContentPane().add(lblKratakNazivdo);
        
        txtKratakNaziv = new JTextField();
        txtKratakNaziv.setBounds(217, 268, 78, 36);
        txtKratakNaziv.setColumns(5);
        frmUsmjerenja.getContentPane().add(txtKratakNaziv);

        Object columnNamesPredmeti[] = { "ID", "Predmet", "Uključen"};        
		int j=0;
		Object[][] rowDataPredmeti = new Object[predmeti.size()][3];
	    for(Predmet pr : predmeti)
	    {
	    	rowDataPredmeti[j][0]=new Integer(pr.getId()).toString();
	    	rowDataPredmeti[j][1]=pr.getNaziv();
	    	rowDataPredmeti[j][2]=false;
	    	j++;
	    }
	    
	    tablePredmeti = new JTable(rowDataPredmeti, columnNamesPredmeti) {
		    @Override
		    public boolean isCellEditable(int row, int column) {
		       if(column==2)
		    	   return true;
		       else
		    	   return false;
		    }
		    public Class getColumnClass(int column) {
                switch (column) {
                    case 0:
                        return String.class;
                    case 1:
                        return String.class;
                    default:
                        return Boolean.class;
                       
                }
		    }
		};    
        scrollPanePredmeti = new JScrollPane(tablePredmeti);
        scrollPanePredmeti.setBounds(20, 346, 275, 163);
        frmUsmjerenja.getContentPane().add(scrollPanePredmeti);
        tablePredmeti.getColumnModel().getColumn(0).setMinWidth(20);
        tablePredmeti.getColumnModel().getColumn(0).setMaxWidth(50);
        tablePredmeti.getColumnModel().getColumn(0).setPreferredWidth(35);
        tablePredmeti.getColumnModel().getColumn(2).setMinWidth(60);
        tablePredmeti.getColumnModel().getColumn(2).setMaxWidth(60);
        tablePredmeti.getColumnModel().getColumn(2).setPreferredWidth(60);
        
        JLabel lblListaPredmeta = new JLabel("Lista predmeta:");
        lblListaPredmeta.setBounds(18, 317, 187, 16);
        frmUsmjerenja.getContentPane().add(lblListaPredmeta);
        
        JLabel lblFilterPoNazivu = new JLabel("Filter po nazivu usmjerenja:");
        lblFilterPoNazivu.setFont(new Font("Tahoma", Font.BOLD, 14));
        lblFilterPoNazivu.setBounds(333, 489, 213, 16);
        frmUsmjerenja.getContentPane().add(lblFilterPoNazivu);
        
        txtFilter = new JTextField();
        txtFilter.setBounds(531, 483, 201, 28);
        txtFilter.getDocument().addDocumentListener(new DocumentListener()
        {

            public void changedUpdate(DocumentEvent arg0) 
            {
           
            }
            public void insertUpdate(DocumentEvent arg0) 
            {
            	usmjerenja = select(usmjerenjaSource, having(on(Usmjerenje.class).getNaziv(), Matchers.containsString(txtFilter.getText())));
            	OsvjeziTabelu(false);
            }

            public void removeUpdate(DocumentEvent arg0) 
            {
            	usmjerenja = select(usmjerenjaSource, having(on(Usmjerenje.class).getNaziv(), Matchers.containsString(txtFilter.getText())));
            	OsvjeziTabelu(false);
            }           
        });

        txtFilter.setColumns(10);
        frmUsmjerenja.getContentPane().add(txtFilter);
        
        label = new JLabel("");
        label.setBounds(0, 0, 744, 180);
        Image naslovnica= new ImageIcon (this.getClass().getResource("/cover.jpg")).getImage();
		label.setIcon(new ImageIcon(naslovnica));
        frmUsmjerenja.getContentPane().add(label);
        
        panel = new JPanel();
        panel.setBounds(0, 179, 315, 385);
        frmUsmjerenja.getContentPane().add(panel);
        
        OsvjeziTabelu(true);
        
       
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            			@Override
			public void valueChanged(ListSelectionEvent e) 
            {
            	selected = table.getSelectedRow();
				
			}
        });

		
		frmUsmjerenja.setSize(new Dimension(750, 599));
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frmUsmjerenja.setLocation(dim.width/2-frmUsmjerenja.getSize().width/2, dim.height/2-frmUsmjerenja.getSize().height/2);
	}
}
