package views;

import models.Korisnik;

import java.awt.EventQueue;

import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.swing.JFrame;

import javax.swing.JLabel;
import javax.swing.ImageIcon;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.InputStream;
import java.util.List;


import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;

import javax.swing.SwingConstants;
public class Login {

	public JFrame frmPrijavaKorisnika;
	private JTextField txtKorisnickoIme;
	private JPasswordField txtLozinka;
	private EntityManagerFactory emf;
	private EntityManager em;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
				Login window = new Login();
					window.frmPrijavaKorisnika.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	public Login() {
		
		initialize();
	}

	private void initialize() 
	{
		emf = Persistence.createEntityManagerFactory("StudentskaSluzba");
		em = emf.createEntityManager();
		
		em.getTransaction().begin();
		Korisnik k=new Korisnik();
		k.setUsername("admin");
		k.setLozinka("admin");
		k.setRole(0);
		em.persist(k);
		
		
		
		em.getTransaction().commit();
		
		frmPrijavaKorisnika = new JFrame();
		frmPrijavaKorisnika.setResizable(false);
		frmPrijavaKorisnika.setTitle("Registar Fakulteta Elektrotehnike");
		frmPrijavaKorisnika.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmPrijavaKorisnika.setSize(new Dimension(544, 503));
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frmPrijavaKorisnika.setLocation(dim.width/2-frmPrijavaKorisnika.getSize().width/2, dim.height/2-	frmPrijavaKorisnika.getSize().height/2);
	
		frmPrijavaKorisnika.getContentPane().setBackground(Color.WHITE);
		frmPrijavaKorisnika.getContentPane().setLayout(null);
		
		JLabel label = new JLabel("");
		Image untz_logo1= new ImageIcon (this.getClass().getResource("/logo.jpg")).getImage();
		label.setIcon(new ImageIcon(untz_logo1));
		label.setBounds(73, 27, 144, 129);
		frmPrijavaKorisnika.getContentPane().add(label);
		
	    JLabel label_1 = new JLabel("");
		Image shield= new ImageIcon (this.getClass().getResource("/login_lock_logo.png")).getImage();
		label_1.setIcon(new ImageIcon(shield));
		label_1.setBounds(12, 175, 236, 280);
		frmPrijavaKorisnika.getContentPane().add(label_1);
		
	JLabel lblNewLabel = new JLabel("UNIVERZITET U TUZLI");
		lblNewLabel.setBounds(229, 58, 246, 26);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Dialog", Font.BOLD, 20));
		lblNewLabel.setForeground(new Color(139, 0, 0));
		frmPrijavaKorisnika.getContentPane().add(lblNewLabel);
		
		JLabel lblFakultetElektrotehnike = new JLabel("FAKULTET ELEKTROTEHNIKE");
		lblFakultetElektrotehnike.setBounds(222, 97, 265, 16);
		lblFakultetElektrotehnike.setHorizontalAlignment(SwingConstants.CENTER);
		lblFakultetElektrotehnike.setFont(new Font("Dialog", Font.BOLD, 18));
		frmPrijavaKorisnika.getContentPane().add(lblFakultetElektrotehnike);
		
		JLabel lblNewLabel_1 = new JLabel("Unesite svoje\r\n korisničke podatke");
		lblNewLabel_1.setBounds(274, 214, 252, 26);
		lblNewLabel_1.setFont(new Font("Calibri Light", Font.BOLD, 16));
		lblNewLabel_1.setForeground(new Color(0, 0, 0));
		frmPrijavaKorisnika.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Login");
		lblNewLabel_2.setBounds(309, 253, 118, 30);
		lblNewLabel_2.setFont(new Font("Dialog", Font.PLAIN, 17));
		frmPrijavaKorisnika.getContentPane().add(lblNewLabel_2);
		
		JLabel lblLozinka = new JLabel("Šifra");
		lblLozinka.setBounds(309, 337, 118, 16);
		lblLozinka.setFont(new Font("Dialog", Font.PLAIN, 17));
		frmPrijavaKorisnika.getContentPane().add(lblLozinka);
		
		txtKorisnickoIme = new JTextField();
		txtKorisnickoIme.setBounds(309, 296, 178, 28);
		txtKorisnickoIme.setFont(new Font("Lucida Grande", Font.PLAIN, 14));
		frmPrijavaKorisnika.getContentPane().add(txtKorisnickoIme);
		txtKorisnickoIme.setColumns(10);
		
		JButton btnOK = new JButton("Prijavi se");
		btnOK.setForeground(new Color(255, 250, 250));
		btnOK.setBackground(new Color(128, 0, 0));
		btnOK.setBounds(355, 421, 98, 29);
		frmPrijavaKorisnika.getContentPane().add(btnOK);
		
		txtLozinka = new JPasswordField();
		txtLozinka.setBounds(309, 366, 178, 28);
		txtLozinka.setFont(new Font("Lucida Grande", Font.PLAIN, 14));
		frmPrijavaKorisnika.getContentPane().add(txtLozinka);
		
		JLabel lblLogo = new JLabel("");
		lblLogo.setBounds(37, 0, 70, 70);
		
		try {
			InputStream stream = this.getClass().getResourceAsStream("img/logo.jpg");
			ImageIcon image;
			image = new ImageIcon(ImageIO.read(stream));
			lblLogo.setIcon(image);
		} catch (Exception e1) 
		{
			
		}
		frmPrijavaKorisnika.getContentPane().add(lblLogo);
		
		JLabel lblNewLabel_3 = new JLabel("");
		lblNewLabel_3.setBounds(22, 13, 188, 169);
		frmPrijavaKorisnika.getContentPane().add(lblNewLabel_3);
		
				
		btnOK.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{				
				Query q = em.createQuery("select t from Korisnik t where t.username='" + txtKorisnickoIme.getText() + "' and t.lozinka='" + txtLozinka.getText() + "'");
			    List<Korisnik> users = q.getResultList();
			    if(users.size()==0)
			    {
			    	JOptionPane.showMessageDialog(null, "Unijeli ste pogresne podatke!", "Greska", JOptionPane.ERROR_MESSAGE);
			    }
			    else
			    {
			    	Korisnik user=users.get(0);
			    	if(user.getRole()==0)
			    	{
			    		AdminPanel window = new AdminPanel(user);
			    		frmPrijavaKorisnika.dispose();
			    		window.frmAdministrator.setVisible(true);
			    	}
			    	else if(user.getRole()==1)
			    	{
			    		NastavnikHome window = new NastavnikHome(user);
			    		frmPrijavaKorisnika.dispose();
			    		window.frmNastavnikHome.setVisible(true);
			    	}
			    	else if(user.getRole()==2)
			    	{
			    		StudentHome window = new StudentHome(user);
			    		frmPrijavaKorisnika.dispose();
			    		window.frmStudentHome.setVisible(true);
			    	}
			    	else if(user.getRole()==3)
			    	{
			    		AsistentHome window = new AsistentHome(user);
			    		frmPrijavaKorisnika.dispose();
			    		window.frmAsistentHome.setVisible(true);
			    	}
			    }
			}
			
		});
        
	}
}
