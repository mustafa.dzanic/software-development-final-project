package models;


import javax.swing.JFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import java.awt.Dimension;

import java.awt.Toolkit;

import javax.persistence.*;
import javax.swing.ImageIcon;


import java.awt.Color;
import org.hamcrest.Matchers;
import javax.swing.JLabel;

import java.awt.Font;
import java.awt.Image;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import static ch.lambdaj.Lambda.*;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.JTextField;

import models.Nastavnik;

import models.Korisnik;

import javax.swing.JTable;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import javax.swing.JComboBox;


import views.AdminPanel;
import views.ProfesorProfil;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JPanel;
public class Nastavnici {

	public JFrame frmNastavnici;
	private JTextField txtIme;
	private EntityManagerFactory emf;
	private EntityManager em;
	private JTable table;
	private  JScrollPane scrollPane;
	private JTable tablePredmeti;
	private JScrollPane scrollPanePredmeti;
	private List<Predmet> predmeti;
	private int selected;
	public Korisnik currentUser;
	private JCheckBox chckbxNewCheckBox;
	private List<Nastavnik> nastavnici;
	private List<Nastavnik> nastavniciSource;
	private List<RadnoMjesto> radnaMjesta;
	private List<Korisnik> korisnici;
	private JTextField txtPrezime;
	private JTextField txtBrojIndeksa;
	private JTextField txtUsername;
	private JTextField txtPassword;
	private JComboBox cmbUsmjerenja;
	private JComboBox cmbFilter;
	private boolean izmjena=false;
	private JButton btnObrisati;
	private JButton btnIzmijeniti;
	private JButton btnDodati;
	private JTextField txtFilter;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final Action action = new SwingAction();
	private void OsvjeziTabelu(boolean reload)
	{
		Query q = em.createQuery("select t from Nastavnik t");
	   
		if(reload)
		{
			nastavnici = q.getResultList();
	    	nastavniciSource=nastavnici;
		}
	    q = em.createQuery("select t from Korisnik t");
	    
	    korisnici = q.getResultList();
        Object columnNames[] = { "ID", "Ime", "Prezime", "Radno mjesto", "Predmeti"};
        String[][] rowData = new String[nastavnici.size()][5];
        
        int i=0;
	    for(Nastavnik n : nastavnici)
	    {
	    	rowData[i][0]=new Integer(n.getId()).toString();
	    	rowData[i][1]=n.getIme();
	    	rowData[i][2]=n.getPrezime();
	    	rowData[i][3]=n.getRadnoMjesto().getNaziv();
	    	rowData[i][4]=n.getListPredmeti();
	    	i++;
	    }
	    			
	    table.setModel(new DefaultTableModel(rowData,columnNames));
        table.getColumnModel().getColumn(0).setMinWidth(20);
        table.getColumnModel().getColumn(0).setMaxWidth(50);
        table.getColumnModel().getColumn(0).setPreferredWidth(35);	
        
	}
	
	public Nastavnici() {
		emf = Persistence.createEntityManagerFactory("StudentskaSluzba");		
	    em = emf.createEntityManager();			   
	    em.getEntityManagerFactory().getCache().evictAll();
	    Query q = em.createQuery("select t from RadnoMjesto t");	    
	    radnaMjesta = q.getResultList();
	    q = em.createQuery("select t from Predmet t");	    
	    predmeti = q.getResultList();
		initialize();
	}
	
	private void SetSelectedPredmeti(Collection<Predmet> collection)
	{
			for(int i=0; i<tablePredmeti.getRowCount(); i++)
    	    {
    	    	tablePredmeti.setValueAt(false, i, 2);
    	    }
		if(collection!=null)
		{
			ArrayList<Predmet> list=new ArrayList<Predmet>();
			list.addAll(collection);
			for(int i=0; i<list.size(); i++)
			{
				if(predmeti.contains(list.get(i)))
					tablePredmeti.setValueAt(true, predmeti.indexOf(list.get(i)), 2);
			}
		}
	}

	private void OcistiPolja()
	{
		btnIzmijeniti.setText("Izmijeniti");
		btnDodati.setText("Dodati");
 		btnObrisati.setEnabled(true);
		txtIme.setText("");
		txtPrezime.setText("");
		txtUsername.setText("");
		txtPassword.setText("");
		txtBrojIndeksa.setText("");
		if(radnaMjesta.size()>0)
			cmbUsmjerenja.setSelectedIndex(0);
		SetSelectedPredmeti(null);
	}
	
	private void initialize() {
		frmNastavnici = new JFrame();
		frmNastavnici.setTitle("Dodavanje novog nastavnika");
		frmNastavnici.getContentPane().setBackground(Color.WHITE);
		frmNastavnici.setBounds(100, 100, 450, 300);
		frmNastavnici.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JLabel lblNewLabel = new JLabel("Ime:");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel.setBounds(18, 196, 111, 16);
		frmNastavnici.getContentPane().add(lblNewLabel);
		
		txtIme = new JTextField();
		txtIme.setBounds(102, 190, 177, 28);
		frmNastavnici.getContentPane().add(txtIme);
		txtIme.setColumns(10);
		
		Object columnNames[] = { "ID", "Radno mjesto"};
        String[][] rowData = new String[0][2];
        table = new JTable(rowData, columnNames) {
		    @Override
		    public boolean isCellEditable(int row, int column) {
		       return false;
		    }
		};
        scrollPane = new JScrollPane(table);
        scrollPane.setBounds(314, 190, 408, 332);
        frmNastavnici.getContentPane().add(scrollPane);
        
        JButton btnPovratak = new JButton("Povratak");
        btnPovratak.setBackground(new Color(128, 0, 0));
        btnPovratak.setForeground(new Color(255, 255, 255));
        btnPovratak.setBounds(632, 570, 90, 29);
        frmNastavnici.getContentPane().add(btnPovratak);
        btnPovratak.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				AdminPanel window = new AdminPanel(currentUser);
				frmNastavnici.dispose();
	    		window.frmAdministrator.setVisible(true);
			}
		});
        
   	 table.addMouseListener(new MouseAdapter() {
	        public void mouseClicked(MouseEvent e) {
	            
	        		if (e.getClickCount()==2)
	        		{
	        			JTable target = (JTable) e.getSource();
	        			int row = target.getSelectedRow();	        			
	        			Nastavnik nast=nastavniciSource.get(row);
	        			ProfesorProfil window=new ProfesorProfil(nast);
	        			window.frmInfoOProfesoru.setVisible(true);
	        		}
	        }
	        

	    });
	
        
        btnDodati = new JButton("Dodati");
        btnDodati.setForeground(new Color(255, 255, 255));
        btnDodati.setBackground(new Color(128, 0, 0));
        btnDodati.setBounds(0, 581, 100, 34);
        btnDodati.addActionListener(new ActionListener()
        {
        @Override
          public void actionPerformed(ActionEvent e)
          {
        	if(selected>-1)
        	{
        	Nastavnik nast;
        	if(izmjena==false)
        		nast=new Nastavnik();
        	else
        		nast=nastavnici.get(selected);
        	for(Korisnik k : korisnici)
        	{
        	if(izmjena==false && k.getUsername().equals(txtUsername.getText()))
        	{
        		JOptionPane.showMessageDialog(null, "KorisniČko ime " + txtUsername.getText() + " veĆ postoji u bazi podataka!", "GreŠka", JOptionPane.ERROR_MESSAGE);
        		txtUsername.requestFocus();
        		return;
        	}
        	if(izmjena==true && k.getUsername().equals(txtUsername.getText()) && k.getId()!=nast.getKorisnik().getId())
        	{
        		JOptionPane.showMessageDialog(null, "KorisniČko ime " + txtUsername.getText() + " veĆ postoji u bazi podataka!", "Greška", JOptionPane.ERROR_MESSAGE);
        		txtUsername.requestFocus();
        		return;
        	}
        	
        	}
        	  if(txtIme.getText().length()<1)
        	  {
        		  JOptionPane.showMessageDialog(null, "Niste unijeli ime nastavnika!", "Greška", JOptionPane.ERROR_MESSAGE);
        		  txtIme.requestFocus();
        	  }
        	  else if(txtPrezime.getText().length()<1)
        	  {
        		  JOptionPane.showMessageDialog(null, "Niste unijeli prezime nastavnika!", "Greška", JOptionPane.ERROR_MESSAGE);
        		  txtPrezime.requestFocus();
        	  }
        	  else if(txtUsername.getText().length()<1)
        	  {
        		  JOptionPane.showMessageDialog(null, "Niste unijeli korisničko ime nastavnika!", "Greška", JOptionPane.ERROR_MESSAGE);
        		  txtUsername.requestFocus();
        	  }
        	  else if(txtPassword.getText().length()<1)
        	  {
        		  JOptionPane.showMessageDialog(null, "Niste unijeli lozinku!", "Greška", JOptionPane.ERROR_MESSAGE);
        		  txtPassword.requestFocus();
        	  }
        	  else if(txtBrojIndeksa.getText().length()<1)
        	  {
        		  JOptionPane.showMessageDialog(null, "Niste unijeli zvanje nastavnika!", "Greška", JOptionPane.ERROR_MESSAGE);
        		  txtBrojIndeksa.requestFocus();
        	  }        	  
        	  else
        	  {
        	  	em.getTransaction().begin();
        	  	Korisnik kor= izmjena ? nast.getKorisnik() : new Korisnik();        	    
        	    kor.setUsername(txtUsername.getText());
        	    kor.setLozinka(txtPassword.getText());
        	    if (chckbxNewCheckBox.isSelected())
        	    kor.setRole(3);
        	    else
        	    kor.setRole(1);
        	    nast.setKorisnik(kor);        	    
        	    nast.setZvanje(txtBrojIndeksa.getText());
        	    nast.setIme(txtIme.getText());
        	    nast.setPrezime(txtPrezime.getText());        	    
        	    nast.setRadnoMjesto((RadnoMjesto)cmbUsmjerenja.getSelectedItem());
        	    em.persist(nast.getKorisnik());
        	    ArrayList<Predmet> odabraniPredmeti=new ArrayList<Predmet>();
        	    for(int i=0; i<tablePredmeti.getRowCount(); i++)
        	    {
        	    	if(tablePredmeti.getValueAt(i, 2).equals(true))
        	    	{
        	    		odabraniPredmeti.add(predmeti.get(i));
        	    	}
        	    }
        	    nast.setPredmeti(odabraniPredmeti);
        	    em.persist(nast);
        	    em.getTransaction().commit();        	    
        	    OsvjeziTabelu(true);       
        	    OcistiPolja();
        	  }
          }
          }
        });
        frmNastavnici.getContentPane().add(btnDodati);
        
        btnIzmijeniti = new JButton("Izmijeniti");
        btnIzmijeniti.setBackground(new Color(128, 0, 0));
        btnIzmijeniti.setForeground(new Color(255, 255, 255));
        btnIzmijeniti.setBounds(94, 581, 100, 32);
        btnIzmijeniti.addActionListener(new ActionListener()
        {
          public void actionPerformed(ActionEvent e)
          {
        	 if(izmjena==false)
          	 {        		 
          	 if(selected>-1)
          	 {
          		 izmjena=true;
          		 chckbxNewCheckBox.setSelected(false);
          		 btnIzmijeniti.setText("Odustati");
          		 btnDodati.setText("Potvrditi");
          		 btnObrisati.setEnabled(false);
          		 Nastavnik nast = nastavnici.get(selected);
          		 txtIme.setText(nast.getIme());
          		 txtPrezime.setText(nast.getPrezime());
          		 txtUsername.setText(nast.getKorisnik().getUsername());
          		 txtPassword.setText(nast.getKorisnik().getLozinka());
          		 txtBrojIndeksa.setText(nast.getZvanje());
          		 if(radnaMjesta.size()>0)
          			 cmbUsmjerenja.setSelectedItem(nast.getRadnoMjesto());
          		 txtIme.requestFocus();
          		SetSelectedPredmeti(nast.getPredmeti());
          		if (nast.getKorisnik().getRole()==3) {
          		chckbxNewCheckBox.setSelected(true); }
          	 }
          	 }
          	 else
          	 {         		 
          		 izmjena=false;
          		 OcistiPolja();
          	 }
          }
        
        });
        frmNastavnici.getContentPane().add(btnIzmijeniti);
        
        btnObrisati = new JButton("Obrisati");
        btnObrisati.setForeground(new Color(255, 255, 255));
        btnObrisati.setBounds(188, 581, 104, 32);
        btnObrisati.setBackground(new Color(128, 0, 0));
        btnObrisati.addActionListener(new ActionListener()
        {
        	@Override
          public void actionPerformed(ActionEvent e)
          {
          if(selected>-1)
	         {
	        	try
	        	{
        	    em.getTransaction().begin();
        	       
        	    Nastavnik temp = nastavnici.get(selected);
        	    
        	    em.remove(temp);
        	    em.getTransaction().commit();        	    
        	 	  DefaultTableModel model = new DefaultTableModel();
            	  model=(DefaultTableModel) table.getModel();
                  model.removeRow(selected);
                  selected=-1;
	        	}
	        	catch (Exception err)
	        	  {
	        		  JOptionPane.showMessageDialog(null, "Nije moguće obrisati nastavnika koji ima pridružene podatke!", "Greška", JOptionPane.ERROR_MESSAGE);
	        	  }
              }
             }
        
        });
        frmNastavnici.getContentPane().add(btnObrisati);
        
        JLabel lblPrezime = new JLabel("Prezime:");
        lblPrezime.setFont(new Font("Tahoma", Font.BOLD, 13));
        lblPrezime.setBounds(18, 227, 111, 16);
        frmNastavnici.getContentPane().add(lblPrezime);
        
        txtPrezime = new JTextField();
        txtPrezime.setBounds(102, 221, 177, 28);
        txtPrezime.setColumns(10);
        frmNastavnici.getContentPane().add(txtPrezime);
        
        JLabel lblBrojIndeksa = new JLabel("Zvanje:");
        lblBrojIndeksa.setFont(new Font("Tahoma", Font.BOLD, 13));
        lblBrojIndeksa.setBounds(18, 320, 111, 16);
        frmNastavnici.getContentPane().add(lblBrojIndeksa);
        
        txtBrojIndeksa = new JTextField();
        txtBrojIndeksa.setBounds(18, 337, 261, 28);
        txtBrojIndeksa.setColumns(10);
        frmNastavnici.getContentPane().add(txtBrojIndeksa);
        
        
        cmbUsmjerenja = new JComboBox();
        cmbUsmjerenja.setBounds(18, 390, 261, 27);
        for(RadnoMjesto r : radnaMjesta)
        	cmbUsmjerenja.addItem(r);
        frmNastavnici.getContentPane().add(cmbUsmjerenja);
        
        JLabel lblUsmjerenje = new JLabel("Radno mjesto:");
        lblUsmjerenje.setFont(new Font("Tahoma", Font.BOLD, 13));
        lblUsmjerenje.setBounds(18, 373, 111, 16);
        frmNastavnici.getContentPane().add(lblUsmjerenje);
        
        JLabel lblKorisnikoIme = new JLabel("Korisničko ime\r\n");
        lblKorisnikoIme.setFont(new Font("Tahoma", Font.BOLD, 13));
        lblKorisnikoIme.setBounds(18, 262, 111, 16);
        frmNastavnici.getContentPane().add(lblKorisnikoIme);
        
        txtUsername = new JTextField();
        txtUsername.setBounds(144, 256, 135, 28);
        txtUsername.setColumns(10);
        frmNastavnici.getContentPane().add(txtUsername);
        
        JLabel lblPassword = new JLabel("Password:");
        lblPassword.setFont(new Font("Tahoma", Font.BOLD, 13));
        lblPassword.setBounds(18, 291, 70, 16);
        frmNastavnici.getContentPane().add(lblPassword);
        
        txtPassword = new JTextField();
        txtPassword.setBounds(144, 285, 135, 28);
        txtPassword.setColumns(10);
        frmNastavnici.getContentPane().add(txtPassword);
        
        Object columnNamesPredmeti[] = { "ID", "Predmet", "Uključen"};        
		int j=0;
		Object[][] rowDataPredmeti = new Object[predmeti.size()][3];
	    for(Predmet pr : predmeti)
	    {
	    	rowDataPredmeti[j][0]=new Integer(pr.getId()).toString();
	    	rowDataPredmeti[j][1]=pr.getNaziv();
	    	rowDataPredmeti[j][2]=false;
	    	j++;
	    }
	    
	    tablePredmeti = new JTable(rowDataPredmeti, columnNamesPredmeti) {
		    @Override
		    public boolean isCellEditable(int row, int column) {
		       if(column==2)
		    	   return true;
		       else
		    	   return false;
		    }
		    public Class getColumnClass(int column) {
                switch (column) {
                    case 0:
                        return String.class;
                    case 1:
                        return String.class;
                    default:
                        return Boolean.class;
                       
                }
		    }
		};    
        scrollPanePredmeti = new JScrollPane(tablePredmeti);
        scrollPanePredmeti.setBounds(18, 449, 261, 93);
        frmNastavnici.getContentPane().add(scrollPanePredmeti);
        tablePredmeti.getColumnModel().getColumn(0).setMinWidth(20);
        tablePredmeti.getColumnModel().getColumn(0).setMaxWidth(50);
        tablePredmeti.getColumnModel().getColumn(0).setPreferredWidth(35);
        tablePredmeti.getColumnModel().getColumn(2).setMinWidth(60);
        tablePredmeti.getColumnModel().getColumn(2).setMaxWidth(60);
        tablePredmeti.getColumnModel().getColumn(2).setPreferredWidth(60);
        
        
        JLabel lblPredmeti = new JLabel("Predmeti:");
        lblPredmeti.setFont(new Font("Tahoma", Font.BOLD, 13));
        lblPredmeti.setBounds(18, 430, 111, 34);
        lblPredmeti.setVerticalAlignment(SwingConstants.TOP);
        frmNastavnici.getContentPane().add(lblPredmeti);
        
        JLabel label = new JLabel("Filter po:");
        label.setBounds(314, 535, 70, 16);
        frmNastavnici.getContentPane().add(label);
        
        cmbFilter = new JComboBox();
        cmbFilter.setForeground(new Color(255, 255, 255));
        cmbFilter.setBackground(new Color(128, 0, 0));
        cmbFilter.setBounds(370, 530, 111, 27);
        cmbFilter.addActionListener (new ActionListener () {
	        public void actionPerformed(ActionEvent e) {
	        	txtFilter.setText("");
	            OsvjeziTabelu(true);
	        }
	    });
        cmbFilter.setModel(new DefaultComboBoxModel(new String[] {"ime", "prezime","radno mjesto"}));
        frmNastavnici.getContentPane().add(cmbFilter);
        
        txtFilter = new JTextField();
        txtFilter.setBounds(486, 529, 236, 28);
        txtFilter.getDocument().addDocumentListener(new DocumentListener()
        {
            public void changedUpdate(DocumentEvent arg0) 
            {
           
            }
            public void insertUpdate(DocumentEvent arg0) 
            {
            	if(cmbFilter.getSelectedIndex()==0)
            	     nastavnici = select(nastavniciSource, having ( on(Nastavnik.class).getIme(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==1)
            		 nastavnici = select(nastavniciSource, having(on(Nastavnik.class).getPrezime(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==2)
           		     nastavnici = select(nastavniciSource, having(on(Nastavnik.class).getRadnoMjesto().getNaziv(), Matchers.containsString(txtFilter.getText())));
            	OsvjeziTabelu(false);
            }

            public void removeUpdate(DocumentEvent arg0) 
            {
            	if(cmbFilter.getSelectedIndex()==0)
            		nastavnici = select(nastavniciSource, having(on(Nastavnik.class).getIme(), Matchers.containsString(txtFilter.getText()))); 
            	else if(cmbFilter.getSelectedIndex()==1)
            		nastavnici = select(nastavniciSource, having(on(Nastavnik.class).getPrezime(), Matchers.containsString(txtFilter.getText())));
            	else if(cmbFilter.getSelectedIndex()==2)
          		     nastavnici = select(nastavniciSource, having(on(Nastavnik.class).getRadnoMjesto().getNaziv(), Matchers.containsString(txtFilter.getText())));
            	OsvjeziTabelu(false);
            }           
        });
        txtFilter.setColumns(10); 
        frmNastavnici.getContentPane().add(txtFilter);
        
        chckbxNewCheckBox = new JCheckBox("Asistent");
        chckbxNewCheckBox.setFont(new Font("Tahoma", Font.BOLD, 14));
        chckbxNewCheckBox.setBounds(18, 549, 113, 25);
        frmNastavnici.getContentPane().add(chckbxNewCheckBox);
        
        JLabel label_1 = new JLabel("");
        label_1.setBounds(0, 0, 734, 173);
        Image naslovnica= new ImageIcon (this.getClass().getResource("/cover.jpg")).getImage();
		label_1.setIcon(new ImageIcon(naslovnica));
        frmNastavnici.getContentPane().add(label_1);
        
        JPanel panel = new JPanel();
        panel.setBounds(0, 173, 290, 439);
        frmNastavnici.getContentPane().add(panel);

        OsvjeziTabelu(true);
        
       
        table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            			@Override
			public void valueChanged(ListSelectionEvent e) 
            {
            	selected = table.getSelectedRow();
            	OcistiPolja();
				
			}
        });
		
		
		
        frmNastavnici.setBackground(new Color(255, 255, 255));        
        frmNastavnici.setBounds(100, 100, 450, 300);
        frmNastavnici.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmNastavnici.setSize(new Dimension(752, 659));
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frmNastavnici.setLocation(dim.width/2-frmNastavnici.getSize().width/2, dim.height/2-frmNastavnici.getSize().height/2);

	}
	
	private class SwingAction extends AbstractAction {
		public SwingAction() {
			putValue(NAME, "SwingAction");
			putValue(SHORT_DESCRIPTION, "Some short description");
		}
		public void actionPerformed(ActionEvent e) {
		}
	}
	
	
}
